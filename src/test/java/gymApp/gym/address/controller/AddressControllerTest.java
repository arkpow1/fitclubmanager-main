package gymApp.gym.address.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;
import gymApp.gym.GymApplication;
import gymApp.gym.client.service.ClientService;
import gymApp.gym.commons.client.TestCommonClient;
import gymApp.gym.commons.client.TestCommonService;
import gymApp.gym.mail.service.EmailService;
import gymApp.gym.models.address.Address;
import gymApp.gym.security.UrlFinder;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD;

@RunWith(SpringRunner.class)
@DirtiesContext(classMode = AFTER_EACH_TEST_METHOD)
@SpringBootTest(classes = {GymApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@MockBean(EmailService.class)
public class AddressControllerTest {

    @Autowired
    ClientService clientService;

    @Autowired
    TestCommonClient commonClient;

    @Autowired
    TestCommonService commonService;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    UrlFinder urlFinder;

    private String token;

    @Before
    public void before() throws JsonProcessingException {
        RestAssured.baseURI = urlFinder.findApplicationUrl();
        token = commonClient.getAdminToken();
    }

    @Test
    public void shouldCreateAndGetAddresByClientId() throws IOException {
        //given
        List<Address> addressesList = objectMapper.readValue(AddressControllerClient.getAll(token), new TypeReference<List<Address>>() {
        });
        Address address = objectMapper.readValue(AddressControllerClient.create(objectMapper.writeValueAsString(commonService.getSampleAddress()), token), Address.class);

        //when
        List<Address> actualAddressesList = objectMapper.readValue(AddressControllerClient.getAll(token), new TypeReference<List<Address>>() {
        });
        Address addressByClientId = objectMapper.readValue(AddressControllerClient.getByClientId(address.getClient().getId().toString(), token), Address.class);

        //then
        Assertions.assertThat(addressesList.size()).isEqualTo(actualAddressesList.size() - 1);
        Assertions.assertThat(addressByClientId.getId()).isEqualTo(address.getId());
        Assertions.assertThat(addressByClientId.getCountry()).isEqualTo(address.getCountry());
        Assertions.assertThat(addressByClientId.getState()).isEqualTo(address.getState());
        Assertions.assertThat(addressByClientId.isRemoved()).isEqualTo(address.isRemoved());
        Assertions.assertThat(addressByClientId.getCity()).isEqualTo(address.getCity());
        Assertions.assertThat(addressByClientId.getApartmentNumber()).isEqualTo(address.getApartmentNumber());
        Assertions.assertThat(addressByClientId.getHouseNumber()).isEqualTo(address.getHouseNumber());
        Assertions.assertThat(addressByClientId.getPostalCode()).isEqualTo(address.getPostalCode());
        Assertions.assertThat(addressByClientId.getPostOffice()).isEqualTo(address.getPostOffice());
        Assertions.assertThat(addressByClientId.getStreet()).isEqualTo(address.getStreet());
        Assertions.assertThat(addressByClientId.getClient().getId()).isEqualTo(address.getClient().getId());

    }

    @Test
    public void shouldUpdateAddress() throws IOException {
        //given
        Address address = objectMapper.readValue(AddressControllerClient.create(objectMapper.writeValueAsString(commonService.getSampleAddress()), token), Address.class);
        String addressCountry = address.getCountry();
        String addressState = address.getState();
        String addressCity = address.getCity();
        String addressStreet = address.getStreet();
        Long addressApartmentNumber = address.getApartmentNumber();
        Long addressHouseNumber = address.getHouseNumber();
        String addressPostalCode = address.getPostalCode();
        String addressPostOffice = address.getPostOffice();

        Address addressWithFieldsToUpdate = address;

        String exampleAddressCountry = UUID.randomUUID().toString();
        String exampleAddressState = UUID.randomUUID().toString();
        String exampleAddressCity = UUID.randomUUID().toString();
        String exampleAddressStreet = UUID.randomUUID().toString();
        Long exampleAddressApartmentNumber = UUID.randomUUID().getMostSignificantBits();
        Long exampleAddresHouseNumber = UUID.randomUUID().getMostSignificantBits();
        String exampleAddressPostalCode = UUID.randomUUID().toString();
        String exampleAddressPostOffice = UUID.randomUUID().toString();

        addressWithFieldsToUpdate.setCountry(exampleAddressCountry);
        addressWithFieldsToUpdate.setState(exampleAddressState);
        addressWithFieldsToUpdate.setCity(exampleAddressCity);
        addressWithFieldsToUpdate.setStreet(exampleAddressStreet);
        addressWithFieldsToUpdate.setApartmentNumber(exampleAddressApartmentNumber);
        addressWithFieldsToUpdate.setHouseNumber(exampleAddresHouseNumber);
        addressWithFieldsToUpdate.setPostalCode(exampleAddressPostalCode);
        addressWithFieldsToUpdate.setPostOffice(exampleAddressPostOffice);

        //when
        AddressControllerClient.update(objectMapper.writeValueAsString(addressWithFieldsToUpdate), token);
        Address addressWithUpdatedFields = objectMapper.readValue(AddressControllerClient.getById(address.getId().toString(), token), Address.class);

        //then
        Assertions.assertThat(addressWithUpdatedFields).isNotEqualTo(address);
        Assertions.assertThat(addressWithUpdatedFields.getId()).isEqualTo(address.getId());

        Assertions.assertThat(addressWithUpdatedFields.getCountry()).isNotEqualTo(addressCountry);
        Assertions.assertThat(addressWithUpdatedFields.getCountry()).isEqualTo(exampleAddressCountry);

        Assertions.assertThat(addressWithUpdatedFields.getState()).isNotEqualTo(addressState);
        Assertions.assertThat(addressWithUpdatedFields.getState()).isEqualTo(exampleAddressState);

        Assertions.assertThat(addressWithUpdatedFields.getCity()).isNotEqualTo(addressCity);
        Assertions.assertThat(addressWithUpdatedFields.getCity()).isEqualTo(exampleAddressCity);

        Assertions.assertThat(addressWithUpdatedFields.getStreet()).isNotEqualTo(addressStreet);
        Assertions.assertThat(addressWithUpdatedFields.getStreet()).isEqualTo(exampleAddressStreet);

        Assertions.assertThat(addressWithUpdatedFields.getHouseNumber()).isNotEqualTo(addressHouseNumber);
        Assertions.assertThat(addressWithUpdatedFields.getHouseNumber()).isEqualTo(exampleAddresHouseNumber);

        Assertions.assertThat(addressWithUpdatedFields.getApartmentNumber()).isNotEqualTo(addressApartmentNumber);
        Assertions.assertThat(addressWithUpdatedFields.getApartmentNumber()).isEqualTo(exampleAddressApartmentNumber);

        Assertions.assertThat(addressWithUpdatedFields.getPostOffice()).isNotEqualTo(addressPostOffice);
        Assertions.assertThat(addressWithUpdatedFields.getPostOffice()).isEqualTo(exampleAddressPostOffice);

        Assertions.assertThat(addressWithUpdatedFields.getPostalCode()).isNotEqualTo(addressPostalCode);
        Assertions.assertThat(addressWithUpdatedFields.getPostalCode()).isEqualTo(exampleAddressPostalCode);
    }

    @Test
    public void shouldCreateAndGetAddressById() throws IOException {
        //given
        List<Address> addressesList = objectMapper.readValue(AddressControllerClient.getAll(token), new TypeReference<List<Address>>() {
        });
        Address address = objectMapper.readValue(AddressControllerClient.create(objectMapper.writeValueAsString(commonService.getSampleAddress()), token), Address.class);

        //when
        List<Address> actualAddressesList = objectMapper.readValue(AddressControllerClient.getAll(token), new TypeReference<List<Address>>() {
        });
        Address addressByClientId = objectMapper.readValue(AddressControllerClient.getById(address.getId().toString(), token), Address.class);

        //then
        Assertions.assertThat(addressesList.size()).isEqualTo(actualAddressesList.size() - 1);
        Assertions.assertThat(addressByClientId.getId()).isEqualTo(address.getId());
        Assertions.assertThat(addressByClientId.getCountry()).isEqualTo(address.getCountry());
        Assertions.assertThat(addressByClientId.getState()).isEqualTo(address.getState());
        Assertions.assertThat(addressByClientId.isRemoved()).isEqualTo(address.isRemoved());
        Assertions.assertThat(addressByClientId.getCity()).isEqualTo(address.getCity());
        Assertions.assertThat(addressByClientId.getApartmentNumber()).isEqualTo(address.getApartmentNumber());
        Assertions.assertThat(addressByClientId.getHouseNumber()).isEqualTo(address.getHouseNumber());
        Assertions.assertThat(addressByClientId.getPostalCode()).isEqualTo(address.getPostalCode());
        Assertions.assertThat(addressByClientId.getPostOffice()).isEqualTo(address.getPostOffice());
        Assertions.assertThat(addressByClientId.getStreet()).isEqualTo(address.getStreet());
        Assertions.assertThat(addressByClientId.getClient().getId()).isEqualTo(address.getClient().getId());
    }

    @Test
    public void shouldSetRemovedAddress() throws IOException {
        //given
        Address address = objectMapper.readValue(AddressControllerClient.create(objectMapper.writeValueAsString(commonService.getSampleAddress()), token), Address.class);
        List<Address> addressesList = objectMapper.readValue(AddressControllerClient.getAll(token), new TypeReference<List<Address>>() {
        });

        //when
        AddressControllerClient.setRemoved(address.getId().toString(), token);
        List<Address> actualList = objectMapper.readValue(AddressControllerClient.getAll(token), new TypeReference<List<Address>>() {
        });

        //then
        Assertions.assertThat(addressesList.size() - 1).isEqualTo(actualList.size());
        Assertions.assertThat(AddressControllerClient.getById(address.getId().toString(), token)).isEmpty();
    }

    @Test
    public void shouldRemoveAddress() throws IOException {
        //given
        Address address = objectMapper.readValue(AddressControllerClient.create(objectMapper.writeValueAsString(commonService.getSampleAddress()), token), Address.class);
        List<Address> addressesList = objectMapper.readValue(AddressControllerClient.getAll(token), new TypeReference<List<Address>>() {
        });

        //when
        AddressControllerClient.remove(address.getId().toString(), token);
        List<Address> actualList = objectMapper.readValue(AddressControllerClient.getAll(token), new TypeReference<List<Address>>() {
        });

        //then
        Assertions.assertThat(addressesList.size() - 1).isEqualTo(actualList.size());
        Assertions.assertThat(AddressControllerClient.getById(address.getId().toString(), token)).isEmpty();
    }
}
