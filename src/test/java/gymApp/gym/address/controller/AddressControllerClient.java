package gymApp.gym.address.controller;

import com.jayway.restassured.http.ContentType;
import org.apache.http.HttpHeaders;

import static com.jayway.restassured.RestAssured.given;

public class AddressControllerClient {

    private static final String ADRESS_PATH = "/address";

    public static String getAll(String token) {
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .when()
                .header(HttpHeaders.AUTHORIZATION, token)
                .get(ADRESS_PATH)
                .then()
                .log()
                .ifError()
                .assertThat()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .print();
    }

    static String setRemoved(String id, String token) {
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .when()
                .header(HttpHeaders.AUTHORIZATION, token)
                .delete(ADRESS_PATH + "/setRemoved/" + id)
                .then()
                .log()
                .ifError()
                .assertThat()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .print();
    }

    static String remove(String id, String token) {
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .when()
                .header(HttpHeaders.AUTHORIZATION, token)
                .delete(ADRESS_PATH + "/remove/" + id)
                .then()
                .log()
                .ifError()
                .assertThat()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .print();
    }

    public static String create(String json, String token) {
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .when()
                .header(HttpHeaders.AUTHORIZATION, token)
                .body(json)
                .post(ADRESS_PATH)
                .then()
                .log()
                .ifError()
                .assertThat()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .print();
    }

    static String update(String json, String token) {
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .when()
                .header(HttpHeaders.AUTHORIZATION, token)
                .body(json)
                .patch(ADRESS_PATH)
                .then()
                .log()
                .ifError()
                .assertThat()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .print();
    }

    static String getById(String id, String token) {
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .when()
                .header(HttpHeaders.AUTHORIZATION, token)
                .get(ADRESS_PATH + "/id/" + id)
                .then()
                .log()
                .ifError()
                .assertThat()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .print();
    }

    static String getByClientId(String clientId, String token) {
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .when()
                .header(HttpHeaders.AUTHORIZATION, token)
                .get(ADRESS_PATH + "/cid/" + clientId)
                .then()
                .log()
                .ifError()
                .assertThat()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .print();
    }
}
