package gymApp.gym.address.service;

import gymApp.gym.client.service.ClientService;
import gymApp.gym.commons.client.TestCommonService;
import gymApp.gym.mail.service.EmailService;
import gymApp.gym.models.address.Address;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"application.properties"})
@MockBean(EmailService.class)
public class AdressServiceTest {

    @Autowired
    ClientService clientService;

    @Autowired
    AddressService addressService;

    @Autowired
    TestCommonService commonService;

    @Test
    public void shouldCreateAndGetAddresByClientId() {
        //given
        List<Address> addressesList = addressService.getAll();
        Address address = commonService.getAndSaveSampleAddress();

        //when
        List<Address> actualAddressesList = addressService.getAll();
        Address addressByClientId = addressService.getForClientAddress(address.getClient().getId());

        //then
        Assertions.assertThat(addressesList.size()).isEqualTo(actualAddressesList.size() - 1);
        Assertions.assertThat(addressByClientId.getId()).isEqualTo(address.getId());
        Assertions.assertThat(addressByClientId.getCountry()).isEqualTo(address.getCountry());
        Assertions.assertThat(addressByClientId.getState()).isEqualTo(address.getState());
        Assertions.assertThat(addressByClientId.isRemoved()).isEqualTo(address.isRemoved());
        Assertions.assertThat(addressByClientId.getCity()).isEqualTo(address.getCity());
        Assertions.assertThat(addressByClientId.getApartmentNumber()).isEqualTo(address.getApartmentNumber());
        Assertions.assertThat(addressByClientId.getHouseNumber()).isEqualTo(address.getHouseNumber());
        Assertions.assertThat(addressByClientId.getPostalCode()).isEqualTo(address.getPostalCode());
        Assertions.assertThat(addressByClientId.getPostOffice()).isEqualTo(address.getPostOffice());
        Assertions.assertThat(addressByClientId.getStreet()).isEqualTo(address.getStreet());
        Assertions.assertThat(addressByClientId.getClient().getId()).isEqualTo(address.getClient().getId());
    }

    @Test
    public void shouldUpdateAddress() {
        //given
        Address address = commonService.getAndSaveSampleAddress();
        String addressCountry = address.getCountry();
        String addressState = address.getState();
        String addressCity = address.getCity();
        String addressStreet = address.getStreet();
        Long addressApartmentNumber = address.getApartmentNumber();
        Long addressHouseNumber = address.getHouseNumber();
        String addressPostalCode = address.getPostalCode();
        String addressPostOffice = address.getPostOffice();

        Address addressWithFieldsToUpdate = address;

        String exampleAddressCountry = UUID.randomUUID().toString();
        String exampleAddressState = UUID.randomUUID().toString();
        String exampleAddressCity = UUID.randomUUID().toString();
        String exampleAddressStreet = UUID.randomUUID().toString();
        Long exampleAddressApartmentNumber = UUID.randomUUID().getMostSignificantBits();
        Long exampleAddresHouseNumber = UUID.randomUUID().getMostSignificantBits();
        String exampleAddressPostalCode = UUID.randomUUID().toString();
        String exampleAddressPostOffice = UUID.randomUUID().toString();

        addressWithFieldsToUpdate.setCountry(exampleAddressCountry);
        addressWithFieldsToUpdate.setState(exampleAddressState);
        addressWithFieldsToUpdate.setCity(exampleAddressCity);
        addressWithFieldsToUpdate.setStreet(exampleAddressStreet);
        addressWithFieldsToUpdate.setApartmentNumber(exampleAddressApartmentNumber);
        addressWithFieldsToUpdate.setHouseNumber(exampleAddresHouseNumber);
        addressWithFieldsToUpdate.setPostalCode(exampleAddressPostalCode);
        addressWithFieldsToUpdate.setPostOffice(exampleAddressPostOffice);

        //when
        addressService.update(addressWithFieldsToUpdate);
        Address addressWithUpdatedFields = addressService.getById(address.getId());

        //then
        Assertions.assertThat(addressWithUpdatedFields).isNotEqualTo(address);
        Assertions.assertThat(addressWithUpdatedFields.getId()).isEqualTo(address.getId());

        Assertions.assertThat(addressWithUpdatedFields.getCountry()).isNotEqualTo(addressCountry);
        Assertions.assertThat(addressWithUpdatedFields.getCountry()).isEqualTo(exampleAddressCountry);

        Assertions.assertThat(addressWithUpdatedFields.getState()).isNotEqualTo(addressState);
        Assertions.assertThat(addressWithUpdatedFields.getState()).isEqualTo(exampleAddressState);

        Assertions.assertThat(addressWithUpdatedFields.getCity()).isNotEqualTo(addressCity);
        Assertions.assertThat(addressWithUpdatedFields.getCity()).isEqualTo(exampleAddressCity);

        Assertions.assertThat(addressWithUpdatedFields.getStreet()).isNotEqualTo(addressStreet);
        Assertions.assertThat(addressWithUpdatedFields.getStreet()).isEqualTo(exampleAddressStreet);

        Assertions.assertThat(addressWithUpdatedFields.getHouseNumber()).isNotEqualTo(addressHouseNumber);
        Assertions.assertThat(addressWithUpdatedFields.getHouseNumber()).isEqualTo(exampleAddresHouseNumber);

        Assertions.assertThat(addressWithUpdatedFields.getApartmentNumber()).isNotEqualTo(addressApartmentNumber);
        Assertions.assertThat(addressWithUpdatedFields.getApartmentNumber()).isEqualTo(exampleAddressApartmentNumber);

        Assertions.assertThat(addressWithUpdatedFields.getPostOffice()).isNotEqualTo(addressPostOffice);
        Assertions.assertThat(addressWithUpdatedFields.getPostOffice()).isEqualTo(exampleAddressPostOffice);

        Assertions.assertThat(addressWithUpdatedFields.getPostalCode()).isNotEqualTo(addressPostalCode);
        Assertions.assertThat(addressWithUpdatedFields.getPostalCode()).isEqualTo(exampleAddressPostalCode);
    }

    @Test
    public void shouldCreateAndGetAddressById() {
        //given
        List<Address> addressesList = addressService.getAll();
        Address address = commonService.getAndSaveSampleAddress();

        //when
        List<Address> actualAddressesList = addressService.getAll();
        Address addressByClientId = addressService.getById(address.getId());

        Assertions.assertThat(addressesList.size()).isEqualTo(actualAddressesList.size() - 1);
        Assertions.assertThat(addressByClientId.getId()).isEqualTo(address.getId());
        Assertions.assertThat(addressByClientId.getCountry()).isEqualTo(address.getCountry());
        Assertions.assertThat(addressByClientId.getState()).isEqualTo(address.getState());
        Assertions.assertThat(addressByClientId.isRemoved()).isEqualTo(address.isRemoved());
        Assertions.assertThat(addressByClientId.getCity()).isEqualTo(address.getCity());
        Assertions.assertThat(addressByClientId.getApartmentNumber()).isEqualTo(address.getApartmentNumber());
        Assertions.assertThat(addressByClientId.getHouseNumber()).isEqualTo(address.getHouseNumber());
        Assertions.assertThat(addressByClientId.getPostalCode()).isEqualTo(address.getPostalCode());
        Assertions.assertThat(addressByClientId.getPostOffice()).isEqualTo(address.getPostOffice());
        Assertions.assertThat(addressByClientId.getStreet()).isEqualTo(address.getStreet());
        Assertions.assertThat(addressByClientId.getClient().getId()).isEqualTo(address.getClient().getId());
    }

    @Test
    public void shouldSetRemovedAddress() {
        //given
        Address address = commonService.getAndSaveSampleAddress();
        List<Address> addressesList = addressService.getAll();

        //when
        addressService.setRemoved(address.getId());
        Address addressAfterSettingRemoved = addressService.getById(address.getId());
        List<Address> actualList = addressService.getAll();

        //then
        Assertions.assertThat(addressesList.size() - 1).isEqualTo(actualList.size());
        Assertions.assertThat(addressAfterSettingRemoved).isNull();
    }

    @Test
    public void shouldRemoveAddress() {
        Address address = commonService.getAndSaveSampleAddress();
        List<Address> addressesList = addressService.getAll();

        //when
        addressService.remove(address.getId());
        List<Address> actualList = addressService.getAll();

        //then
        Assertions.assertThat(addressesList.size() - 1).isEqualTo(actualList.size());
        Assertions.assertThat(addressService.getById(address.getId())).isNull();
    }
}
