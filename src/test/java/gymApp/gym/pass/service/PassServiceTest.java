package gymApp.gym.pass.service;


import gymApp.gym.client.service.ClientService;
import gymApp.gym.commons.client.TestCommonService;
import gymApp.gym.mail.service.EmailService;
import gymApp.gym.models.pass.Pass;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"application.properties"})
@MockBean(EmailService.class)
public class PassServiceTest {

    @Autowired
    PassService passService;

    @Autowired
    ClientService clientService;

    @Autowired
    TestCommonService commonService;

    @Test
    public void shouldCreate() {
        List<Pass> passesList = passService.getAll();
        Pass pass = commonService.getAndSaveActiveSamplePass();

        //when
        List<Pass> actualPassesList = passService.getAll();

        //then
        Assertions.assertThat(passesList.size()).isEqualTo(actualPassesList.size() - 1);
    }

    @Test
    public void shouldCreateAndGetByPassId() {
        List<Pass> passesList = passService.getAll();
        Pass pass = commonService.getAndSaveActiveSamplePass();

        //when
        List<Pass> actualPassesList = passService.getAll();
        Pass passByClientId = passService.getById(pass.getId());

        //then
        Assertions.assertThat(passesList.size()).isEqualTo(actualPassesList.size() - 1);
        Assertions.assertThat(passByClientId.getId()).isEqualTo(pass.getId());
        Assertions.assertThat(passByClientId.isRemoved()).isEqualTo(pass.isRemoved());
        Assertions.assertThat(passByClientId.getClientId()).isEqualTo(pass.getClientId());
    }

    @Test
    public void shouldRemovePass() {
        Pass pass = commonService.getAndSaveNotActiveSamplePass();
        List<Pass> passesList = passService.getAll();

        //when
        passService.remove(pass.getId());
        List<Pass> actualList = passService.getAll();

        //then
        Assertions.assertThat(passesList.size() - 1).isEqualTo(actualList.size());
        Assertions.assertThat(passService.getById(pass.getId())).isNull();
    }

}
