package gymApp.gym.trainer.service;

import gymApp.gym.commons.client.TestCommonService;
import gymApp.gym.mail.service.EmailService;
import gymApp.gym.models.trainer.Gender;
import gymApp.gym.models.trainer.Trainer;
import gymApp.gym.user.service.UserService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"application.properties"})
@MockBean(EmailService.class)
public class TrainerServiceTest {

    @Autowired
    TrainerService trainerService;

    @Autowired
    TestCommonService commonService;

    @Test
    public void shouldCreateAndGetTrainerById() {
        //given
        List<Trainer> trainersList = trainerService.getAll();
        Trainer trainer = commonService.getAndSaveSampleTrainer();
//        UUID trainerId = trainer.getId();
//        trainerService.create(UUID.fromString("FD317A46-415C-4ADA-8468-7600FF869A2A"), trainer);

        //when
        List<Trainer> actualTrainersList = trainerService.getAll();
        Trainer trainerById = trainerService.getById(trainer.getId());

        //then
        Assertions.assertThat(trainersList.size()).isEqualTo(actualTrainersList.size() - 1);
        Assertions.assertThat(trainerById.getId()).isEqualTo(trainer.getId());
        Assertions.assertThat(trainerById.getEmail()).isEqualTo(trainer.getEmail());
        Assertions.assertThat(trainerById.getName()).isEqualTo(trainer.getName());
        Assertions.assertThat(trainerById.getSurname()).isEqualTo(trainer.getSurname());
        Assertions.assertThat(trainerById.getPesel()).isEqualTo(trainer.getPesel());
        Assertions.assertThat(trainerById.getPhoneNumber()).isEqualTo(trainer.getPhoneNumber());
        Assertions.assertThat(trainerById.getDescription()).isEqualTo(trainer.getDescription());
        Assertions.assertThat(trainerById.isNotifications()).isEqualTo(trainer.isNotifications());
        Assertions.assertThat(trainerById.isShowProfile()).isEqualTo(trainer.isShowProfile());
        Assertions.assertThat(trainerById.isShowProfile()).isEqualTo(false);
        Assertions.assertThat(trainerById.getGender()).isEqualTo(trainer.getGender());
        Assertions.assertThat(trainerById.isRemoved()).isEqualTo(trainer.isRemoved());
    }

    @Test
    public void shouldCreateAndGetTrainerWithNotifications() {
        //given
        List<Trainer> trainersList = trainerService.getAll();
        Trainer trainer = commonService.getAndSaveSampleTrainer();

        //when
        List<Trainer> actualTrainersList = trainerService.getAll();
        List<Trainer> trainersWithNotifications = trainerService.getWithNotifications();

        //then
        Assertions.assertThat(trainersList.size()).isEqualTo(actualTrainersList.size() - 1);
        Assertions.assertThat(trainersWithNotifications.size()).isGreaterThan(0);
        Assertions.assertThat(trainersWithNotifications.get(trainersWithNotifications.size() - 1).getId()).isEqualTo(trainer.getId());
        Assertions.assertThat(trainersWithNotifications.get(trainersWithNotifications.size() - 1).getEmail()).isEqualTo(trainer.getEmail());
        Assertions.assertThat(trainersWithNotifications.get(trainersWithNotifications.size() - 1).getName()).isEqualTo(trainer.getName());
        Assertions.assertThat(trainersWithNotifications.get(trainersWithNotifications.size() - 1).getSurname()).isEqualTo(trainer.getSurname());
        Assertions.assertThat(trainersWithNotifications.get(trainersWithNotifications.size() - 1).getPesel()).isEqualTo(trainer.getPesel());
        Assertions.assertThat(trainersWithNotifications.get(trainersWithNotifications.size() - 1).getPhoneNumber()).isEqualTo(trainer.getPhoneNumber());
        Assertions.assertThat(trainersWithNotifications.get(trainersWithNotifications.size() - 1).getDescription()).isEqualTo(trainer.getDescription());
        Assertions.assertThat(trainersWithNotifications.get(trainersWithNotifications.size() - 1).isNotifications()).isEqualTo(trainer.isNotifications());
        Assertions.assertThat(trainersWithNotifications.get(trainersWithNotifications.size() - 1).getGender()).isEqualTo(trainer.getGender());
        Assertions.assertThat(trainersWithNotifications.get(trainersWithNotifications.size() - 1).isRemoved()).isEqualTo(trainer.isRemoved());
    }

    @Test
    public void shouldUpdateTrainer() {
        //given
        Trainer trainer = commonService.getAndSaveSampleTrainer();
        Boolean trainerNotifications = trainer.isNotifications();
        String trainerSurname = trainer.getSurname();
        String trainerEmail = trainer.getEmail();

        Trainer trainerWithFieldsToUpdate = trainer;

        String exampleEmail = UUID.randomUUID().toString() + "@test.pl";

        trainerWithFieldsToUpdate.setNotifications(false);
        trainerWithFieldsToUpdate.setEmail(exampleEmail);

        //when
        trainerService.update(trainerWithFieldsToUpdate);
        Trainer trainerWithUpdatedFields = trainerService.getById(trainer.getId());

        //then
//        Assertions.assertThat(trainerWithUpdatedFields).isNotEqualTo(trainer);
        Assertions.assertThat(trainerWithUpdatedFields.getId()).isEqualTo(trainer.getId());
        Assertions.assertThat(trainerWithUpdatedFields.isNotifications()).isNotEqualTo(trainerNotifications);
        Assertions.assertThat(trainerWithUpdatedFields.isNotifications()).isEqualTo(false);
        Assertions.assertThat(trainerWithUpdatedFields.getEmail()).isNotEqualTo(trainerEmail);
        Assertions.assertThat(trainerWithUpdatedFields.getEmail()).isEqualTo(exampleEmail);
    }

    @Test
    public void shouldUpdateAndGetTrainerWithShowProfile() {
        //given
        Trainer trainer = commonService.getAndSaveSampleTrainer();
        Boolean trainerNotifications = trainer.isNotifications();
        Boolean trainerShowProfile = trainer.isShowProfile();
        String trainerSurname = trainer.getSurname();
        String trainerEmail = trainer.getEmail();

        Trainer trainerWithFieldsToUpdate = trainer;

        String exampleEmail = UUID.randomUUID().toString() + "@test.pl";

        trainerWithFieldsToUpdate.setNotifications(false);
        trainerWithFieldsToUpdate.setShowProfile(true);
        trainerWithFieldsToUpdate.setEmail(exampleEmail);

        //when
        trainerService.update(trainerWithFieldsToUpdate);
        Trainer trainerWithUpdatedFields = trainerService.getById(trainer.getId());
        List<Trainer> trainersWithShowProfile = trainerService.getWithShowProfile();

        //then
        Assertions.assertThat(trainerWithUpdatedFields.getId()).isEqualTo(trainer.getId());
        Assertions.assertThat(trainerWithUpdatedFields.isNotifications()).isNotEqualTo(trainerNotifications);
        Assertions.assertThat(trainerWithUpdatedFields.isNotifications()).isEqualTo(false);
        Assertions.assertThat(trainerWithUpdatedFields.isShowProfile()).isNotEqualTo(trainerShowProfile);
        Assertions.assertThat(trainerWithUpdatedFields.isShowProfile()).isEqualTo(true);
        Assertions.assertThat(trainerWithUpdatedFields.getEmail()).isNotEqualTo(trainerEmail);
        Assertions.assertThat(trainerWithUpdatedFields.getEmail()).isEqualTo(exampleEmail);
        Assertions.assertThat(trainersWithShowProfile).contains(trainerWithUpdatedFields);
        Assertions.assertThat(trainersWithShowProfile.size()).isGreaterThanOrEqualTo(1);
    }

    @Test
    public void shouldSetRemovedTrainer() {
        //given
        Trainer trainer = commonService.getAndSaveSampleTrainer();
        List<Trainer> trainersList = trainerService.getAll();

        //when
        trainerService.setRemoved(trainer.getId());
        Trainer trainerAfterSettingRemoved = trainerService.getById(trainer.getId());
        List<Trainer> actualList = trainerService.getAll();

        //then
        Assertions.assertThat(trainersList.size() - 1).isEqualTo(actualList.size());
        Assertions.assertThat(trainerAfterSettingRemoved).isNull();
    }

    @Test
    public void shouldRemoveTrainer() {
        //given
        Trainer trainer = commonService.getAndSaveSampleTrainer();
        List<Trainer> trainersList = trainerService.getAll();

        //when
        trainerService.deleteById(trainer.getId());
        List<Trainer> actualList = trainerService.getAll();

        //then
        Assertions.assertThat(trainersList.size() - 1).isEqualTo(actualList.size());
        Assertions.assertThat(trainerService.getById(trainer.getId())).isNull();
    }


//    private Trainer getSampleTrainer() {
//        Trainer trainer = Trainer.builder()
//                .id(UUID.randomUUID())
//                .email(UUID.randomUUID().toString() + "@test.pl")
//                .name(UUID.randomUUID().toString())
//                .surname(UUID.randomUUID().toString())
//                .birthdate(LocalDate.now().minusYears(18))
//                .pesel(UUID.randomUUID().toString())
//                .phoneNumber("123456789")
//                .description(UUID.randomUUID().toString())
//                .notifications(true)
//                .gender(Gender.MAN)
//                .build();
//        return trainer;
//    }


}
