package gymApp.gym.user.controller;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ValidatableResponse;
import org.apache.http.HttpHeaders;

import static com.jayway.restassured.RestAssured.given;

public class UserControllerClient {

    private static final String USERS_PATH = "/users";
    private static final String LOGIN_PATH = "/login";

    public static ValidatableResponse create(String json, String token) {
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .body(json)
                .when()
                .header(HttpHeaders.AUTHORIZATION, token)
                .post(USERS_PATH)
                .then()
                .log()
                .ifError()
                .assertThat();
    }

    public static ValidatableResponse findUserByEmail(String email, String token) {
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .when()
                .header(HttpHeaders.AUTHORIZATION, token)
                .param("email", email)
                .get(USERS_PATH + "/email")
                .then()
                .log()
                .ifError()
                .assertThat();
    }

    public static String getAdminToken(String json) {
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .body(json)
                .when()
                .post(LOGIN_PATH)
                .then()
                .log()
                .ifError()
                .assertThat()
                .statusCode(200)
                .extract()
                .header(HttpHeaders.AUTHORIZATION);
    }
}
