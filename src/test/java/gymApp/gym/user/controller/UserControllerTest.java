package gymApp.gym.user.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import gymApp.gym.GymApplication;
import gymApp.gym.commons.client.TestCommonClient;
import gymApp.gym.commons.client.TestCommonService;
import gymApp.gym.loginToken.mapper.LoginTokenMapper;
import gymApp.gym.loginToken.service.LoginTokenService;
import gymApp.gym.mail.service.EmailService;
import gymApp.gym.models.loginToken.LoginToken;
import gymApp.gym.models.user.User;
import gymApp.gym.models.user.UserLoginTO;
import gymApp.gym.models.user.UserLoginTOHelper;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.security.AuthenticationFilter;
import gymApp.gym.security.TokenService;
import gymApp.gym.security.UrlFinder;
import gymApp.gym.tools.TestApplicationConfig;
import gymApp.gym.user.service.UserService;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD;

@RunWith(SpringRunner.class)
@DirtiesContext(classMode = AFTER_EACH_TEST_METHOD)
@SpringBootTest(classes = {GymApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@MockBean(EmailService.class)
public class UserControllerTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UrlFinder urlFinder;

    @Autowired
    UserService userService;

    @Autowired
    TestApplicationConfig applicationConfig;

    @Autowired
    TestCommonService commonService;

    @Autowired
    TestCommonClient commonClient;

    @Autowired
    LoginTokenMapper loginTokenMapper;

    private String token;

    @Before
    public void before() throws JsonProcessingException {
        RestAssured.baseURI = urlFinder.findApplicationUrl();
        token = commonClient.getAdminToken();
    }

    @Test
    public void shouldCreateAndFindUserByEmail() throws JsonProcessingException {
        User user = commonService.getSampleUser();
        List<User> userList = userService.getAll();

        UserControllerClient.create(objectMapper.writeValueAsString(user), token)
                .statusCode(200)
                .body("email", equalTo(user.getEmail()));

        List<User> actualUserList = userService.getAll();
        Assertions.assertThat(actualUserList.size() - 1).isEqualTo(userList.size());

        UserControllerClient.findUserByEmail(user.getEmail(), token)
                .statusCode(200)
                .contentType(ContentType.JSON)
                .body("email", equalTo(user.getEmail()))
                .body("removed", equalTo(false))
                .body("role", equalTo(user.getRole().toString()));
    }
}
