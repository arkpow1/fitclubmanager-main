package gymApp.gym.user.service;

import gymApp.gym.address.service.AddressService;
import gymApp.gym.client.service.ClientService;
import gymApp.gym.commons.client.TestCommonService;
import gymApp.gym.mail.service.EmailService;
import gymApp.gym.models.user.User;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"application.properties"})
@MockBean(EmailService.class)
public class UserServiceTest {

    @Autowired
    UserService userService;

    @Autowired
    ClientService clientService;

    @Autowired
    AddressService addressService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    TestCommonService commonService;

    @Test
    public void shouldCreateAndGetUserById() {
        //given
        List<User> usersList = userService.getAll();
        User user = commonService.getAndSaveSampleUser();

        //when
        List<User> actualUsersList = userService.getAll();
        User userById = userService.getById(user.getId());

        //then
        Assertions.assertThat(usersList.size()).isEqualTo(actualUsersList.size() - 1);
        Assertions.assertThat(userById.getId()).isEqualTo(user.getId());
        Assertions.assertThat(userById.getEmail()).isEqualTo(user.getEmail());
        Assertions.assertThat(userById.getPassword()).isEqualTo(user.getPassword());
        Assertions.assertThat(userById.getRemoved()).isEqualTo(user.getRemoved());
        Assertions.assertThat(userById.getRole()).isEqualTo(user.getRole());
    }

    @Test
    public void shouldCreateAndGetUserByEmail() {
        //given
        List<User> usersList = userService.getAll();
        User user = commonService.getAndSaveSampleUser();

        //when
        List<User> actualUsersList = userService.getAll();
        User userByEmail = userService.getByEmail(user.getEmail());

        //then
        Assertions.assertThat(usersList.size()).isEqualTo(actualUsersList.size() - 1);
        Assertions.assertThat(userByEmail.getId()).isEqualTo(user.getId());
        Assertions.assertThat(userByEmail.getEmail()).isEqualTo(user.getEmail());
        Assertions.assertThat(userByEmail.getPassword()).isEqualTo(user.getPassword());
        Assertions.assertThat(userByEmail.getRemoved()).isEqualTo(user.getRemoved());
        Assertions.assertThat(userByEmail.getRole()).isEqualTo(user.getRole());
    }

    @Test
    public void shouldCreateAndGetUserByRole() {
        //given
        List<User> usersList = userService.getAll();
        User user = commonService.getAndSaveSampleUser();

        //when
        List<User> actualUsersList = userService.getAll();
        List<User> usersByRole = userService.getByRole(user.getRole());

        //then
        Assertions.assertThat(usersList.size()).isEqualTo(actualUsersList.size() - 1);
        Assertions.assertThat(usersByRole.size()).isGreaterThan(0);
        Assertions.assertThat(usersByRole.get(usersByRole.size() - 1).getId()).isEqualTo(user.getId());
        Assertions.assertThat(usersByRole.get(usersByRole.size() - 1).getEmail()).isEqualTo(user.getEmail());
        Assertions.assertThat(usersByRole.get(usersByRole.size() - 1).getPassword()).isEqualTo(user.getPassword());
        Assertions.assertThat(usersByRole.get(usersByRole.size() - 1).getRemoved()).isEqualTo(user.getRemoved());
        Assertions.assertThat(usersByRole.get(usersByRole.size() - 1).getRole()).isEqualTo(user.getRole());
    }

    @Test
    public void shouldUpdateUser() {
        //given
        User user = commonService.getAndSaveSampleUser();
        String userEmail = user.getEmail();
        User userWithFieldsToUpdate = user;
        String exampleFirstname = UUID.randomUUID().toString();
        String exampleLastname = UUID.randomUUID().toString();
        String exampleEmail = commonService.getSampleEmail();
        userWithFieldsToUpdate.setEmail(exampleEmail);

        //when
        userService.update(userWithFieldsToUpdate.getId(), userWithFieldsToUpdate);
        User userWithUpdatedFields = userService.getById(user.getId());

        //then
        Assertions.assertThat(userWithUpdatedFields).isNotEqualTo(user);
        Assertions.assertThat(userWithUpdatedFields.getId()).isEqualTo(user.getId());
        Assertions.assertThat(userWithUpdatedFields.getEmail()).isNotEqualTo(userEmail);
        Assertions.assertThat(userWithUpdatedFields.getEmail()).isEqualTo(exampleEmail);
    }

    @Test
    public void shouldUpdateUserPassword() {
        //given
        User user = commonService.getAndSaveSampleUser();
        String examplePassword = commonService.getSamplePassword();

        //when
        userService.updatePassword(user.getId(), examplePassword);
        User userWithUpdatedPassword = userService.getById(user.getId());

        //then
        //       Assertions.assertThat(userWithUpdatedPassword).isNotEqualTo(user);
        Assertions.assertThat(userWithUpdatedPassword.getId()).isEqualTo(user.getId());
        Assertions.assertThat(userWithUpdatedPassword.getPassword()).isNotEqualTo(user.getPassword());
        Assertions.assertThat(passwordEncoder.matches(examplePassword, userWithUpdatedPassword.getPassword())).isEqualTo(true);
        Assertions.assertThat(userWithUpdatedPassword.getEmail()).isEqualTo(user.getEmail());
        Assertions.assertThat(userWithUpdatedPassword.getRemoved()).isEqualTo(user.getRemoved());
        Assertions.assertThat(userWithUpdatedPassword.getRole()).isEqualTo(user.getRole());
    }

    @Test
    public void shouldSetRemovedUser() {
        //given
        User user = commonService.getAndSaveSampleUser();
        List<User> usersList = userService.getAll();

        //when
        userService.setRemoved(user.getId());
        User userAfterSettingRemoved = userService.getById(user.getId());
        List<User> actualList = userService.getAll();

        //then
        Assertions.assertThat(usersList.size() - 1).isEqualTo(actualList.size());
        Assertions.assertThat(userAfterSettingRemoved).isNull();
    }

    @Test
    public void shouldRemoveUser() {
        //given
        User user = commonService.getAndSaveSampleUser();
        List<User> usersList = userService.getAll();

        //when
        userService.remove(user.getId());
        List<User> actualList = userService.getAll();

        //then
        Assertions.assertThat(usersList.size() - 1).isEqualTo(actualList.size());
        Assertions.assertThat(userService.getById(user.getId())).isNull();
    }
}
