package gymApp.gym.commons.client;

import gymApp.gym.address.service.AddressService;
import gymApp.gym.client.service.ClientService;
import gymApp.gym.mail.service.EmailService;
import gymApp.gym.models.address.Address;
import gymApp.gym.models.client.Client;
import gymApp.gym.models.client.Gender;
import gymApp.gym.models.groupTraining.GroupTraining;
import gymApp.gym.models.loginToken.LoginToken;
import gymApp.gym.models.pass.Pass;
import gymApp.gym.models.tariff.Currency;
import gymApp.gym.models.tariff.SettlementType;
import gymApp.gym.models.tariff.Tariff;
import gymApp.gym.models.trainer.Trainer;
import gymApp.gym.models.user.User;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.pass.service.PassService;
import gymApp.gym.tariff.service.TariffService;
import gymApp.gym.trainer.service.TrainerService;
import gymApp.gym.user.service.UserService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.*;
import java.util.Random;
import java.util.UUID;

@Component
public class TestCommonService {

    @Autowired
    ClientService clientService;

    @Autowired
    TrainerService trainerService;

    @Autowired
    TariffService tariffService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserService userService;

    @Autowired
    PassService passService;

    @Autowired
    AddressService addressService;

    @Value("${admin.email}")
    String adminEmail;

    public Address getAndSaveSampleAddress() {
        return addressService.create(getSampleAddress());
    }

    public Address getSampleAddress() {
        return Address.builder()
                .country(randomUuidAsString())
                .state(randomUuidAsString())
                .city(randomUuidAsString())
                .street(randomUuidAsString())
                .houseNumber(UUID.randomUUID().getMostSignificantBits())
                .apartmentNumber(UUID.randomUUID().getMostSignificantBits())
                .postOffice(randomUuidAsString())
                .postalCode(randomUuidAsString())
                .client(clientService.create(getAndSaveSampleUser().getId(), getSampleClient()))
                .build();
    }

    public Client getAndSaveSampleClient() {
        return clientService.create(getAndSaveSampleUser().getId(), getSampleClient());
    }

    public Client getSampleClient() {
        Random rand = new Random();
        return Client.builder()
                .email(RandomStringUtils.randomAlphabetic(6) + "@" + RandomStringUtils.randomAlphabetic(4) + ".pl")
                .name(randomUuidAsString())
                .surname(randomUuidAsString())
                .birthdate(Instant.now().atZone(ZoneId.systemDefault()).toLocalDate().minusYears(18).atStartOfDay().toInstant(ZoneOffset.UTC))
                .pesel(getRandomIntegerAsString(11))
                .phoneNumber("123456789")
                .notifications(true)
                .gender(Gender.MAN)
                .removed(false)
                .removeReason(null)
                .userId(getAndSaveSampleUser().getId())
                .build();
    }

    public String getRandomIntegerAsString(Integer digitAmount) {
        String randomIntAsString = "";
        Random rand = new Random();
        for (int i = 0; i < digitAmount; i++) {
            int doubleValue = (rand.nextInt(10));
            randomIntAsString += doubleValue;
        }
        return randomIntAsString;
    }

    public User getAndSaveSampleUser() {
        return userService.create(getAdminUUID(), getSampleUser());
    }

    public User getSampleUser() {
        return User.builder()
                .email(RandomStringUtils.randomAlphabetic(6) + "@" + RandomStringUtils.randomAlphabetic(4) + ".pl")
                .password(RandomStringUtils.randomAlphabetic(4) + "A1@" + RandomStringUtils.randomAlphabetic(4))
                .role(UserRole.ADMIN)
                .createdBy(UUID.randomUUID())
                .removed(false)
                .build();
    }

    public Trainer getSampleTrainer() {
        return Trainer.builder()
                .email(randomUuidAsString() + "@test.pl")
                .name(randomUuidAsString())
                .surname(randomUuidAsString())
                .birthdate(LocalDate.now().minusYears(18))
                .pesel(getRandomIntegerAsString(11))
                .phoneNumber("0123456789")
                .creationDate(Instant.now().atZone(ZoneId.systemDefault()).toLocalDate().minusYears(18).atStartOfDay().toInstant(ZoneOffset.UTC))
                .removed(false)
                .description(randomUuidAsString())
                .trainingTypes(randomUuidAsString())
                .notifications(true)
                .showProfile(false)
                .gender(gymApp.gym.models.trainer.Gender.MAN)
                .imageId(null)
                .build();
    }

    public Trainer getAndSaveSampleTrainer() {
        return trainerService.create(getAndSaveSampleUser().getId(), getSampleTrainer());
    }

    public GroupTraining getSampleGroupTraining() {
        return GroupTraining.builder()
                .name(randomUuidAsString())
                .date(LocalDate.now())
                .startingTime(LocalTime.NOON)
                .durationInMinutes(50)
                .maxMembersAmount(30)
                .spotsLeft(30)
                .intensity(5)
                .createdBy(getAndSaveSampleUser().getId())
                .creationDate(Instant.now().atZone(ZoneId.systemDefault()).toLocalDate().minusYears(18).atStartOfDay().toInstant(ZoneOffset.UTC))
                .removed(false)
                .gender(gymApp.gym.models.groupTraining.Gender.MAN)
                .description(randomUuidAsString())
                .trainerId(trainerService.create(getAndSaveSampleUser().getId(), getSampleTrainer()).getId())
                .build();
    }

    public LoginToken getSampleLoginToken(String adminEmail, String token) {
        return LoginToken.builder()
                .id(UUID.randomUUID())
                .token(new BCryptPasswordEncoder().encode(token))
                .email(adminEmail)
                .creationDate(Instant.now())
                .removed(false)
                .build();
    }

    private Tariff getAndSaveSampleTariff() {
        Tariff tariff = Tariff.builder()
                .name(randomUuidAsString())
                .price(BigDecimal.TEN)
                .currency(Currency.PLN)
                .settlementType(SettlementType.MONTH)
                .creationDate(Instant.now())
                .removed(false)
                .build();
        return tariffService.create(getAndSaveSampleUser().getId(), tariff);
    }

    public Pass getAndSaveActiveSamplePass() {
        Tariff tariff = getAndSaveSampleTariff();
        Pass pass = Pass.builder()
                .tariffName(tariff.getName())
                .tariffSettlementType(tariff.getSettlementType())
                .totalPrice(tariff.getPrice())
                .currency(tariff.getCurrency())
                .activationDate(LocalDate.now())
                .creationDate(Instant.now())
                .removed(false)
                .clientId(getAndSaveSampleClient().getId())
                .build();
        return passService.create(tariff.getId(), getAndSaveSampleUser().getId(), pass);
    }

    public Pass getAndSaveNotActiveSamplePass() {
        Tariff tariff = getAndSaveSampleTariff();
        Pass pass = Pass.builder()
                .tariffName(tariff.getName())
                .tariffSettlementType(tariff.getSettlementType())
                .totalPrice(tariff.getPrice())
                .currency(tariff.getCurrency())
                .activationDate(LocalDate.now().minusDays(60))
                .creationDate(Instant.now())
                .removed(false)
                .clientId(getAndSaveSampleClient().getId())
                .build();
        return passService.create(tariff.getId(), getAndSaveSampleUser().getId(), pass);
    }

    private String randomUuidAsString() {
        return UUID.randomUUID().toString();
    }

    public String getSampleEmail() {
        return RandomStringUtils.randomAlphabetic(6) + "@" + RandomStringUtils.randomAlphabetic(4) + ".pl";
    }

    public String getSamplePassword() {
        return RandomStringUtils.randomAlphabetic(4) + "A1@" + RandomStringUtils.randomAlphabetic(4);
    }

    private UUID getAdminUUID() {
        return userService.getByEmail(adminEmail).getId();
    }
}
