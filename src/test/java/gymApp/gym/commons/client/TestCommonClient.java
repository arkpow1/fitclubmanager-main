package gymApp.gym.commons.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.http.ContentType;
import gymApp.gym.loginToken.mapper.LoginTokenMapper;
import gymApp.gym.models.loginToken.LoginToken;
import gymApp.gym.models.user.UserLoginTOHelper;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.tools.TestApplicationConfig;
import org.apache.http.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import static com.jayway.restassured.RestAssured.given;

@Component
public class TestCommonClient {

    @Autowired
    private LoginTokenMapper loginTokenMapper;

    @Autowired
    private TestCommonService commonService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    TestApplicationConfig applicationConfig;

    private static final String LOGIN_PATH = "/login";

    public String getAdminToken() throws JsonProcessingException {
        loginTokenMapper.removeByEmail(applicationConfig.getAdminEmail());
        LoginToken loginToken = commonService.getSampleLoginToken(applicationConfig.getAdminEmail(), "example");
        loginTokenMapper.create(loginToken);
        String json = objectMapper.writeValueAsString(UserLoginTOHelper.builder()
                .email(applicationConfig.getAdminEmail())
                .password(applicationConfig.getAdminPassword())
                .token("example")
                .role(UserRole.ADMIN)
                .build());
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .body(json)
                .when()
                .post(LOGIN_PATH)
                .then()
                .log()
                .ifError()
                .assertThat()
                .statusCode(200)
                .extract()
                .header(HttpHeaders.AUTHORIZATION);
    }
}
