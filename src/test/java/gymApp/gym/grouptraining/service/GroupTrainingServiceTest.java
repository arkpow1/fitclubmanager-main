package gymApp.gym.grouptraining.service;

import gymApp.gym.commons.client.TestCommonService;
import gymApp.gym.groupTraining.service.GroupTrainingService;
import gymApp.gym.mail.service.EmailService;
import gymApp.gym.models.groupTraining.GroupTraining;
import gymApp.gym.models.trainer.Gender;
import gymApp.gym.models.trainer.Trainer;
import gymApp.gym.trainer.service.TrainerService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"application.properties"})
@MockBean(EmailService.class)
public class GroupTrainingServiceTest {

    @Autowired
    TrainerService trainerService;

    @Autowired
    GroupTrainingService groupTrainingService;

    @Autowired
    TestCommonService commonService;

    @Test
    public void shouldCreateAndGetByGroupTrainingId() {
        //given
        List<GroupTraining> groupTrainingsList = groupTrainingService.getAll();
        GroupTraining groupTraining = commonService.getSampleGroupTraining();

        //when
        List<GroupTraining> createdGroupTrainings = groupTrainingService.create(commonService.getAndSaveSampleUser().getId(), groupTraining, null, null);
        List<GroupTraining> actualGroupTrainingsList = groupTrainingService.getAll();
        GroupTraining groupTrainingById = groupTrainingService.getById(createdGroupTrainings.get(0).getId());

        //then
        Assertions.assertThat(groupTrainingsList.size()).isEqualTo(actualGroupTrainingsList.size() - 1);
        Assertions.assertThat(groupTrainingById.getId()).isEqualTo(createdGroupTrainings.get(0).getId());
        Assertions.assertThat(groupTrainingById.getName()).isEqualTo(createdGroupTrainings.get(0).getName());
        Assertions.assertThat(groupTrainingById.isRemoved()).isEqualTo(createdGroupTrainings.get(0).isRemoved());
        Assertions.assertThat(groupTrainingById.getGender()).isEqualTo(createdGroupTrainings.get(0).getGender());
        Assertions.assertThat(groupTrainingById.getDescription()).isEqualTo(createdGroupTrainings.get(0).getDescription());
        Assertions.assertThat(groupTrainingById.getTrainerId()).isEqualTo(createdGroupTrainings.get(0).getTrainerId());
    }

    @Test
    public void shouldSetRemovedGroupTraining() {
        //given
        GroupTraining groupTraining = commonService.getSampleGroupTraining();
        List<GroupTraining> createdGroupTrainings = groupTrainingService.create(commonService.getAndSaveSampleUser().getId(), groupTraining, null, null);
        List<GroupTraining> groupTrainingsList = groupTrainingService.getAll();

        //when
        groupTrainingService.setRemoved(createdGroupTrainings.get(0).getId());
        GroupTraining groupTrainingAfterSettingRemoved = groupTrainingService.getById(createdGroupTrainings.get(0).getId());
        List<GroupTraining> actualList = groupTrainingService.getAll();

        //then
        Assertions.assertThat(groupTrainingsList.size() - 1).isEqualTo(actualList.size());
        Assertions.assertThat(groupTrainingAfterSettingRemoved).isNull();
    }

    @Test
    public void shouldRemoveGroupTraining() {
        //given
        GroupTraining groupTraining = commonService.getSampleGroupTraining();
        List<GroupTraining> createdGroupTrainings = groupTrainingService.create(commonService.getAndSaveSampleUser().getId(), groupTraining, null, null);
        List<GroupTraining> groupTrainingsList = groupTrainingService.getAll();

        //when
        groupTrainingService.deleteById(createdGroupTrainings.get(0).getId());
        List<GroupTraining> actualList = groupTrainingService.getAll();

        //then
        Assertions.assertThat(groupTrainingsList.size() - 1).isEqualTo(actualList.size());
        Assertions.assertThat(groupTrainingService.getById(createdGroupTrainings.get(0).getId())).isNull();
    }

    @Test
    public void shouldUpdateGroupTraining() {
        GroupTraining groupTraining = commonService.getSampleGroupTraining();
        List<GroupTraining> createdGroupTrainings = groupTrainingService.create(commonService.getAndSaveSampleUser().getId(), groupTraining, null, null);
        String groupTrainingName = createdGroupTrainings.get(0).getName();
        String groupTrainingDescription = createdGroupTrainings.get(0).getDescription();
        gymApp.gym.models.groupTraining.Gender groupTrainingGender = createdGroupTrainings.get(0).getGender();

        GroupTraining groupTrainingWithFieldsToUpdate = createdGroupTrainings.get(0);

        String exampleGroupTrainingName = UUID.randomUUID().toString();
        String exampleGroupTrainingDescription = UUID.randomUUID().toString();
        gymApp.gym.models.groupTraining.Gender exampleGroupTrainingGender = gymApp.gym.models.groupTraining.Gender.ALL;

        Trainer exampleGroupTrainingTrainer = commonService.getAndSaveSampleTrainer();

        groupTrainingWithFieldsToUpdate.setDescription(exampleGroupTrainingDescription);
        groupTrainingWithFieldsToUpdate.setGender(exampleGroupTrainingGender);
        groupTrainingWithFieldsToUpdate.setName(exampleGroupTrainingName);
        groupTrainingWithFieldsToUpdate.setTrainerId(exampleGroupTrainingTrainer.getId());

        //when
        groupTrainingService.update(null, groupTrainingWithFieldsToUpdate);
        GroupTraining groupTrainingWithUpdatedFields = groupTrainingService.getById(createdGroupTrainings.get(0).getId());

        //then
        Assertions.assertThat(groupTrainingWithUpdatedFields).isNotEqualTo(createdGroupTrainings.get(0));
        Assertions.assertThat(groupTrainingWithUpdatedFields.getId()).isEqualTo(createdGroupTrainings.get(0).getId());

        Assertions.assertThat(groupTrainingWithUpdatedFields.getDescription()).isNotEqualTo(groupTrainingDescription);
        Assertions.assertThat(groupTrainingWithUpdatedFields.getDescription()).isEqualTo(exampleGroupTrainingDescription);

        Assertions.assertThat(groupTrainingWithUpdatedFields.getGender()).isNotEqualTo(groupTrainingGender);
        Assertions.assertThat(groupTrainingWithUpdatedFields.getGender()).isEqualTo(exampleGroupTrainingGender);

        Assertions.assertThat(groupTrainingWithUpdatedFields.getTrainerId()).isEqualTo(exampleGroupTrainingTrainer.getId());

        Assertions.assertThat(groupTrainingWithUpdatedFields.getName()).isNotEqualTo(groupTrainingName);
        Assertions.assertThat(groupTrainingWithUpdatedFields.getName()).isEqualTo(exampleGroupTrainingName);
    }

}
