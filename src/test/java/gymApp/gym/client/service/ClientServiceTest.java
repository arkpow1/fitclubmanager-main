package gymApp.gym.client.service;

import gymApp.gym.commons.client.TestCommonService;
import gymApp.gym.mail.service.EmailService;
import gymApp.gym.models.client.Client;
import gymApp.gym.models.client.RemoveReason;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"application.properties"})
@MockBean(EmailService.class)
public class ClientServiceTest {

    @Autowired
    TestCommonService commonService;

    @Autowired
    ClientService clientService;

    @Test
    public void shouldCreateAndGetClientById() {
        //given
        List<Client> clientsList = clientService.getAll();
        Client client = commonService.getAndSaveSampleClient();

        //when
        List<Client> actualClientsList = clientService.getAll();
        Client clientById = clientService.getById(client.getId());

        //then
        Assertions.assertThat(clientsList.size()).isEqualTo(actualClientsList.size() - 1);
        Assertions.assertThat(clientById.getId()).isEqualTo(client.getId());
        Assertions.assertThat(clientById.getEmail()).isEqualTo(client.getEmail());
        Assertions.assertThat(clientById.getNumber()).hasSize(8);
        Assertions.assertThat(clientById.getName()).isEqualTo(client.getName());
        Assertions.assertThat(clientById.getSurname()).isEqualTo(client.getSurname());
        Assertions.assertThat(clientById.getPesel()).isEqualTo(client.getPesel());
        Assertions.assertThat(clientById.getPhoneNumber()).isEqualTo(client.getPhoneNumber());
        Assertions.assertThat(clientById.getRemoveReason()).isEqualTo(client.getRemoveReason());
        Assertions.assertThat(clientById.isNotifications()).isEqualTo(client.isNotifications());
        Assertions.assertThat(clientById.getGender()).isEqualTo(client.getGender());
        Assertions.assertThat(clientById.isRemoved()).isEqualTo(client.isRemoved());
    }

    @Test
    public void shouldCreateAndGetClientByEmail() {
        //given
        List<Client> clientsList = clientService.getAll();
        Client client = commonService.getAndSaveSampleClient();

        //when
        List<Client> actualClientsList = clientService.getAll();
        Client clientByEmail = clientService.getByEmail(client.getEmail());

        //then
        Assertions.assertThat(clientsList.size()).isEqualTo(actualClientsList.size() - 1);
        Assertions.assertThat(clientByEmail.getId()).isEqualTo(client.getId());
        Assertions.assertThat(clientByEmail.getEmail()).isEqualTo(client.getEmail());
        Assertions.assertThat(clientByEmail.getNumber()).hasSize(8);
        Assertions.assertThat(clientByEmail.getName()).isEqualTo(client.getName());
        Assertions.assertThat(clientByEmail.getSurname()).isEqualTo(client.getSurname());
        Assertions.assertThat(clientByEmail.getPesel()).isEqualTo(client.getPesel());
        Assertions.assertThat(clientByEmail.getPhoneNumber()).isEqualTo(client.getPhoneNumber());
        Assertions.assertThat(clientByEmail.getRemoveReason()).isEqualTo(client.getRemoveReason());
        Assertions.assertThat(clientByEmail.isNotifications()).isEqualTo(client.isNotifications());
        Assertions.assertThat(clientByEmail.getGender()).isEqualTo(client.getGender());
        Assertions.assertThat(clientByEmail.isRemoved()).isEqualTo(client.isRemoved());
    }

    public void shouldCreateAndGetClientWithNotifications() {
        //given
        List<Client> clientsList = clientService.getAll();
        Client client = commonService.getAndSaveSampleClient();

        //when
        List<Client> actualClientsList = clientService.getAll();
        List<Client> clientsWithNotifications = clientService.getWithNotifications();

        //then
        Assertions.assertThat(clientsList.size()).isEqualTo(actualClientsList.size() - 1);
        Assertions.assertThat(clientsWithNotifications.size()).isGreaterThan(0);
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).getId()).isEqualTo(client.getId());
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).getEmail()).isEqualTo(client.getEmail());
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).getName()).isEqualTo(client.getName());
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).getSurname()).isEqualTo(client.getSurname());
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).getPesel()).isEqualTo(client.getPesel());
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).getPhoneNumber()).isEqualTo(client.getPhoneNumber());
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).getRemoveReason()).isEqualTo(client.getRemoveReason());
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).isNotifications()).isEqualTo(client.isNotifications());
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).getGender()).isEqualTo(client.getGender());
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).isRemoved()).isEqualTo(client.isRemoved());
    }

    @Test
    public void shouldUpdateClient() {
        //given
        Client client = commonService.getAndSaveSampleClient();
        Boolean clientNotifications = client.isNotifications();
        String clientSurname = client.getSurname();
        String clientEmail = client.getEmail();
        Client clientWithFieldsToUpdate = client;
        clientWithFieldsToUpdate.setNotifications(false);
        String exampleEmail = UUID.randomUUID().toString() + "@test.pl";
        clientWithFieldsToUpdate.setEmail(exampleEmail);

        //when
        clientService.update(client.getUserId(), clientWithFieldsToUpdate);
        Client clientWithUpdatedFields = clientService.getById(client.getId());

        //then
        Assertions.assertThat(clientWithUpdatedFields).isNotEqualTo(client);
        Assertions.assertThat(clientWithUpdatedFields.getId()).isEqualTo(client.getId());
        Assertions.assertThat(clientWithUpdatedFields.isNotifications()).isNotEqualTo(clientNotifications);
        Assertions.assertThat(clientWithUpdatedFields.isNotifications()).isEqualTo(false);
        Assertions.assertThat(clientWithUpdatedFields.getEmail()).isNotEqualTo(clientEmail);
        Assertions.assertThat(clientWithUpdatedFields.getEmail()).isEqualTo(exampleEmail);
    }

    @Test
    public void shouldSetRemovedClient() {
        //given
        Client client = commonService.getAndSaveSampleClient();
        List<Client> clientsList = clientService.getAll();

        //when
        clientService.setRemoved(client.getId(), RemoveReason.GYMCHANGE);
        Client clientAfterSettingRemoved = clientService.getById(client.getId());
        List<Client> actualList = clientService.getAll();

        //then
        Assertions.assertThat(clientsList.size() - 1).isEqualTo(actualList.size());
        Assertions.assertThat(clientAfterSettingRemoved).isNull();
    }

    @Test
    public void shouldRemoveClient() {
        //given
        Client client = commonService.getAndSaveSampleClient();
        List<Client> clientsList = clientService.getAll();

        //when
        clientService.deleteById(client.getId());
        List<Client> actualList = clientService.getAll();

        //then
        Assertions.assertThat(clientsList.size() - 1).isEqualTo(actualList.size());
        Assertions.assertThat(clientService.getById(client.getId())).isNull();
    }
}
