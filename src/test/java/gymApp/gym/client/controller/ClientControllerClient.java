package gymApp.gym.client.controller;

import com.jayway.restassured.http.ContentType;
import org.apache.http.HttpHeaders;

import static com.jayway.restassured.RestAssured.given;

public class ClientControllerClient {

    private static final String CLIENT_PATH = "/clients";

    public static String getAll(String token) {
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .when()
                .header(HttpHeaders.AUTHORIZATION, token)
                .get(CLIENT_PATH)
                .then()
                .log()
                .ifError()
                .assertThat()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .print();
    }

    static String setRemoved(String id, String removeReason, String token) {
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .when()
                .header(HttpHeaders.AUTHORIZATION, token)
                .delete(CLIENT_PATH + "/setRemoved/" + id + "/" + removeReason)
                .then()
                .log()
                .ifError()
                .assertThat()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .print();
    }

    static String deleteById(String id, String token) {
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .when()
                .header(HttpHeaders.AUTHORIZATION, token)
                .delete(CLIENT_PATH + "/remove/" + id)
                .then()
                .log()
                .ifError()
                .assertThat()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .print();
    }

    public static String create(String json, String token) {
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .when()
                .header(HttpHeaders.AUTHORIZATION, token)
                .body(json)
                .post(CLIENT_PATH)
                .then()
                .log()
                .ifError()
                .assertThat()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .print();
    }

    static String update(String json, String token) {
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .when()
                .header(HttpHeaders.AUTHORIZATION, token)
                .body(json)
                .patch(CLIENT_PATH)
                .then()
                .log()
                .ifError()
                .assertThat()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .print();
    }

    static String getById(String id, String token) {
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .when()
                .header(HttpHeaders.AUTHORIZATION, token)
                .get(CLIENT_PATH + "/id/" + id)
                .then()
                .log()
                .ifError()
                .assertThat()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .print();
    }

    static String getByEmail(String email, String token) {
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .when()
                .header(HttpHeaders.AUTHORIZATION, token)
                .param("email", email)
                .get(CLIENT_PATH + "/email")
                .then()
                .log()
                .ifError()
                .assertThat()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .print();
    }

    static String getByType(String type, String token) {
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .when()
                .header(HttpHeaders.AUTHORIZATION, token)
                .get(CLIENT_PATH + "/type/" + type)
                .then()
                .log()
                .ifError()
                .assertThat()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .print();
    }

    static String getWithNotifications(String token) {
        return given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .with()
                .when()
                .header(HttpHeaders.AUTHORIZATION, token)
                .get(CLIENT_PATH + "/withNotifications")
                .then()
                .log()
                .ifError()
                .assertThat()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .print();
    }
}
