package gymApp.gym.client.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import gymApp.gym.GymApplication;
import gymApp.gym.commons.client.TestCommonClient;
import gymApp.gym.commons.client.TestCommonService;
import gymApp.gym.mail.service.EmailService;
import gymApp.gym.models.client.Client;
import gymApp.gym.models.client.RemoveReason;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD;

@RunWith(SpringRunner.class)
@DirtiesContext(classMode = AFTER_EACH_TEST_METHOD)
@SpringBootTest(classes = {GymApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@MockBean(EmailService.class)
public class ClientControllerTest {

    @Autowired
    TestCommonClient commonClient;

    @Autowired
    TestCommonService commonService;

    @Autowired
    ObjectMapper objectMapper;

    private String token;

    @Before
    public void before() throws JsonProcessingException {
        token = commonClient.getAdminToken();
    }

    @Test
    public void shouldCreateAndGetClientById() throws IOException {
        //given
        List<Client> clientsList = objectMapper.readValue(ClientControllerClient.getAll(token), new TypeReference<List<Client>>() {
        });
        Client client = objectMapper.readValue(ClientControllerClient.create(objectMapper.writeValueAsString(commonService.getSampleClient()), token), Client.class);

        //when
        List<Client> actualClientsList = objectMapper.readValue(ClientControllerClient.getAll(token), new TypeReference<List<Client>>() {
        });
        Client clientById = objectMapper.readValue(ClientControllerClient.getById(client.getId().toString(), token), Client.class);

        //then
        Assertions.assertThat(clientsList.size()).isEqualTo(actualClientsList.size() - 1);
        Assertions.assertThat(clientById.getId()).isEqualTo(client.getId());
        Assertions.assertThat(clientById.getEmail()).isEqualTo(client.getEmail());
        Assertions.assertThat(clientById.getName()).isEqualTo(client.getName());
        Assertions.assertThat(clientById.getSurname()).isEqualTo(client.getSurname());
        Assertions.assertThat(clientById.getPesel()).isEqualTo(client.getPesel());
        Assertions.assertThat(clientById.getPhoneNumber()).isEqualTo(client.getPhoneNumber());
        Assertions.assertThat(clientById.getRemoveReason()).isEqualTo(client.getRemoveReason());
        Assertions.assertThat(clientById.isNotifications()).isEqualTo(client.isNotifications());
        Assertions.assertThat(clientById.getGender()).isEqualTo(client.getGender());
        Assertions.assertThat(clientById.isRemoved()).isEqualTo(client.isRemoved());
    }

    @Test
    public void shouldCreateAndGetClientByEmail() throws IOException {
        //given
        List<Client> clientsList = objectMapper.readValue(ClientControllerClient.getAll(token), new TypeReference<List<Client>>() {
        });
        Client client = objectMapper.readValue(ClientControllerClient.create(objectMapper.writeValueAsString(commonService.getSampleClient()), token), Client.class);

        //when
        List<Client> actualClientsList = objectMapper.readValue(ClientControllerClient.getAll(token), new TypeReference<List<Client>>() {
        });
        Client clientByEmail = objectMapper.readValue(ClientControllerClient.getByEmail(client.getEmail(), token), Client.class);

        //then
        Assertions.assertThat(clientsList.size()).isEqualTo(actualClientsList.size() - 1);
        Assertions.assertThat(clientByEmail.getId()).isEqualTo(client.getId());
        Assertions.assertThat(clientByEmail.getEmail()).isEqualTo(client.getEmail());
        Assertions.assertThat(clientByEmail.getName()).isEqualTo(client.getName());
        Assertions.assertThat(clientByEmail.getSurname()).isEqualTo(client.getSurname());
        Assertions.assertThat(clientByEmail.getPesel()).isEqualTo(client.getPesel());
        Assertions.assertThat(clientByEmail.getPhoneNumber()).isEqualTo(client.getPhoneNumber());
        Assertions.assertThat(clientByEmail.getRemoveReason()).isEqualTo(client.getRemoveReason());
        Assertions.assertThat(clientByEmail.isNotifications()).isEqualTo(client.isNotifications());
        Assertions.assertThat(clientByEmail.getGender()).isEqualTo(client.getGender());
        Assertions.assertThat(clientByEmail.isRemoved()).isEqualTo(client.isRemoved());
    }

    public void shouldCreateAndGetClientWithNotifications() throws IOException {
        //given
        List<Client> clientsList = objectMapper.readValue(ClientControllerClient.getAll(token), new TypeReference<List<Client>>() {
        });
        Client client = objectMapper.readValue(ClientControllerClient.create(objectMapper.writeValueAsString(commonService.getSampleClient()), token), Client.class);

        //when
        List<Client> actualClientsList = objectMapper.readValue(ClientControllerClient.getAll(token), new TypeReference<List<Client>>() {
        });
        List<Client> clientsWithNotifications = objectMapper.readValue(ClientControllerClient.getWithNotifications(token), new TypeReference<List<Client>>() {
        });

        //then
        Assertions.assertThat(clientsList.size()).isEqualTo(actualClientsList.size() - 1);
        Assertions.assertThat(clientsWithNotifications.size()).isGreaterThan(0);
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).getId()).isEqualTo(client.getId());
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).getEmail()).isEqualTo(client.getEmail());
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).getName()).isEqualTo(client.getName());
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).getSurname()).isEqualTo(client.getSurname());
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).getPesel()).isEqualTo(client.getPesel());
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).getPhoneNumber()).isEqualTo(client.getPhoneNumber());
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).getRemoveReason()).isEqualTo(client.getRemoveReason());
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).isNotifications()).isEqualTo(client.isNotifications());
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).getGender()).isEqualTo(client.getGender());
        Assertions.assertThat(clientsWithNotifications.get(clientsWithNotifications.size() - 1).isRemoved()).isEqualTo(client.isRemoved());
    }

    @Test
    public void shouldUpdateClient() throws IOException {
        //given
        Client client = objectMapper.readValue(ClientControllerClient.create(objectMapper.writeValueAsString(commonService.getSampleClient()), token), Client.class);
        Boolean clientNotifications = client.isNotifications();
        String clientEmail = client.getEmail();
        client.setNotifications(false);
        String exampleEmail = UUID.randomUUID().toString() + "@test.pl";
        client.setEmail(exampleEmail);

        //when
        ClientControllerClient.update(objectMapper.writeValueAsString(client), token);
        Client clientWithUpdatedFields = objectMapper.readValue(ClientControllerClient.getById(client.getId().toString(), token), Client.class);

        //then
        Assertions.assertThat(clientWithUpdatedFields).isNotEqualTo(client);
        Assertions.assertThat(clientWithUpdatedFields.getId()).isEqualTo(client.getId());
        Assertions.assertThat(clientWithUpdatedFields.isNotifications()).isNotEqualTo(clientNotifications);
        Assertions.assertThat(clientWithUpdatedFields.isNotifications()).isEqualTo(false);
        Assertions.assertThat(clientWithUpdatedFields.getEmail()).isNotEqualTo(clientEmail);
        Assertions.assertThat(clientWithUpdatedFields.getEmail()).isEqualTo(exampleEmail);
    }

    @Test
    public void shouldSetRemovedClient() throws IOException {
        //given
        Client client = objectMapper.readValue(ClientControllerClient.create(objectMapper.writeValueAsString(commonService.getSampleClient()), token), Client.class);
        List<Client> clientsList = objectMapper.readValue(ClientControllerClient.getAll(token), new TypeReference<List<Client>>() {
        });

        //when
        ClientControllerClient.setRemoved(client.getId().toString(), RemoveReason.GYMCHANGE.toString(), token);
        List<Client> actualList = objectMapper.readValue(ClientControllerClient.getAll(token), new TypeReference<List<Client>>() {
        });

        //then
        Assertions.assertThat(clientsList.size() - 1).isEqualTo(actualList.size());
        Assertions.assertThat(ClientControllerClient.getById(client.getId().toString(), token)).isEmpty();
    }

    @Test
    public void shouldRemoveClient() throws IOException {
        //given
        Client client = objectMapper.readValue(ClientControllerClient.create(objectMapper.writeValueAsString(commonService.getSampleClient()), token), Client.class);
        List<Client> clientsList = objectMapper.readValue(ClientControllerClient.getAll(token), new TypeReference<List<Client>>() {
        });

        //when
        ClientControllerClient.deleteById(client.getId().toString(), token);
        List<Client> actualList = objectMapper.readValue(ClientControllerClient.getAll(token), new TypeReference<List<Client>>() {
        });

        //then
        Assertions.assertThat(clientsList.size() - 1).isEqualTo(actualList.size());
        Assertions.assertThat(ClientControllerClient.getById(client.getId().toString(), token)).isEmpty();
    }
}
