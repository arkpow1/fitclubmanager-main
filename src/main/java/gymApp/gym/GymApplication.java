package gymApp.gym;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;

@ComponentScan("gymApp.gym")
@MapperScan("gymApp.gym")
@Controller
@SpringBootApplication
@EnableScheduling
public class GymApplication {


    @Autowired
    public GymApplication() {
    }

    public static void main(String[] args) {
        SpringApplication.run(GymApplication.class, args);
    }

}
