package gymApp.gym.notification.service;

import gymApp.gym.groupTraining.service.GroupTrainingService;
import gymApp.gym.groupTrainingMember.service.GroupTrainingMemberService;
import gymApp.gym.individualTraining.service.IndividualTrainingService;
import gymApp.gym.mail.service.EmailService;
import gymApp.gym.models.groupTrainingMember.GroupTrainingMember;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class NotificationService {

    private final EmailService emailService;
    private final IndividualTrainingService individualTrainingService;
    private final GroupTrainingService groupTrainingService;
    private final GroupTrainingMemberService groupTrainingMemberService;

    @Value("${individual.training.notification.email.subject}")
    private String individualTrainingNotificationEmailSubject;
    @Value("${individual.training.notification.email.text}")
    private String individualTrainingNotificationEmailText;
    @Value("${group.training.notification.email.subject}")
    private String groupTrainingNotificationEmailSubject;
    @Value("${group.training.notification.email.text}")
    private String groupTrainingNotificationEmailText;

    @Autowired
    public NotificationService(EmailService emailService, IndividualTrainingService individualTrainingService, GroupTrainingService groupTrainingService, GroupTrainingMemberService groupTrainingMemberService) {
        this.emailService = emailService;
        this.individualTrainingService = individualTrainingService;
        this.groupTrainingService = groupTrainingService;
        this.groupTrainingMemberService = groupTrainingMemberService;
    }

    @Scheduled(cron = "0 0 8 * * *")
    private void sendIndividualTrainingNotifications() {
        individualTrainingService.getAll().stream().filter(it ->
                it.getDate().atStartOfDay().isEqual(LocalDateTime.now().toLocalDate().atStartOfDay())
        ).forEach(it -> {
            String email = it.getClient().getEmail();
            emailService.sendSimpleMessage(email, individualTrainingNotificationEmailSubject, individualTrainingNotificationEmailText);
        });
        groupTrainingService.getAll().stream().filter(it ->
                it.getDate().atStartOfDay().isEqual(LocalDateTime.now().toLocalDate().atStartOfDay())
        ).forEach(it -> {
            List<GroupTrainingMember> trainingMembers = groupTrainingMemberService.getAllByGroupTrainingId(it.getId());
            trainingMembers.forEach(gtm -> {
                String email = gtm.getClient().getEmail();
                emailService.sendSimpleMessage(email, groupTrainingNotificationEmailSubject, groupTrainingNotificationEmailText);
            });
        });
    }
}
