package gymApp.gym.client.controller;


import gymApp.gym.client.service.ClientService;
import gymApp.gym.models.client.Client;
import gymApp.gym.models.client.RemoveReason;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.security.PermissionChecker;
import gymApp.gym.security.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@EnableWebMvc
@RestController
@RequestMapping(value = "clients")
public class ClientController {

    private final ClientService clientService;
    private final PermissionChecker permissionChecker;
    private final TokenService tokenService;

    @Autowired
    public ClientController(ClientService clientService, PermissionChecker permissionChecker, TokenService tokenService) {
        this.clientService = clientService;
        this.permissionChecker = permissionChecker;
        this.tokenService = tokenService;
    }

    @GetMapping
    public List<Client> getAll(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return clientService.getAll();
    }

    @DeleteMapping(value = "/setRemoved/{id}/{removeReason}")
    public ResponseEntity setRemoved(@PathVariable("id") UUID id,
                                     @PathVariable RemoveReason removeReason,
                                     HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        clientService.setRemoved(id, removeReason);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/setRemovedSelf/{removeReason}")
    public ResponseEntity setRemovedSelf(@PathVariable RemoveReason removeReason,
                                         HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.CLIENT_ONLY);
        clientService.setRemovedSelf(tokenService.getUserId(request), removeReason);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/remove/{id}")
    public ResponseEntity deleteById(@PathVariable UUID id,
                                     HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        clientService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping
    public Client create(@RequestBody Client client,
                         HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return clientService.create(tokenService.getUserId(request), client);
    }

    @PatchMapping
    public Client update(@RequestBody Client client,
                         HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return clientService.update(tokenService.getUserId(request), client);
    }

    @PatchMapping(value = "/self")
    public Client updateSelf(@RequestBody Client client,
                             HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.CLIENT_ONLY);
        return clientService.updateSelf(tokenService.getUserId(request), client);
    }

    @GetMapping(value = "/id/{id}")
    public Client getById(@PathVariable UUID id,
                          HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return clientService.getById(id);
    }

    @GetMapping(value = "/self")
    public Client getSelf(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.CLIENT_ONLY);
        return clientService.getByUserId(tokenService.getUserId(request));
    }

    @GetMapping(value = "/email")
    public Client getByEmail(@RequestParam String email,
                             HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return clientService.getByEmail(email);
    }

    @GetMapping(value = "/withNotifications")
    public List<Client> getWithNotifications(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return clientService.getWithNotifications();
    }
}
