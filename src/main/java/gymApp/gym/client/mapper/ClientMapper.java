package gymApp.gym.client.mapper;

import gymApp.gym.models.client.Client;
import gymApp.gym.models.client.RemoveReason;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ClientMapper {

    List<Client> getAll();

    Client getById(@Param("id") UUID id);

    Client getByUserId(@Param("userId") UUID id);

    Client getByEmail(@Param("email") String email);

    List<Client> getWithNotifications();

    void setRemoved(@Param("id") UUID id, @Param("removeReason") RemoveReason removeReason);

    void deleteById(@Param("id") UUID id);

    void create(Client client);

    void update(Client client);

}
