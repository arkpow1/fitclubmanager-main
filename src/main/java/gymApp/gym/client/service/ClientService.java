package gymApp.gym.client.service;

import gymApp.gym.client.mapper.ClientMapper;
import gymApp.gym.models.client.Client;
import gymApp.gym.models.client.RemoveReason;
import gymApp.gym.models.user.User;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Service
public class ClientService {

    private final ClientMapper clientMapper;
    private final UserService userService;

    @Value("${client.number.digit.amount}")
    private Integer clientNumberDigitAmount;

    @Autowired
    public ClientService(ClientMapper clientMapper, UserService userService) {
        this.clientMapper = clientMapper;
        this.userService = userService;
    }

    public List<Client> getAll() {
        return clientMapper.getAll();
    }

    public void setRemovedSelf(UUID userId, RemoveReason removeReason) {
        Client client = getByUserId(userId);
        clientMapper.setRemoved(getByUserId(userId).getId(), removeReason);
        userService.setRemoved(client.getUserId());
    }

    public void setRemoved(UUID id, RemoveReason removeReason) {
        Client client = getById(id);
        clientMapper.setRemoved(id, removeReason);
        userService.setRemoved(client.getUserId());
    }

    public void deleteById(UUID id) {
        Client client = getById(id);
        clientMapper.deleteById(id);
        userService.remove(client.getUserId());
    }

    @Transactional
    public Client create(UUID createdBy, Client clientTemplate) {
        User user = userService.create(createdBy, User.builder().email(clientTemplate.getEmail()).role(UserRole.CLIENT).build());
        clientTemplate.setUserId(userService.getByEmail(user.getEmail()).getId());
        Client client = prepareClientToSave(clientTemplate);
        validateClient(client);
        clientMapper.create(client);
        return client;
    }

    private Client prepareClientToSave(Client clientTemplate) {
        return Client.builder()
                .id(UUID.randomUUID())
                .email(clientTemplate.getEmail())
                .number(getRandomIntegerAsString(clientNumberDigitAmount))
                .name(clientTemplate.getName())
                .surname(clientTemplate.getSurname())
                .birthdate(clientTemplate.getBirthdate())
                .pesel(null)
                .phoneNumber(clientTemplate.getPhoneNumber())
                .notifications(clientTemplate.isNotifications())
                .gender(clientTemplate.getGender())
                .creationDate(Instant.now())
                .removed(false)
                .removeReason(null)
                .userId(clientTemplate.getUserId())
                .build();
    }

    @Transactional
    public Client update(UUID userId, Client clientTemplate) {
        Client client = prepareClientToUpdate(clientMapper.getById(clientTemplate.getId()), clientTemplate);
        validateClient(client);
        clientMapper.update(client);
        userService.updateEmail(client.getUserId(), client.getEmail());
        return client;
    }

    @Transactional
    public Client updateSelf(UUID userId, Client clientTemplate) {
        Client client = prepareClientToUpdate(clientMapper.getByUserId(userId), clientTemplate);
        validateClient(client);
        clientMapper.update(client);
        userService.updateEmail(client.getUserId(), client.getEmail());
        return client;
    }

    private Client prepareClientToUpdate(Client existingClient, Client clientTemplate) {
        return Client.builder()
                .id(existingClient.getId())
                .email(clientTemplate.getEmail())
                .number(existingClient.getNumber())
                .name(existingClient.getName())
                .surname(existingClient.getSurname())
                .birthdate(existingClient.getBirthdate())
                .pesel(null)
                .phoneNumber(clientTemplate.getPhoneNumber())
                .notifications(clientTemplate.isNotifications())
                .gender(existingClient.getGender())
                .creationDate(existingClient.getCreationDate())
                .removed(existingClient.isRemoved())
                .removeReason(clientTemplate.getRemoveReason())
                .userId(existingClient.getUserId())
                .build();
    }

    private void validateClient(Client client) {
        if (!client.getPhoneNumber().matches("[0-9]+"))
            throw new IllegalStateException("Wrong credentials");
    }

    public String getRandomIntegerAsString(Integer digitAmount) {
        String randomIntAsString = "";
        Random rand = new Random();
        for (int i = 0; i < digitAmount; i++) {
            int doubleValue = (rand.nextInt(10));
            randomIntAsString += doubleValue;
        }
        return randomIntAsString;
    }

    public Client getById(UUID id) {
        return clientMapper.getById(id);
    }

    public Client getByUserId(UUID userId) {
        return clientMapper.getByUserId(userId);
    }

    public Client getByEmail(String email) {
        return clientMapper.getByEmail(email);
    }

    public List<Client> getWithNotifications() {
        return clientMapper.getWithNotifications();
    }
}
