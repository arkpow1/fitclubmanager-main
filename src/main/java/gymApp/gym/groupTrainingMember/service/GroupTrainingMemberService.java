package gymApp.gym.groupTrainingMember.service;

import gymApp.gym.client.service.ClientService;
import gymApp.gym.groupTraining.mapper.GroupTrainingMapper;
import gymApp.gym.groupTrainingMember.exception.GroupTrainingNotPresentException;
import gymApp.gym.groupTrainingMember.exception.ManyGroupTrainingsInSameTimeException;
import gymApp.gym.groupTrainingMember.mapper.GroupTrainingMemberMapper;
import gymApp.gym.models.client.Client;
import gymApp.gym.models.groupTraining.GroupTraining;
import gymApp.gym.models.groupTrainingMember.GroupTrainingMember;
import gymApp.gym.models.restriction.RestrictedAction;
import gymApp.gym.restriction.service.RestrictionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class GroupTrainingMemberService {
    private final GroupTrainingMemberMapper groupTrainingMemberMapper;
    private final ClientService clientService;
    private final GroupTrainingMapper groupTrainingMapper;
    private final RestrictionService restrictionService;

    @Autowired
    public GroupTrainingMemberService(GroupTrainingMemberMapper groupTrainingMemberMapper, ClientService clientService, GroupTrainingMapper groupTrainingMapper, RestrictionService restrictionService) {
        this.groupTrainingMemberMapper = groupTrainingMemberMapper;
        this.clientService = clientService;
        this.groupTrainingMapper = groupTrainingMapper;
        this.restrictionService = restrictionService;
    }

    public List<GroupTrainingMember> getAllByGroupTrainingId(UUID groupTrainingId) {
        return groupTrainingMemberMapper.getAllByGroupTrainingId(groupTrainingId);
    }

    public Integer countAllByGroupTrainingId(UUID groupTrainingId) {
        return groupTrainingMemberMapper.countAllByGroupTrainingId(groupTrainingId);
    }

    public GroupTrainingMember getAllByGroupTrainingIdAndClientId(UUID groupTrainingId, UUID clientId) {
        return groupTrainingMemberMapper.getByGroupTrainingIdAndClientId(groupTrainingId, clientId);
    }

    public boolean checkMembership(UUID groupTrainingId, UUID clientId) {
        return groupTrainingMemberMapper.getByGroupTrainingIdAndClientId(groupTrainingId, clientId) != null;
    }

    public void deleteById(UUID id) {
        checkIfGroupTrainingIsPresentOrThrow(groupTrainingMemberMapper.getById(id).getGroupTrainingId());
        groupTrainingMemberMapper.deleteById(id);
    }

    public void deleteByGroupTrainingIdAndUserId(UUID groupTrainingId, UUID userId) {
        checkIfGroupTrainingIsPresentOrThrow(groupTrainingId);
        groupTrainingMemberMapper.deleteByGroupTrainingIdAndClientId(clientService.getByUserId(userId).getId(), groupTrainingId);
    }

    public GroupTrainingMember createSelf(UUID userId, GroupTrainingMember groupTrainingMemberTemplate) {
        restrictionService.checkIfActionIsRestrictedOrThrow(RestrictedAction.GROUP_TRAINING_REGISTRATION, userId);
        Client client = clientService.getByUserId(userId);
        checkIfClientCanJoinGroupTrainingOrThrow(groupTrainingMemberTemplate.getGroupTrainingId(), client.getId());
        groupTrainingMemberTemplate.setClientId(client.getId());
        GroupTrainingMember groupTrainingMember = prepareGroupTrainingMemberToCreate(groupTrainingMemberTemplate);
        groupTrainingMemberMapper.create(groupTrainingMember);
        return groupTrainingMember;
    }

    public GroupTrainingMember create(UUID userId, GroupTrainingMember groupTrainingMemberTemplate) {
        checkIfClientCanJoinGroupTrainingOrThrow(groupTrainingMemberTemplate.getGroupTrainingId(), groupTrainingMemberTemplate.getClientId());
        GroupTrainingMember groupTrainingMember = prepareGroupTrainingMemberToCreate(groupTrainingMemberTemplate);
        groupTrainingMemberMapper.create(groupTrainingMember);
        return groupTrainingMember;
    }

    private GroupTrainingMember prepareGroupTrainingMemberToCreate(GroupTrainingMember groupTrainingMemberTemplate) {
        return GroupTrainingMember.builder()
                .id(UUID.randomUUID())
                .creationDate(Instant.now())
                .removed(false)
                .groupTrainingId(groupTrainingMemberTemplate.getGroupTrainingId())
                .clientId(groupTrainingMemberTemplate.getClientId())
                .build();
    }

    private boolean checkIfGroupTrainingIsPresentOrThrow(UUID groupTrainingId) {
        GroupTraining groupTraining = groupTrainingMapper.getById(groupTrainingId);
        LocalDateTime groupTrainingDate = LocalDateTime.of(groupTraining.getDate(), groupTraining.getStartingTime());
        if (LocalDateTime.now().isBefore(groupTrainingDate))
            return true;
        throw new GroupTrainingNotPresentException("Group training is not present");
    }

    private boolean checkIfClientCanJoinGroupTrainingOrThrow(UUID groupTrainingId, UUID clientId) {
        GroupTraining groupTraining = groupTrainingMapper.getById(groupTrainingId);
        checkIfGroupTrainingIsPresentOrThrow(groupTraining.getId());
        List<GroupTraining> groupTrainingsWithSameDate = groupTrainingMapper.getByDate(groupTraining.getDate());
        groupTrainingsWithSameDate.forEach(it -> {
                    if (checkMembership(it.getId(), clientId))
                        checkIfGroupTrainingIsTakingPlaceDuringOtherGroupTrainingOrThrow(groupTraining, it);
                }
        );
        return true;
    }

    private boolean checkIfGroupTrainingIsTakingPlaceDuringOtherGroupTrainingOrThrow(GroupTraining groupTraining, GroupTraining otherGroupTraining) {
        if (!isStartingTimeDuringOtherGroupTraining(groupTraining, otherGroupTraining)
                && !isEndingTimeDuringOtherGroupTraining(groupTraining, otherGroupTraining)
                && !isStartingTimeBeforeAndEndingTimeAfterOtherGroupTraining(groupTraining, otherGroupTraining))
            return true;
        throw new ManyGroupTrainingsInSameTimeException("Cannot join many group trainings taking place during same time");
    }

    private boolean isStartingTimeDuringOtherGroupTraining(GroupTraining groupTraining, GroupTraining otherGroupTraining) {
        if (LocalDateTime.of(groupTraining.getDate(), groupTraining.getStartingTime())
                .isEqual(LocalDateTime.of(otherGroupTraining.getDate(), otherGroupTraining.getStartingTime())))
            return true;
        return (LocalDateTime.of(groupTraining.getDate(), groupTraining.getStartingTime())
                .isAfter(LocalDateTime.of(otherGroupTraining.getDate(), otherGroupTraining.getStartingTime()))
                && LocalDateTime.of(groupTraining.getDate(), groupTraining.getStartingTime())
                .isBefore(LocalDateTime.of(otherGroupTraining.getDate(), otherGroupTraining.getStartingTime().plusMinutes(otherGroupTraining.getDurationInMinutes()))));
    }

    private boolean isEndingTimeDuringOtherGroupTraining(GroupTraining groupTraining, GroupTraining otherGroupTraining) {
        if (LocalDateTime.of(groupTraining.getDate(), groupTraining.getStartingTime().plusMinutes(groupTraining.getDurationInMinutes()))
                .isEqual(LocalDateTime.of(otherGroupTraining.getDate(), otherGroupTraining.getStartingTime().plusMinutes(otherGroupTraining.getDurationInMinutes()))))
            return true;
        return (LocalDateTime.of(groupTraining.getDate(), groupTraining.getStartingTime().plusMinutes(groupTraining.getDurationInMinutes()))
                .isAfter(LocalDateTime.of(otherGroupTraining.getDate(), otherGroupTraining.getStartingTime()))
                && LocalDateTime.of(groupTraining.getDate(), groupTraining.getStartingTime().plusMinutes(groupTraining.getDurationInMinutes()))
                .isBefore(LocalDateTime.of(otherGroupTraining.getDate(), otherGroupTraining.getStartingTime().plusMinutes(otherGroupTraining.getDurationInMinutes()))));
    }

    private boolean isStartingTimeBeforeAndEndingTimeAfterOtherGroupTraining(GroupTraining groupTraining, GroupTraining otherGroupTraining) {
        return (LocalDateTime.of(groupTraining.getDate(), groupTraining.getStartingTime())
                .isBefore(LocalDateTime.of(otherGroupTraining.getDate(), otherGroupTraining.getStartingTime()))
                && LocalDateTime.of(groupTraining.getDate(), groupTraining.getStartingTime().plusMinutes(groupTraining.getDurationInMinutes()))
                .isAfter(LocalDateTime.of(otherGroupTraining.getDate(), otherGroupTraining.getStartingTime().plusMinutes(otherGroupTraining.getDurationInMinutes()))));
    }
}
