package gymApp.gym.groupTrainingMember.mapper;

import gymApp.gym.models.groupTrainingMember.GroupTrainingMember;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface GroupTrainingMemberMapper {

    List<GroupTrainingMember> getAllByGroupTrainingId(@Param("groupTrainingId") UUID groupTrainingId);

    GroupTrainingMember getById(@Param("id") UUID id);

    Integer countAllByGroupTrainingId(@Param("groupTrainingId") UUID groupTrainingId);

    GroupTrainingMember getByGroupTrainingIdAndClientId(@Param("groupTrainingId") UUID groupTrainingId, @Param("clientId") UUID clientId);

    void setRemoved(@Param("id") UUID id);

    void deleteById(@Param("id") UUID id);

    void deleteByGroupTrainingIdAndClientId(@Param("clientId") UUID clientId, @Param("groupTrainingId") UUID groupTrainingId);

    void create(GroupTrainingMember groupTrainingMember);
}
