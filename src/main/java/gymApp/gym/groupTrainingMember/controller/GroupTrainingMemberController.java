package gymApp.gym.groupTrainingMember.controller;

import gymApp.gym.groupTrainingMember.service.GroupTrainingMemberService;
import gymApp.gym.models.groupTrainingMember.GroupTrainingMember;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.security.PermissionChecker;
import gymApp.gym.security.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "training/group/members")
public class GroupTrainingMemberController {

    private final GroupTrainingMemberService groupTrainingMemberService;
    private final PermissionChecker permissionChecker;
    private final TokenService tokenService;

    @Autowired
    public GroupTrainingMemberController(GroupTrainingMemberService groupTrainingMemberService, PermissionChecker permissionChecker, TokenService tokenService) {
        this.groupTrainingMemberService = groupTrainingMemberService;
        this.permissionChecker = permissionChecker;
        this.tokenService = tokenService;
    }

    @PostMapping(value = "/self")
    public GroupTrainingMember createByClient(@RequestBody GroupTrainingMember groupTrainingMember,
                                              HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.CLIENT_ONLY);
        return groupTrainingMemberService.createSelf(tokenService.getUserId(request), groupTrainingMember);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable UUID id,
                                 HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        groupTrainingMemberService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/group/training/{groupTrainingId}")
    public ResponseEntity deleteSelf(@PathVariable UUID groupTrainingId,
                                     HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.CLIENT_ONLY);
        groupTrainingMemberService.deleteByGroupTrainingIdAndUserId(groupTrainingId, tokenService.getUserId(request));
        return ResponseEntity.ok().build();
    }

    @PostMapping
    public GroupTrainingMember create(@RequestBody GroupTrainingMember groupTrainingMember,
                                      HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return groupTrainingMemberService.create(tokenService.getUserId(request), groupTrainingMember);
    }

    @GetMapping(value = "/groupTraining/{groupTrainingId}")
    public List<GroupTrainingMember> getAllByGroupTrainingId(@PathVariable UUID groupTrainingId,
                                                             HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return groupTrainingMemberService.getAllByGroupTrainingId(groupTrainingId);
    }
}
