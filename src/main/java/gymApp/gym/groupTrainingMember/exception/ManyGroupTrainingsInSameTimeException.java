package gymApp.gym.groupTrainingMember.exception;

import gymApp.gym.common.exception.ExceptionCode;
import gymApp.gym.common.exception.GymAppException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class ManyGroupTrainingsInSameTimeException extends GymAppException {
    public ManyGroupTrainingsInSameTimeException(String message) {
        super(message, ExceptionCode.MANY_GROUP_TRAININGS_IN_SAME_TIME);
    }
}
