package gymApp.gym.groupTrainingMember.exception;


import gymApp.gym.common.exception.ExceptionCode;
import gymApp.gym.common.exception.GymAppException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class GroupTrainingNotPresentException extends GymAppException {
    public GroupTrainingNotPresentException(String message) {
        super(message, ExceptionCode.GROUP_TRAINING_NOT_PRESENT);
    }
}
