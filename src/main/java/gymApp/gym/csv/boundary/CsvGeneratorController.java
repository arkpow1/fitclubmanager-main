package gymApp.gym.csv.boundary;

import gymApp.gym.csv.service.CsvGeneratorService;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.security.PermissionChecker;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;

@RestController
@RequestMapping(value = "/csv")
public class CsvGeneratorController {

    private final CsvGeneratorService csvGeneratorService;
    private final PermissionChecker permissionChecker;

    public CsvGeneratorController(CsvGeneratorService csvGeneratorService, PermissionChecker permissionChecker) {
        this.csvGeneratorService = csvGeneratorService;
        this.permissionChecker = permissionChecker;
    }

    @GetMapping("/products")
    public byte[] generateProductsCsv(@RequestParam Instant from,
                                      @RequestParam Instant to,
                                      HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return csvGeneratorService.generateProductsCsv(from, to);
    }

    @GetMapping("/productSales")
    public byte[] generateProductSalesCsv(@RequestParam Instant from,
                                          @RequestParam Instant to,
                                          HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return csvGeneratorService.generateProductSalesCsv(from, to);
    }

    @GetMapping("/productReturns")
    public byte[] generateProductReturnsCsv(@RequestParam Instant from,
                                            @RequestParam Instant to,
                                            HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return csvGeneratorService.generateProductReturnsCsv(from, to);
    }

    @GetMapping("/productSupplies")
    public byte[] generateProductSuppliesCsv(@RequestParam Instant from,
                                             @RequestParam Instant to,
                                             HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return csvGeneratorService.generateProductSuppliesCsv(from, to);
    }

    @GetMapping("/passes")
    public byte[] generatePassCsv(@RequestParam Instant from,
                                  @RequestParam Instant to,
                                  HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return csvGeneratorService.generatePassCsv(from, to);
    }
}