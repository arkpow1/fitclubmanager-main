package gymApp.gym.csv.service;

import gymApp.gym.csv.service.client.CsvGeneratorServiceClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class CsvGeneratorService {

    @Value("${csv.generator.product.service.url}")
    String productUrl;

    @Value("${csv.generator.product.sale.service.url}")
    String productSaleUrl;

    @Value("${csv.generator.product.return.service.url}")
    String productReturnUrl;

    @Value("${csv.generator.product.supply.service.url}")
    String productSupplyUrl;

    @Value("${csv.generator.pass.service.url}")
    String passUrl;

    private final CsvGeneratorServiceClient csvGeneratorServiceClient;

    public CsvGeneratorService(CsvGeneratorServiceClient csvGeneratorServiceClient) {
        this.csvGeneratorServiceClient = csvGeneratorServiceClient;
    }

    public byte[] generateProductsCsv(Instant from, Instant to) {
        return csvGeneratorServiceClient.generateCsv(productUrl, from, to);
    }

    public byte[] generateProductSalesCsv(Instant from, Instant to) {
        return csvGeneratorServiceClient.generateCsv(productSaleUrl, from, to);
    }

    public byte[] generateProductReturnsCsv(Instant from, Instant to) {
        return csvGeneratorServiceClient.generateCsv(productReturnUrl, from, to);
    }

    public byte[] generateProductSuppliesCsv(Instant from, Instant to) {
        return csvGeneratorServiceClient.generateCsv(productSupplyUrl, from, to);
    }

    public byte[] generatePassCsv(Instant from, Instant to) {
        return csvGeneratorServiceClient.generateCsv(passUrl, from, to);
    }

}