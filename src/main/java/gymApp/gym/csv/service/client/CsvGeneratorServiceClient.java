package gymApp.gym.csv.service.client;

import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;

@Service
public class CsvGeneratorServiceClient {

    private final RestTemplate restTemplate;

    public CsvGeneratorServiceClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public byte[] generateCsv(String url, Instant from, Instant to) {
        try {
            return restTemplate.getForObject(prepareUrl(url, from, to), byte[].class);
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    private URI prepareUrl(String urlAsString, Instant from, Instant to) {
        try {
            return new URI((urlAsString + "?from=" + from.toString() + "&to=" + to.toString()));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Error while preparing url");
    }

}
