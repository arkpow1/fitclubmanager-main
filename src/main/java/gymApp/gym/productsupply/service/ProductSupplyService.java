package gymApp.gym.productsupply.service;

import gymApp.gym.models.productsupply.ProductSupply;
import gymApp.gym.productsupply.service.client.ProductSupplyServiceClient;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ProductSupplyService {

    private final ProductSupplyServiceClient productSupplyServiceClient;

    public ProductSupplyService(ProductSupplyServiceClient productSupplyServiceClient) {
        this.productSupplyServiceClient = productSupplyServiceClient;
    }

    public ProductSupply getById(UUID id) {
        return productSupplyServiceClient.getById(id);
    }

    public List<ProductSupply> getAll() {
        return productSupplyServiceClient.getAll();
    }

    public List<ProductSupply> getAllByProductId(UUID productId) {
        return productSupplyServiceClient.getAllByProductId(productId);
    }

    public ProductSupply create(ProductSupply productSupply, UUID userId) {
        productSupply.setUserId(userId);
        return productSupplyServiceClient.create(productSupply);
    }

}