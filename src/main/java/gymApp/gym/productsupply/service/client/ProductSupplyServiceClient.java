package gymApp.gym.productsupply.service.client;

import gymApp.gym.models.productsupply.ProductSupply;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
public class ProductSupplyServiceClient {

    private final RestTemplate restTemplate;

    @Value("${product.supply.service.url}")
    String url;

    public ProductSupplyServiceClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ProductSupply getById(UUID id) {
        try {
            return restTemplate.getForEntity(prepareUrl(url + "/" + id.toString()), ProductSupply.class).getBody();
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    public List<ProductSupply> getAll() {
        try {
            return Arrays.asList(restTemplate.getForEntity(prepareUrl(url), ProductSupply[].class).getBody());
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    public List<ProductSupply> getAllByProductId(UUID productId) {
        try {
            return Arrays.asList(restTemplate.getForEntity(prepareUrl(url + "/product/" + productId.toString()), ProductSupply[].class).getBody());
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    public ProductSupply create(ProductSupply productSupply) {
        try {
            return restTemplate.postForEntity(prepareUrl(url), productSupply, ProductSupply.class).getBody();
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    private URI prepareUrl(String urlAsString) {
        try {
            return new URI((urlAsString));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Error while preparing url");
    }

}
