package gymApp.gym.productsupply.boundary;

import gymApp.gym.models.productsupply.ProductSupply;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.productsupply.service.ProductSupplyService;
import gymApp.gym.security.PermissionChecker;
import gymApp.gym.security.TokenService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/productSupplies")
public class ProductSupplyController {

    private final ProductSupplyService productSupplyService;
    private final PermissionChecker permissionChecker;
    private final TokenService tokenService;

    public ProductSupplyController(ProductSupplyService productSupplyService, PermissionChecker permissionChecker, TokenService tokenService) {
        this.productSupplyService = productSupplyService;
        this.permissionChecker = permissionChecker;
        this.tokenService = tokenService;
    }

    @GetMapping("/{id}")
    public ProductSupply getById(@PathVariable UUID id,
                                 HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return productSupplyService.getById(id);
    }

    @GetMapping
    public List<ProductSupply> getAll(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return productSupplyService.getAll();
    }

    @GetMapping("/product/{productId}")
    public List<ProductSupply> getAllByProductId(@PathVariable UUID productId, HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return productSupplyService.getAllByProductId(productId);
    }

    @PostMapping
    public ProductSupply create(@RequestBody ProductSupply productSupply,
                                HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return productSupplyService.create(productSupply, tokenService.getUserId(request));
    }

}