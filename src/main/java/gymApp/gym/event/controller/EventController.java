package gymApp.gym.event.controller;

import gymApp.gym.event.service.EventService;
import gymApp.gym.models.event.Event;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.security.PermissionChecker;
import gymApp.gym.security.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "events")
public class EventController {

    private final EventService eventService;
    private final PermissionChecker permissionChecker;
    private final TokenService tokenService;

    @Autowired
    public EventController(EventService eventService, PermissionChecker permissionChecker, TokenService tokenService) {
        this.eventService = eventService;
        this.permissionChecker = permissionChecker;
        this.tokenService = tokenService;
    }

    @GetMapping
    public List<Event> getAll(HttpServletRequest request) {
        return eventService.getAll();
    }

    @GetMapping(value = "/quantity/{quantity}")
    public List<Event> getAllByQuantitySortByDateDescending(@PathVariable Integer quantity,
                                                            HttpServletRequest request) {
        return eventService.getAllByQuantitySortByDateDescending(quantity);
    }

    @GetMapping(value = "/{id}")
    public Event getById(@PathVariable UUID id,
                         HttpServletRequest request) {
        return eventService.getById(id);
    }

    @DeleteMapping(value = "/setRemoved/{id}")
    public void setRemoved(@PathVariable UUID id,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        eventService.setRemoved(id);
    }

    @DeleteMapping(value = "/remove/{id}")
    public void deleteById(@PathVariable UUID id,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        eventService.deleteById(id);
    }

    @PostMapping
    public Event create(@RequestBody Event event,
                        HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return eventService.create(tokenService.getUserId(request), event);
    }

    @PatchMapping
    public Event update(@RequestBody Event event,
                        HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return eventService.update(event);
    }

    @PatchMapping(value = "/like/{id}")
    public ResponseEntity enlargeLikes(@PathVariable UUID id,
                                       HttpServletRequest request) {
        eventService.enlargeLikes(id);
        return ResponseEntity.ok().build();
    }
}
