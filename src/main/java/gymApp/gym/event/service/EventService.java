package gymApp.gym.event.service;

import gymApp.gym.event.mapper.EventMapper;
import gymApp.gym.models.event.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Service
public class EventService {

    private final EventMapper eventMapper;

    @Autowired
    public EventService(EventMapper eventMapper) {
        this.eventMapper = eventMapper;
    }

    public List<Event> getAll() {
        return eventMapper.getAll();
    }

    public List<Event> getAllByQuantitySortByDateDescending(Integer quantity) {
        return eventMapper.getAllByQuantitySortByDateDescending(quantity);
    }

    public Event getById(UUID id) {
        return eventMapper.getById(id);
    }

    public void setRemoved(UUID id) {
        eventMapper.setRemoved(id);
    }

    public void deleteById(UUID id) {
        eventMapper.deleteById(id);
    }

    public Event create(UUID userId, Event eventTemplate) {
        Event event = prepareEventToCreate(userId, eventTemplate);
        eventMapper.create(event);
        return event;
    }

    private Event prepareEventToCreate(UUID userId, Event eventTemplate) {
        return Event.builder()
                .id(UUID.randomUUID())
                .title(eventTemplate.getTitle())
                .description(eventTemplate.getDescription())
                .date(eventTemplate.getDate())
                .likes(0)
                .createdBy(userId)
                .creationDate(Instant.now())
                .removed(false)
                .imageId(eventTemplate.getImageId())
                .build();
    }

    public Event update(Event eventTemplate) {
        Event existingEvent = eventMapper.getById(eventTemplate.getId());
        Event event = prepareEventToUpdate(existingEvent, eventTemplate);
        eventMapper.update(event);
        return event;
    }

    private Event prepareEventToUpdate(Event existingEvent, Event eventTemplate) {
        return Event.builder()
                .id(existingEvent.getId())
                .title(eventTemplate.getTitle())
                .description(eventTemplate.getDescription())
                .date(eventTemplate.getDate())
                .likes(existingEvent.getLikes())
                .createdBy(existingEvent.getCreatedBy())
                .creationDate(existingEvent.getCreationDate())
                .removed(existingEvent.isRemoved())
                .imageId(eventTemplate.getImageId())
                .build();
    }

    public void enlargeLikes(UUID id) {
        eventMapper.enlargeLikes(id);
    }
}
