package gymApp.gym.event.mapper;

import gymApp.gym.models.event.Event;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface EventMapper {

    List<Event> getAll();

    List<Event> getAllByQuantitySortByDateDescending(@Param("quantity") Integer quantity);

    Event getById(@Param("id") UUID id);

    void setRemoved(@Param("id") UUID id);

    void create(Event event);

    void update(Event event);

    void enlargeLikes(@Param("id") UUID id);

    void deleteById(@Param("id") UUID id);
}
