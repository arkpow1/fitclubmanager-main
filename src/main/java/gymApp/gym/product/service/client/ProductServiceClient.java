package gymApp.gym.product.service.client;

import gymApp.gym.models.product.Product;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
public class ProductServiceClient {

    private final RestTemplate restTemplate;

    @Value("${product.service.url}")
    String url;

    public ProductServiceClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Product getById(UUID id) {
        try {
            return restTemplate.getForEntity(prepareUrl(url + "/" + id.toString()), Product.class).getBody();
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    public List<Product> getAll() {
        try {
            return Arrays.asList(restTemplate.getForEntity(prepareUrl(url), Product[].class).getBody());
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    public Product create(Product product) {
        try {
            return restTemplate.postForEntity(prepareUrl(url), product, Product.class).getBody();
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    public Product update(Product product) {
        try {
            return restTemplate.exchange(prepareUrl(url), HttpMethod.PUT, new HttpEntity(product, prepareHeaders()), Product.class).getBody();
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    public void setRemoved(UUID id) {
        try {
            restTemplate.delete(prepareUrl(url + "/" + id.toString()));
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    private URI prepareUrl(String urlAsString) {
        try {
            return new URI((urlAsString));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Error while preparing url");
    }

    private HttpHeaders prepareHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

}
