package gymApp.gym.product.service;

import gymApp.gym.models.product.Product;
import gymApp.gym.product.service.client.ProductServiceClient;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ProductService {

    private final ProductServiceClient productServiceClient;

    public ProductService(ProductServiceClient productServiceClient) {
        this.productServiceClient = productServiceClient;
    }

    public Product getById(UUID id) {
        return productServiceClient.getById(id);
    }

    public List<Product> getAll() {
        return productServiceClient.getAll();
    }

    public Product create(Product product, UUID userId) {
        product.setUserId(userId);
        return productServiceClient.create(product);
    }

    public Product update(Product product, UUID userId) {
        product.setUserId(userId);
        return productServiceClient.update(product);
    }

    public void setRemoved(UUID id) {
        productServiceClient.setRemoved(id);
    }

}