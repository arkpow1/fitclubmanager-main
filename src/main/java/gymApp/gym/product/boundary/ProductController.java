package gymApp.gym.product.boundary;

import gymApp.gym.models.product.Product;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.product.service.ProductService;
import gymApp.gym.security.PermissionChecker;
import gymApp.gym.security.TokenService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/products")
public class ProductController {

    private final ProductService productService;
    private final PermissionChecker permissionChecker;
    private final TokenService tokenService;

    public ProductController(ProductService productService, PermissionChecker permissionChecker, TokenService tokenService) {
        this.productService = productService;
        this.permissionChecker = permissionChecker;
        this.tokenService = tokenService;
    }

    @GetMapping("/{id}")
    public Product getById(@PathVariable UUID id,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return productService.getById(id);
    }

    @GetMapping
    public List<Product> getAll(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return productService.getAll();
    }

    @PostMapping
    public Product create(@RequestBody Product product,
                          HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return productService.create(product, tokenService.getUserId(request));
    }

    @PutMapping
    public Product update(@RequestBody Product product,
                          HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return productService.update(product, tokenService.getUserId(request));
    }

    @DeleteMapping("/{id}")
    public void setRemoved(@PathVariable UUID id,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        productService.setRemoved(id);
    }
}