package gymApp.gym.security;

import gymApp.gym.config.ApplicationConfig;
import gymApp.gym.loginToken.service.LoginTokenService;
import gymApp.gym.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;

import javax.ws.rs.HttpMethod;
import java.util.Arrays;

@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final ApplicationConfig appConfig;
    private final UserService userService;
    private final LoginTokenService loginTokenService;

    @Autowired
    public WebSecurity(UserDetailsService userDetailsService,
                       BCryptPasswordEncoder bCryptPasswordEncoder,
                       ApplicationConfig appConfig,
                       UserService userService,
                       LoginTokenService loginTokenService) {
        this.userDetailsService = userDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.appConfig = appConfig;
        this.userService = userService;
        this.loginTokenService = loginTokenService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                .antMatchers("/**/*.html").permitAll()
                .antMatchers("/**/*.js").permitAll()
                .antMatchers("/**/*.png").permitAll()
                .antMatchers("/v2/api-docs").access("hasIpAddress('10.0.0.0/24') or hasIpAddress('127.0.0.1/32')")
                .antMatchers("/configuration/ui").access("hasIpAddress('10.0.0.0/24') or hasIpAddress('127.0.0.1/32')")
                .antMatchers("/swagger-resources/**").access("hasIpAddress('10.0.0.0/24') or hasIpAddress('127.0.0.1/32')")
                .antMatchers("/configuration/security").access("hasIpAddress('10.0.0.0/24') or hasIpAddress('127.0.0.1/32')")
                .antMatchers("/swagger-ui.html").access("hasIpAddress('10.0.0.0/24') or hasIpAddress('127.0.0.1/32')")
                .antMatchers("/webjars/**").access("hasIpAddress('10.0.0.0/24') or hasIpAddress('127.0.0.1/32')")
                .antMatchers(HttpMethod.POST, "/users/resetPassword").permitAll()
                .antMatchers(HttpMethod.POST, "/users/*/changePassword").permitAll()
                .antMatchers("/training/groups/offer/all").permitAll()
                .antMatchers("/training/groups/all").permitAll()
                .antMatchers("/training/individual/offer/all").permitAll()
                .antMatchers("/trainers/all").permitAll()
                .antMatchers("/trainers/withShowProfile").permitAll()
                .antMatchers("/payment/dotpay/notification").permitAll()
                .antMatchers(HttpMethod.GET, "/events").permitAll()
                .antMatchers(HttpMethod.GET, "/events/**").permitAll()
                .antMatchers("/events/like/*").permitAll()
                .antMatchers(HttpMethod.GET, "/images/*").permitAll()
                .antMatchers(HttpMethod.GET, "/regulations/show/*").permitAll()
                .antMatchers(HttpMethod.POST, "/login/tokens").permitAll()
                .antMatchers("/tariff/all").permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilter(new AuthenticationFilter(authenticationManager(), appConfig, userService, loginTokenService))
                .addFilter(new AuthorizationFilter(authenticationManager(), appConfig))
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        CorsConfiguration config = new CorsConfiguration();
        config.applyPermitDefaultValues();
        config.setAllowedMethods(Arrays.asList("PUT", "DELETE", "POST", "PATCH", "GET"));
        http.cors().configurationSource(request -> config);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }
}
