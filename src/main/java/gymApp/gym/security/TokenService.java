package gymApp.gym.security;

import gymApp.gym.config.ApplicationConfig;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.user.mapper.UserMapper;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@Service
public class TokenService {

    private final ApplicationConfig appConfig;
    private final UserMapper userMapper;

    @Autowired
    public TokenService(ApplicationConfig appConfig, UserMapper userMapper) {
        this.appConfig = appConfig;
        this.userMapper = userMapper;
    }

    public String getUserEmail(HttpServletRequest request) {
        String token = request.getHeader(appConfig.getTokenHeader());
        if (token != null) {
            return Jwts.parser()
                    .setSigningKey(appConfig.getSecret().getBytes())
                    .parseClaimsJws(token)
                    .getBody()
                    .getSubject();
        }
        return null;
    }

    public UUID getUserId(HttpServletRequest request) {
        return userMapper.getByEmail(getUserEmail(request)).getId();
    }

    public UserRole getUserRole(HttpServletRequest request) {
        return userMapper.getByEmail(getUserEmail(request)).getRole();
    }
}
