package gymApp.gym.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class UrlFinder {
    private static final Logger LOG = LoggerFactory.getLogger(UrlFinder.class);

    private String serverPort;
    private String serverContextPath;

    @Autowired
    public UrlFinder(@Value("${server.port}") String serverPort,
                     @Value("${server.servlet.context-path}") String serverContextPath) {
        this.serverPort = serverPort;
        this.serverContextPath = serverContextPath;
    }

    public String findApplicationUrl() {
        String url = "http://localhost:" + serverPort + serverContextPath;
        LOG.info("Application URL '{}' found", url);
        return url;
    }
}
