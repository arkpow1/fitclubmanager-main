package gymApp.gym.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import gymApp.gym.config.ApplicationConfig;
import gymApp.gym.loginToken.service.LoginTokenService;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.user.service.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;
    private final ApplicationConfig appConfig;
    private final UserService userService;
    private final LoginTokenService loginTokenService;

    public AuthenticationFilter(AuthenticationManager authenticationManager, ApplicationConfig appConfig, UserService userService, LoginTokenService loginTokenService) {
        this.authenticationManager = authenticationManager;
        this.appConfig = appConfig;
        this.userService = userService;
        this.loginTokenService = loginTokenService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {
        try {
            gymApp.gym.models.user.UserLoginTO creds = new ObjectMapper()
                    .readValue(req.getInputStream(), gymApp.gym.models.user.UserLoginTO.class);

            UserRole role = userService.getByEmail(creds.getEmail()).getRole();
            if (role != creds.getRole())
                throw new IllegalStateException("Invalid role");
            if (role == UserRole.ADMIN && !loginTokenService.validateToken(creds.getEmail(), creds.getToken()))
                    throw new IllegalStateException("Invalid token");

            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            creds.getEmail(),
                            creds.getPassword(),
                            new ArrayList<>())
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException {

        String token = Jwts.builder()
                .setSubject(((User) auth.getPrincipal()).getUsername())
                .setExpiration(new Date(System.currentTimeMillis() + appConfig.getTokenExpirationTime()))
                .signWith(SignatureAlgorithm.HS512, appConfig.getSecret().getBytes())
                .compact();

        res.addHeader(appConfig.getTokenHeader(), token);
        res.getWriter().print(token);
    }
}
