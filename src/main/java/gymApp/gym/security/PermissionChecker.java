package gymApp.gym.security;

import gymApp.gym.common.exception.NoPermissionException;
import gymApp.gym.models.user.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

@Component
public class PermissionChecker {

    private final TokenService tokenService;

    @Autowired
    public PermissionChecker(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    public void checkPermission(HttpServletRequest request, Set<UserRole> allowedRoles) {
        UserRole userRole = tokenService.getUserRole(request);
        if (!allowedRoles.contains(userRole)) {
            throw new NoPermissionException(String.format("User with role %s has no permission to perform this action.", userRole));
        }
    }
}
