package gymApp.gym.groupTrainingOffer.mapper;

import gymApp.gym.models.groupTrainingOffer.GroupTrainingOffer;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface GroupTrainingOfferMapper {

    List<GroupTrainingOffer> getAll();

    GroupTrainingOffer getById(@Param("id") UUID id);

    void setRemoved(@Param("id") UUID id);

    void deleteById(@Param("id") UUID id);

    void create(GroupTrainingOffer groupTrainingOffer);

    void update(GroupTrainingOffer groupTrainingOffer);
}
