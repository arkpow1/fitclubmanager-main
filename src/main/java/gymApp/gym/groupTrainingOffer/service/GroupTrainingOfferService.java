package gymApp.gym.groupTrainingOffer.service;

import gymApp.gym.groupTrainingOffer.mapper.GroupTrainingOfferMapper;
import gymApp.gym.models.groupTrainingOffer.GroupTrainingOffer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Service
public class GroupTrainingOfferService {

    private final GroupTrainingOfferMapper groupTrainingOfferMapper;

    @Autowired
    public GroupTrainingOfferService(GroupTrainingOfferMapper groupTrainingOfferMapper) {
        this.groupTrainingOfferMapper = groupTrainingOfferMapper;
    }

    public List<GroupTrainingOffer> getAll() {
        return groupTrainingOfferMapper.getAll();
    }

    public void setRemoved(UUID id) {
        groupTrainingOfferMapper.setRemoved(id);
    }

    public void deleteById(UUID id) {
        groupTrainingOfferMapper.deleteById(id);
    }

    public GroupTrainingOffer create(UUID userId, GroupTrainingOffer groupTrainingOfferTemplate) {
        GroupTrainingOffer groupTrainingOffer = prepareGroupTrainingOfferToCreate(userId, groupTrainingOfferTemplate);
        groupTrainingOfferMapper.create(groupTrainingOffer);
        return groupTrainingOffer;
    }

    private GroupTrainingOffer prepareGroupTrainingOfferToCreate(UUID userId, GroupTrainingOffer groupTrainingOfferTemplate) {
        return GroupTrainingOffer.builder()
                .id(UUID.randomUUID())
                .name(groupTrainingOfferTemplate.getName())
                .description(groupTrainingOfferTemplate.getDescription())
                .intensity(groupTrainingOfferTemplate.getIntensity())
                .createdBy(userId)
                .creationDate(Instant.now())
                .removed(false)
                .imageId(groupTrainingOfferTemplate.getImageId())
                .build();
    }

    public GroupTrainingOffer update(UUID userId, GroupTrainingOffer groupTrainingOfferTemplate) {
        GroupTrainingOffer existingGroupTrainingOffer = groupTrainingOfferMapper.getById(groupTrainingOfferTemplate.getId());
        GroupTrainingOffer groupTrainingOffer = prepareGroupTrainingOfferToUpdate(existingGroupTrainingOffer, groupTrainingOfferTemplate);
        groupTrainingOfferMapper.update(groupTrainingOffer);
        return groupTrainingOffer;
    }

    private GroupTrainingOffer prepareGroupTrainingOfferToUpdate(GroupTrainingOffer existingGroupTrainingOffer, GroupTrainingOffer groupTrainingOfferTemplate) {
        return GroupTrainingOffer.builder()
                .id(existingGroupTrainingOffer.getId())
                .name(groupTrainingOfferTemplate.getName())
                .description(groupTrainingOfferTemplate.getDescription())
                .intensity(groupTrainingOfferTemplate.getIntensity())
                .createdBy(existingGroupTrainingOffer.getCreatedBy())
                .creationDate(existingGroupTrainingOffer.getCreationDate())
                .removed(existingGroupTrainingOffer.isRemoved())
                .imageId(groupTrainingOfferTemplate.getImageId())
                .build();
    }

    public GroupTrainingOffer getById(UUID id) {
        return groupTrainingOfferMapper.getById(id);
    }
}
