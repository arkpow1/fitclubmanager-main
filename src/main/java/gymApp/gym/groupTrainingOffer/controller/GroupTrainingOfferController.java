package gymApp.gym.groupTrainingOffer.controller;

import gymApp.gym.groupTrainingOffer.service.GroupTrainingOfferService;
import gymApp.gym.models.groupTrainingOffer.GroupTrainingOffer;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.security.PermissionChecker;
import gymApp.gym.security.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "training/groups/offer")
public class GroupTrainingOfferController {

    private final GroupTrainingOfferService groupTrainingOfferService;
    private final PermissionChecker permissionChecker;
    private final TokenService tokenService;

    @Autowired
    public GroupTrainingOfferController(GroupTrainingOfferService groupTrainingOfferService, PermissionChecker permissionChecker, TokenService tokenService) {
        this.groupTrainingOfferService = groupTrainingOfferService;
        this.permissionChecker = permissionChecker;
        this.tokenService = tokenService;
    }

    @GetMapping(value = "/all")
    public List<GroupTrainingOffer> getAll(HttpServletRequest request) {
        return groupTrainingOfferService.getAll();
    }

    @DeleteMapping(value = "/setRemoved/{id}")
    public void setRemoved(@PathVariable UUID id,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        groupTrainingOfferService.setRemoved(id);
    }

    @DeleteMapping(value = "/remove/{id}")
    public void deleteById(@PathVariable UUID id,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        groupTrainingOfferService.deleteById(id);
    }

    @PostMapping
    public GroupTrainingOffer create(@RequestBody GroupTrainingOffer groupTrainingOffer,
                                     HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return groupTrainingOfferService.create(tokenService.getUserId(request), groupTrainingOffer);
    }

    @PatchMapping
    public GroupTrainingOffer update(@RequestBody GroupTrainingOffer groupTrainingOffer,
                                     HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return groupTrainingOfferService.update(tokenService.getUserId(request), groupTrainingOffer);
    }

    @GetMapping(value = "/{id}")
    public GroupTrainingOffer getById(@PathVariable UUID id,
                                      HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ALL);
        return groupTrainingOfferService.getById(id);
    }
}
