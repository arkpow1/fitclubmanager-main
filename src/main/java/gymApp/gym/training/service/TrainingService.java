package gymApp.gym.training.service;

import gymApp.gym.models.training.Training;
import gymApp.gym.training.mapper.TrainingMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class TrainingService {

    private final TrainingMapper trainingMapper;

    @Autowired
    public TrainingService(TrainingMapper trainingMapper) {
        this.trainingMapper = trainingMapper;
    }

    public List<Training> getAll() {
        return trainingMapper.getAll();
    }

    public void setRemoved(UUID id) {
        trainingMapper.setRemoved(id);
    }

    public void deleteById(UUID id) {
        trainingMapper.deleteById(id);
    }

    public void create(Training training) {
        trainingMapper.create(training);
    }

    public void update(Training training) {
        trainingMapper.update(training);
    }

    public Training getById(UUID id) {
        return trainingMapper.getById(id);
    }
}
