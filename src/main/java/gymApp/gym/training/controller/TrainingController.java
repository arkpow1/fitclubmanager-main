package gymApp.gym.training.controller;


import gymApp.gym.models.training.Training;
import gymApp.gym.training.service.TrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "trainings")

public class TrainingController {

    private final TrainingService trainingService;

    @Autowired
    public TrainingController(TrainingService trainingService) {
        this.trainingService = trainingService;
    }

    @GetMapping
    public List<Training> getAll() {
        return trainingService.getAll();
    }

    @DeleteMapping(value = "/setRemoved/{id}")
    public void setRemoved(@PathVariable UUID id) {
        trainingService.setRemoved(id);
    }

    @DeleteMapping(value = "/deleteById/{id}")
    public void deleteById(@PathVariable UUID id) {
        trainingService.deleteById(id);
    }

    @PostMapping
    public void create(Training training) {
        trainingService.create(training);
    }

    @PatchMapping
    public void update(Training training) {
        trainingService.update(training);
    }

    @GetMapping(value = "/id/{id}")
    public Training getById(@PathVariable UUID id) {
        return trainingService.getById(id);
    }
}
