package gymApp.gym.payment.dotpaypayment.service.client;

import gymApp.gym.models.dotpayPayment.DotpayPaymentInfoResponse;
import gymApp.gym.models.dotpayPayment.DotpayPaymentLinkGeneration;
import gymApp.gym.models.dotpayPayment.DotpayPaymentLinkGenerationResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Service
public class DotpayPaymentServiceClient {

    private final RestTemplate restTemplate;

    @Value("${payment.dotpay.service.url}")
    String url;

    public DotpayPaymentServiceClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ResponseEntity<DotpayPaymentLinkGenerationResponse> sendGeneratePaymentLink(DotpayPaymentLinkGeneration dotpayPaymentLinkGeneration) {
        try {
            return restTemplate.exchange(prepareUrl(), HttpMethod.POST, new HttpEntity(dotpayPaymentLinkGeneration, prepareHeaders()), DotpayPaymentLinkGenerationResponse.class);
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    public ResponseEntity<DotpayPaymentInfoResponse> sendGetPaymentInfo() {
        try {
            return restTemplate.exchange(prepareUrl(), HttpMethod.GET, new HttpEntity(HttpEntity.EMPTY, prepareHeaders()), DotpayPaymentInfoResponse.class);
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    private URI prepareUrl() {
        try {
            return new URI((url));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Error while preparing url");
    }

    private HttpHeaders prepareHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

}