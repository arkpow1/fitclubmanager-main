package gymApp.gym.payment.dotpaypayment.service;

import gymApp.gym.models.client.Client;
import gymApp.gym.models.dotpayPayment.*;
import gymApp.gym.models.pass.Pass;
import gymApp.gym.payment.dotpaypayment.service.client.DotpayPaymentServiceClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DotpayPaymentService {

    private final DotpayPaymentServiceClient dotpayPaymentServiceClient;

    @Value("${payment.back.url}")
    private String backUrl;
    @Value("${payment.dotpay.notification.url}")
    private String notificationUrl;

    public DotpayPaymentService(DotpayPaymentServiceClient dotpayPaymentServiceClient) {
        this.dotpayPaymentServiceClient = dotpayPaymentServiceClient;
    }

    public DotpayPaymentLinkGenerationResponse createDotpayPayment(String description, Pass pass, Client client) {
        return dotpayPaymentServiceClient.sendGeneratePaymentLink(prepareDotpayPaymentLinkGeneration(description, pass, client)).getBody();
    }

    public DotpayPaymentInfoResponse getPaymentsInfo() {
        return dotpayPaymentServiceClient.sendGetPaymentInfo().getBody();
    }

    private DotpayPaymentAddress prepareDotpayPaymentAddress() {
        return DotpayPaymentAddress.builder()
                .street("")
                .buildingNumber(-1)
                .postcode("")
                .city("")
                .region("")
                .country("POL")
                .build();
    }

    private DotpayPaymentPayer prepareDotpayPaymentPayer(Client client) {
        return DotpayPaymentPayer.builder()
                .firstName(client.getName())
                .lastName(client.getSurname())
                .email(client.getEmail())
                .phone(client.getPhoneNumber())
                .address(prepareDotpayPaymentAddress())
                .build();
    }

    private DotpayPaymentLinkGeneration prepareDotpayPaymentLinkGeneration(String description, Pass pass, Client client) {
        String urlc = null;
        if (!notificationUrl.equals("empty")) {
            urlc = notificationUrl;
        }
        return DotpayPaymentLinkGeneration.builder()
                .amount(pass.getTotalPrice().toString())
                .currency(pass.getCurrency().toString())
                .description(description)
                .control(pass.getId().toString())
                .language("pl")
                .ignoreLastPaymentChannel(1)
                .redirectionType(0)
                .url(backUrl)
                .urlc(urlc)
                .payer(prepareDotpayPaymentPayer(client))
                .build();
    }
}