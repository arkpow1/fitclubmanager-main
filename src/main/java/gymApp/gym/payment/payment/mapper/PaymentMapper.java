package gymApp.gym.payment.payment.mapper;

import gymApp.gym.models.payment.Payment;
import gymApp.gym.models.payment.PaymentStatus;
import gymApp.gym.models.payment.PaymentType;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface PaymentMapper {

    List<Payment> getAll();

    List<Payment> getAllByType(@Param("type") PaymentType type);

    void remove(@Param("id") UUID id);

    void setRemoved(@Param("id") UUID id);

    void create(Payment payment);

    void updateStatus(Payment payment);

    Payment getById(@Param("id") UUID id);

    Payment getByPassId(@Param("passId") UUID passId);

    List<Payment> getByClientId(@Param("clientId") UUID clientId);
}
