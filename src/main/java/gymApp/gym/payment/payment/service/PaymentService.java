package gymApp.gym.payment.payment.service;

import gymApp.gym.client.service.ClientService;
import gymApp.gym.models.client.Client;
import gymApp.gym.models.pass.Pass;
import gymApp.gym.models.payment.Payment;
import gymApp.gym.models.payment.PaymentStatus;
import gymApp.gym.models.payment.PaymentType;
import gymApp.gym.pass.service.PassService;
import gymApp.gym.payment.dotpaypayment.service.DotpayPaymentService;
import gymApp.gym.payment.payment.exception.PaymentForPassAlreadyExistsException;
import gymApp.gym.payment.payment.mapper.PaymentMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Service
public class PaymentService {

    private final DotpayPaymentService dotpayPaymentService;
    private final ClientService clientService;
    private final PassService passService;
    private final PaymentMapper paymentMapper;

    public PaymentService(DotpayPaymentService dotpayPaymentService, ClientService clientService, PassService passService, PaymentMapper paymentMapper) {
        this.dotpayPaymentService = dotpayPaymentService;
        this.clientService = clientService;
        this.passService = passService;
        this.paymentMapper = paymentMapper;
    }

    @Transactional
    public Payment createDotpayPayment(UUID passId, UUID userId) {
        if (paymentMapper.getByPassId(passId) != null) {
            throw new PaymentForPassAlreadyExistsException("Payment for pass " + passId.toString() + " already exists");
        }
        Pass pass = passService.getById(passId);
        Client client = clientService.getByUserId(userId);
        if (!pass.getClientId().equals(client.getId())) {
            throw new IllegalArgumentException("Pass with id " + passId + " not belongs to client " + client.getId());
        }
        String description = "Payment for pass " + pass.getId().toString();
        Payment payment = preparePaymentToCreate(PaymentType.DOTPAY,
                dotpayPaymentService.createDotpayPayment(description, pass, client).getPayment_url(),
                description,
                pass);
        paymentMapper.create(payment);
        return payment;
    }

    public List<Payment> getSelfClientPayments(UUID userId) {
        return paymentMapper.getByClientId(clientService.getByUserId(userId).getId());
    }

    public List<Payment> getClientPayments(UUID clientId) {
        return paymentMapper.getByClientId(clientId);
    }

    public Payment getSelfByPassId(UUID passId, UUID userId) {
        Client client = clientService.getById(userId);
        Payment payment = paymentMapper.getByPassId(passId);
        if (!payment.getClientId().equals(client.getId())) {
            throw new IllegalStateException("Client " + client.getId().toString() + " is not owner of pass " + passId.toString());
        }
        return payment;
    }

    public Payment getByPassId(UUID passId) {
        return paymentMapper.getByPassId(passId);
    }

    private Payment preparePaymentToCreate(PaymentType type, String url, String description, Pass pass) {
        return Payment.builder()
                .id(UUID.randomUUID())
                .type(type)
                .url(url)
                .price(pass.getTotalPrice())
                .currency(pass.getCurrency())
                .description(description)
                .status(PaymentStatus.NONE)
                .creationDate(Instant.now())
                .removed(false)
                .passId(pass.getId())
                .clientId(pass.getClientId())
                .build();
    }
}