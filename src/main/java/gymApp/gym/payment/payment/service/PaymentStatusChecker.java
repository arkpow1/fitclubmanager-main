package gymApp.gym.payment.payment.service;

import gymApp.gym.models.dotpayPayment.DotpayPaymentInfoResponse;
import gymApp.gym.models.dotpayPayment.DotpayPaymentSingleInfo;
import gymApp.gym.models.payment.Payment;
import gymApp.gym.models.payment.PaymentStatus;
import gymApp.gym.models.payment.PaymentType;
import gymApp.gym.pass.service.PassService;
import gymApp.gym.payment.dotpaypayment.service.DotpayPaymentService;
import gymApp.gym.payment.payment.mapper.PaymentMapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PaymentStatusChecker {

    private final String DOTPAY_PAYMENT_COMPLETED_STATUS = "completed";

    private final DotpayPaymentService dotpayPaymentService;
    private final PassService passService;
    private final PaymentMapper paymentMapper;

    public PaymentStatusChecker(DotpayPaymentService dotpayPaymentService, PassService passService, PaymentMapper paymentMapper) {
        this.dotpayPaymentService = dotpayPaymentService;
        this.passService = passService;
        this.paymentMapper = paymentMapper;
    }

    @Scheduled(cron = "0 */2 * * * *")
    public void checkNotCompletedPaymentsStatus() {
        List<Payment> dotpayPayments = paymentMapper.getAllByType(PaymentType.DOTPAY);
        List<Payment> notCompletedDotpayPayments = dotpayPayments.stream().filter(dP -> dP.getStatus() != PaymentStatus.COMPLETED).collect(Collectors.toList());
        if (notCompletedDotpayPayments.size() > 0) {
            DotpayPaymentInfoResponse paymentsInfo = dotpayPaymentService.getPaymentsInfo();
            notCompletedDotpayPayments.forEach(dP -> {
                paymentsInfo.getResults().forEach(result -> {
                    if (checkIfResultIsForPayment(result, dP)) {
                        if (result.getStatus().equals(DOTPAY_PAYMENT_COMPLETED_STATUS)) {
                            dP.setStatus(PaymentStatus.COMPLETED);
                            paymentMapper.updateStatus(dP);
                            passService.updatePaid(dP.getPassId(), true);
                        } else {
                            dP.setStatus(PaymentStatus.NOT_COMPLETED);
                            paymentMapper.updateStatus(dP);
                        }
                    }
                });
            });
        }
    }

    private Boolean checkIfResultIsForPayment(DotpayPaymentSingleInfo info, Payment payment) {
        return (info.getControl().equals(payment.getPassId().toString()) &&
                info.getAmount().equals(payment.getPrice().toPlainString()) &&
                info.getCurrency().equals(payment.getCurrency().toString()) &&
                info.getDescription().equals(payment.getDescription()));
    }
}