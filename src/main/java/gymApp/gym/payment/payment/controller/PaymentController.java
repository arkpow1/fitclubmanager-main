package gymApp.gym.payment.payment.controller;

import gymApp.gym.models.payment.Payment;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.payment.payment.service.PaymentService;
import gymApp.gym.security.PermissionChecker;
import gymApp.gym.security.TokenService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/payment")
public class PaymentController {

    private final PaymentService paymentService;
    private final PermissionChecker permissionChecker;
    private final TokenService tokenService;

    public PaymentController(PaymentService paymentService, PermissionChecker permissionChecker, TokenService tokenService) {
        this.paymentService = paymentService;
        this.permissionChecker = permissionChecker;
        this.tokenService = tokenService;
    }

    @PostMapping(value = "/dotpay")
    public Payment createDotpayPayment(@RequestParam UUID passId,
                                       HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.CLIENT_ONLY);
        return paymentService.createDotpayPayment(passId, tokenService.getUserId(request));
    }

    @GetMapping(value = "/client/{clientId}")
    public List<Payment> getClientPayments(@PathVariable UUID clientId,
                                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return paymentService.getClientPayments(clientId);
    }

    @GetMapping(value = "/client")
    public List<Payment> getSelfClientPayments(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.CLIENT_ONLY);
        return paymentService.getSelfClientPayments(tokenService.getUserId(request));
    }

    @GetMapping(value = "/pass/{passId}")
    public Payment getByPassId(@PathVariable UUID passId,
                               HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return paymentService.getByPassId(passId);
    }

    @GetMapping(value = "pass/{passId}/self")
    public Payment getSelfByPassId(@PathVariable UUID passId,
                                   HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.CLIENT_ONLY);
        return paymentService.getSelfByPassId(passId, tokenService.getUserId(request));
    }

}