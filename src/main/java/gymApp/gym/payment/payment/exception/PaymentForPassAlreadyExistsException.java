package gymApp.gym.payment.payment.exception;

import gymApp.gym.common.exception.ExceptionCode;
import gymApp.gym.common.exception.GymAppException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class PaymentForPassAlreadyExistsException extends GymAppException {
    public PaymentForPassAlreadyExistsException(String message) {
        super(message, ExceptionCode.PAYMENT_FOR_PASS_ALREADY_EXISTS);
    }
}
