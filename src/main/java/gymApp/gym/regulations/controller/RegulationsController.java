package gymApp.gym.regulations.controller;

import gymApp.gym.models.regulations.RegulationsType;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.regulations.service.RegulationsService;
import gymApp.gym.security.PermissionChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "regulations")
public class RegulationsController {

    private final RegulationsService regulationsService;
    private final PermissionChecker permissionChecker;

    @Autowired
    public RegulationsController(RegulationsService regulationsService, PermissionChecker permissionChecker) {
        this.regulationsService = regulationsService;
        this.permissionChecker = permissionChecker;
    }

    @PostMapping(value = "/{type}")
    public ResponseEntity create(@PathVariable RegulationsType type,
                                 @RequestParam MultipartFile regulations,
                                 HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ADMIN_ONLY);
        regulationsService.create(type, regulations);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/show/{type}", produces = MediaType.APPLICATION_PDF_VALUE)
    public byte[] getByType(@PathVariable RegulationsType type) {
        return regulationsService.getByType(type);
    }
}
