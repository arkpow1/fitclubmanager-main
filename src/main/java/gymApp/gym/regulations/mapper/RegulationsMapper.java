package gymApp.gym.regulations.mapper;

import gymApp.gym.models.regulations.Regulations;
import gymApp.gym.models.regulations.RegulationsType;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RegulationsMapper {

    void remove(@Param("id") UUID id);

    void create(Regulations regulations);

    Regulations getByType(@Param("type") RegulationsType type);
}
