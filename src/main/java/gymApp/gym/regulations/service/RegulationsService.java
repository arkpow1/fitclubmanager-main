package gymApp.gym.regulations.service;

import gymApp.gym.models.regulations.Regulations;
import gymApp.gym.models.regulations.RegulationsType;
import gymApp.gym.regulations.mapper.RegulationsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.Instant;
import java.util.UUID;

@Service
public class RegulationsService {

    private final RegulationsMapper regulationsMapper;

    @Autowired
    public RegulationsService(RegulationsMapper regulationsMapper) {
        this.regulationsMapper = regulationsMapper;
    }

    @Transactional
    public void create(RegulationsType type, MultipartFile imageFile) {
        Regulations regulations = prepareRegulationsToCreate(type, imageFile);
        Regulations existingRegulationsWithSameType = regulationsMapper.getByType(type);
        if (existingRegulationsWithSameType != null) {
            regulationsMapper.remove(existingRegulationsWithSameType.getId());
        }
        regulationsMapper.create(regulations);
    }

    public byte[] getByType(RegulationsType type) {
        Regulations regulations = regulationsMapper.getByType(type);
        return regulations.getBytes();
    }

    private Regulations prepareRegulationsToCreate(RegulationsType type, MultipartFile imageTemplate) {
        try {
            return Regulations.builder()
                    .id(UUID.randomUUID())
                    .type(type)
                    .bytes(imageTemplate.getBytes())
                    .creationDate(Instant.now())
                    .removed(false)
                    .build();
        } catch (IOException e) {
            throw new IllegalStateException("Error while creating image");
        }
    }
}
