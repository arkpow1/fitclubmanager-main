package gymApp.gym.receptionist.controller;


import gymApp.gym.models.receptionist.Receptionist;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.receptionist.service.ReceptionistService;
import gymApp.gym.security.PermissionChecker;
import gymApp.gym.security.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "receptionists")
public class ReceptionistController {

    private final ReceptionistService receptionistService;
    private final PermissionChecker permissionChecker;
    private final TokenService tokenService;

    @Autowired
    public ReceptionistController(ReceptionistService receptionistService, PermissionChecker permissionChecker, TokenService tokenService) {
        this.receptionistService = receptionistService;
        this.permissionChecker = permissionChecker;
        this.tokenService = tokenService;
    }

    @GetMapping
    public List<Receptionist> getAll(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return receptionistService.getAll();
    }

    @DeleteMapping(value = "/setRemoved/{id}")
    public ResponseEntity setRemoved(@PathVariable("id") UUID id,
                                     HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        receptionistService.setRemoved(id);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/deleteById/{id}")
    public ResponseEntity deleteById(@PathVariable("id") UUID id,
                                     HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ADMIN_ONLY);
        receptionistService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping
    public Receptionist create(@RequestBody Receptionist receptionist,
                               HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ADMIN_ONLY);
        return receptionistService.create(tokenService.getUserId(request), receptionist);
    }

    @PatchMapping
    public Receptionist update(@RequestBody Receptionist receptionist,
                               HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return receptionistService.update(receptionist);
    }

    @GetMapping(value = "/id/{id}")
    public Receptionist getById(@PathVariable UUID id,
                                HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return receptionistService.getById(id);
    }

    @GetMapping(value = "/self")
    public Receptionist getSelf(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.RECEPTIONIST_ONLY);
        return receptionistService.getByUserId(tokenService.getUserId(request));
    }

    @GetMapping(value = "/withNotifications")
    public List<Receptionist> getWithNotifications(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return receptionistService.getWithNotifications();
    }
}
