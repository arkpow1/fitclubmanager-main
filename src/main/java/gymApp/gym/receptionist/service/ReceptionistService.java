package gymApp.gym.receptionist.service;

import gymApp.gym.models.receptionist.Receptionist;
import gymApp.gym.models.user.User;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.receptionist.mapper.ReceptionistMapper;
import gymApp.gym.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Service
public class ReceptionistService {

    private final ReceptionistMapper receptionistMapper;
    private final UserService userService;

    @Autowired
    public ReceptionistService(ReceptionistMapper receptionistMapper, UserService userService) {
        this.receptionistMapper = receptionistMapper;
        this.userService = userService;
    }

    public List<Receptionist> getAll() {
        return receptionistMapper.getAll();
    }

    public void setRemoved(UUID id) {
        Receptionist receptionist = getById(id);
        receptionistMapper.setRemoved(id);
        userService.setRemoved(receptionist.getUserId());
    }

    public void deleteById(UUID id) {
        Receptionist receptionist = getById(id);
        receptionistMapper.deleteById(id);
        userService.remove(receptionist.getUserId());
    }

    @Transactional
    public Receptionist create(UUID createdBy, Receptionist receptionistTemplate) {
        User user = userService.create(createdBy, User.builder().email(receptionistTemplate.getEmail()).role(UserRole.RECEPTIONIST).build());
        receptionistTemplate.setUserId(userService.getByEmail(user.getEmail()).getId());
        Receptionist receptionist = prepareReceptionistToSave(receptionistTemplate);
        validateReceptionist(receptionist);
        receptionistMapper.create(receptionist);
        return receptionist;
    }

    private Receptionist prepareReceptionistToSave(Receptionist receptionistTemplate) {
        return Receptionist
                .builder()
                .id(UUID.randomUUID())
                .email(receptionistTemplate.getEmail())
                .name(receptionistTemplate.getName())
                .surname(receptionistTemplate.getSurname())
                .birthdate(receptionistTemplate.getBirthdate())
                .pesel(null)
                .phoneNumber(receptionistTemplate.getPhoneNumber())
                .notifications(true)
                .gender(receptionistTemplate.getGender())
                .creationDate(Instant.now())
                .removed(false)
                .userId(receptionistTemplate.getUserId())
                .build();
    }

    // TODO prepare to update
    @Transactional
    public Receptionist update(Receptionist receptionist) {
        Receptionist existingReceptionist = getById(receptionist.getId());
        validateReceptionist(receptionist);
        receptionistMapper.update(receptionist);
        userService.updateEmail(existingReceptionist.getUserId(), receptionist.getEmail());
        return getById(receptionist.getId());
    }

    private void validateReceptionist(Receptionist receptionist) {
        if (!receptionist.getPhoneNumber().matches("[0-9]+"))
            throw new IllegalStateException("Wrong credentials");
    }

    public Receptionist getById(UUID id) {
        return receptionistMapper.getById(id);
    }

    public Receptionist getByUserId(UUID userId) {
        return receptionistMapper.getByUserId(userId);
    }

    public List<Receptionist> getWithNotifications() {
        return receptionistMapper.getWithNotifications();
    }
}
