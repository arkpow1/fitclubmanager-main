package gymApp.gym.receptionist.mapper;

import gymApp.gym.models.receptionist.Receptionist;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ReceptionistMapper {

    List<Receptionist> getAll();

    Receptionist getById(@Param("id") UUID id);

    Receptionist getByUserId(@Param("userId") UUID userId);

    List<Receptionist> getWithNotifications();

    void setRemoved(@Param("id") UUID id);

    void deleteById(@Param("id") UUID id);

    void create(Receptionist receptionist);

    void update(Receptionist receptionist);
}
