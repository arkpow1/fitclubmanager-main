package gymApp.gym.productsale.boundary;

import gymApp.gym.models.productsale.ProductSale;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.productsale.service.ProductSaleService;
import gymApp.gym.security.PermissionChecker;
import gymApp.gym.security.TokenService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/productSales")
public class ProductSaleController {

    private final ProductSaleService productSaleService;
    private final PermissionChecker permissionChecker;
    private final TokenService tokenService;

    public ProductSaleController(ProductSaleService productSaleService, PermissionChecker permissionChecker, TokenService tokenService) {
        this.productSaleService = productSaleService;
        this.permissionChecker = permissionChecker;
        this.tokenService = tokenService;
    }

    @GetMapping("/{id}")
    public ProductSale getById(@PathVariable UUID id,
                               HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return productSaleService.getById(id);
    }

    @GetMapping
    public List<ProductSale> getAll(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return productSaleService.getAll();
    }

    @GetMapping("/product/{productId}")
    public List<ProductSale> getAllByProductId(@PathVariable UUID productId, HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return productSaleService.getAllByProductId(productId);
    }

    @PostMapping
    public ProductSale create(@RequestBody ProductSale productSale,
                              HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return productSaleService.create(productSale, tokenService.getUserId(request));
    }

}