package gymApp.gym.productsale.service.client;

import gymApp.gym.models.productsale.ProductSale;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
public class ProductSaleServiceClient {

    private final RestTemplate restTemplate;

    @Value("${product.sale.service.url}")
    String url;

    public ProductSaleServiceClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ProductSale getById(UUID id) {
        try {
            return restTemplate.getForEntity(prepareUrl(url + "/" + id.toString()), ProductSale.class).getBody();
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    public List<ProductSale> getAll() {
        try {
            return Arrays.asList(restTemplate.getForEntity(prepareUrl(url), ProductSale[].class).getBody());
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    public List<ProductSale> getAllByProductId(UUID productId) {
        try {
            return Arrays.asList(restTemplate.getForEntity(prepareUrl(url + "/product/" + productId.toString()), ProductSale[].class).getBody());
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    public ProductSale create(ProductSale productSale) {
        try {
            return restTemplate.postForEntity(prepareUrl(url), productSale, ProductSale.class).getBody();
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    private URI prepareUrl(String urlAsString) {
        try {
            return new URI((urlAsString));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Error while preparing url");
    }

}
