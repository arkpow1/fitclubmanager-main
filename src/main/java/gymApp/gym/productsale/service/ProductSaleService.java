package gymApp.gym.productsale.service;

import gymApp.gym.models.productsale.ProductSale;
import gymApp.gym.productsale.service.client.ProductSaleServiceClient;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ProductSaleService {

    private final ProductSaleServiceClient productSaleServiceClient;

    public ProductSaleService(ProductSaleServiceClient productSaleServiceClient) {
        this.productSaleServiceClient = productSaleServiceClient;
    }

    public ProductSale getById(UUID id) {
        return productSaleServiceClient.getById(id);
    }

    public List<ProductSale> getAll() {
        return productSaleServiceClient.getAll();
    }

    public List<ProductSale> getAllByProductId(UUID productId) {
        return productSaleServiceClient.getAllByProductId(productId);
    }

    public ProductSale create(ProductSale productSale, UUID userId) {
        productSale.setUserId(userId);
        return productSaleServiceClient.create(productSale);
    }

}