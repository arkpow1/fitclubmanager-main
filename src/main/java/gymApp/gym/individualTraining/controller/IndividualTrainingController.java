package gymApp.gym.individualTraining.controller;

import gymApp.gym.individualTraining.service.IndividualTrainingService;
import gymApp.gym.models.individualTraining.IndividualTraining;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.security.PermissionChecker;
import gymApp.gym.security.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "trainings/individual")
public class IndividualTrainingController {

    private final IndividualTrainingService individualTrainingService;
    private final PermissionChecker permissionChecker;
    private final TokenService tokenService;

    @Autowired
    public IndividualTrainingController(IndividualTrainingService individualTrainingService, PermissionChecker permissionChecker, TokenService tokenService) {
        this.individualTrainingService = individualTrainingService;
        this.permissionChecker = permissionChecker;
        this.tokenService = tokenService;
    }

    @GetMapping
    public List<IndividualTraining> getAll(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return individualTrainingService.getAll();
    }

    @GetMapping(value = "/client")
    public List<IndividualTraining> getAllForClient(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.CLIENT_ONLY);
        return individualTrainingService.getAllByClientId(tokenService.getUserId(request));
    }

    @GetMapping(value = "/trainer")
    public List<IndividualTraining> getAllForTrainer(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.TRAINER_ONLY);
        return individualTrainingService.getAllByTrainerId(tokenService.getUserId(request));
    }

    @DeleteMapping(value = "/setRemoved/{id}")
    public void setRemoved(@PathVariable UUID id,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        individualTrainingService.setRemoved(id);
    }

    @DeleteMapping(value = "/remove/{id}")
    public void deleteById(@PathVariable UUID id,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        individualTrainingService.deleteById(id);
    }

    @PostMapping
    public List<IndividualTraining> create(@RequestBody IndividualTraining individualTraining,
                                           @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate cyclicTo,
                                           @RequestParam(required = false) Integer daysRange,
                                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return individualTrainingService.create(tokenService.getUserId(request), individualTraining, cyclicTo, daysRange);
    }

    @GetMapping("/days/count")
    public List<LocalDate> getDaysForCyclic(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
                                            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate cyclicTo,
                                            @RequestParam Integer daysRange,
                                            HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return individualTrainingService.getDatesForCyclic(date, cyclicTo, daysRange);
    }

    @PatchMapping
    public IndividualTraining update(@RequestBody IndividualTraining individualTraining,
                                     HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return individualTrainingService.update(tokenService.getUserId(request), individualTraining);
    }

    @GetMapping(value = "/{id}")
    public IndividualTraining getById(@PathVariable UUID id,
                                      HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ALL);
        return individualTrainingService.getById(id);
    }
}
