package gymApp.gym.individualTraining.service;

import gymApp.gym.client.service.ClientService;
import gymApp.gym.individualTraining.mapper.IndividualTrainingMapper;
import gymApp.gym.models.individualTraining.IndividualTraining;
import gymApp.gym.trainer.service.TrainerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class IndividualTrainingService {

    private final IndividualTrainingMapper individualTrainingMapper;
    private final ClientService clientService;
    private final TrainerService trainerService;

    @Autowired
    public IndividualTrainingService(IndividualTrainingMapper individualTrainingMapper, ClientService clientService, TrainerService trainerService) {
        this.individualTrainingMapper = individualTrainingMapper;
        this.clientService = clientService;
        this.trainerService = trainerService;
    }

    public List<IndividualTraining> getAll() {
        return individualTrainingMapper.getAll();
    }

    public List<IndividualTraining> getAllByClientId(UUID userId) {
        return individualTrainingMapper.getAllByClientId(clientService.getByUserId(userId).getId());
    }

    public List<IndividualTraining> getAllByTrainerId(UUID userId) {
        return individualTrainingMapper.getAllByTrainerId(trainerService.getByUserId(userId).getId());
    }

    public void setRemoved(UUID id) {
        individualTrainingMapper.setRemoved(id);
    }

    public void deleteById(UUID id) {
        individualTrainingMapper.deleteById(id);
    }

    @Transactional
    public List<IndividualTraining> create(UUID userId, IndividualTraining individualTrainingTemplate, LocalDate cyclicTo, Integer daysRange) {
        ArrayList<IndividualTraining> individualTrainingsCreated = new ArrayList<>();
        if (cyclicTo != null && daysRange != null) {
            List<LocalDate> dates = getDatesForCyclic(individualTrainingTemplate.getDate(), cyclicTo, daysRange);
            dates.forEach(date -> {
                        individualTrainingTemplate.setDate(date);
                        individualTrainingsCreated.addAll(create(userId, individualTrainingTemplate, null, null));
                    }
            );
            return individualTrainingsCreated;
        }
        IndividualTraining individualTraining = prepareIndividualTrainingToCreate(userId, individualTrainingTemplate);
        individualTrainingMapper.create(individualTraining);
        return Collections.singletonList(individualTraining);
    }

    public List<LocalDate> getDatesForCyclic(LocalDate startDate, LocalDate endDate, Integer daysRange) {
        ArrayList<LocalDate> dates = new ArrayList<>();
        while (startDate.isBefore(endDate) || startDate.isEqual(endDate)) {
            dates.add(startDate);
            startDate = startDate.plusDays(daysRange);
        }
        return dates;
    }

    private IndividualTraining prepareIndividualTrainingToCreate(UUID userId, IndividualTraining individualTrainingTemplate) {
        return IndividualTraining.builder()
                .id(UUID.randomUUID())
                .name(individualTrainingTemplate.getName())
                .description(individualTrainingTemplate.getDescription())
                .price(individualTrainingTemplate.getPrice())
                .date(individualTrainingTemplate.getDate())
                .startingTime(individualTrainingTemplate.getStartingTime())
                .durationInMinutes(individualTrainingTemplate.getDurationInMinutes())
                .intensity(individualTrainingTemplate.getIntensity())
                .createdBy(userId)
                .creationDate(Instant.now())
                .removed(false)
                .trainerId(individualTrainingTemplate.getTrainerId())
                .clientId(individualTrainingTemplate.getClientId())
                .build();
    }

    public IndividualTraining update(UUID userId, IndividualTraining individualTrainingTemplate) {
        IndividualTraining existingIndividualTraining = individualTrainingMapper.getById(individualTrainingTemplate.getId());
        IndividualTraining individualTraining = prepareIndividualTrainingToUpdate(existingIndividualTraining, individualTrainingTemplate);
        individualTrainingMapper.update(individualTraining);
        return individualTraining;
    }

    private IndividualTraining prepareIndividualTrainingToUpdate(IndividualTraining existingIndividualTraining, IndividualTraining individualTrainingTemplate) {
        return IndividualTraining.builder()
                .id(existingIndividualTraining.getId())
                .name(individualTrainingTemplate.getName())
                .description(individualTrainingTemplate.getDescription())
                .price(individualTrainingTemplate.getPrice())
                .date(individualTrainingTemplate.getDate())
                .startingTime(individualTrainingTemplate.getStartingTime())
                .durationInMinutes(individualTrainingTemplate.getDurationInMinutes())
                .intensity(individualTrainingTemplate.getIntensity())
                .creationDate(existingIndividualTraining.getCreationDate())
                .removed(existingIndividualTraining.isRemoved())
                .trainerId(individualTrainingTemplate.getTrainerId())
                .clientId(individualTrainingTemplate.getClientId())
                .build();
    }

    public IndividualTraining getById(UUID id) {
        return individualTrainingMapper.getById(id);
    }
}
