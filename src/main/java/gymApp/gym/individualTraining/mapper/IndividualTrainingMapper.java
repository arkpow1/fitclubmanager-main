package gymApp.gym.individualTraining.mapper;

import gymApp.gym.models.individualTraining.IndividualTraining;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface IndividualTrainingMapper {

    List<IndividualTraining> getAll();

    List<IndividualTraining> getAllByClientId(@Param("clientId") UUID clientId);

    List<IndividualTraining> getAllByTrainerId(@Param("trainerId") UUID trainerId);

    IndividualTraining getById(@Param("id") UUID id);

    IndividualTraining getByTrainerId(@Param("trainerId") UUID trainerId);

    void setRemoved(@Param("id") UUID id);

    void deleteById(@Param("id") UUID id);

    void create(IndividualTraining groupTraining);

    void update(IndividualTraining groupTraining);
}
