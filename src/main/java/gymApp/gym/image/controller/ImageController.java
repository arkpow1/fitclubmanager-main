package gymApp.gym.image.controller;

import gymApp.gym.image.service.ImageService;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.security.PermissionChecker;
import gymApp.gym.security.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@RestController
@RequestMapping(value = "images")
public class ImageController {

    private final ImageService imageService;
    private final PermissionChecker permissionChecker;
    private final TokenService tokenService;

    @Autowired
    public ImageController(ImageService imageService, PermissionChecker permissionChecker, TokenService tokenService) {
        this.imageService = imageService;
        this.permissionChecker = permissionChecker;
        this.tokenService = tokenService;
    }

    @PostMapping
    public UUID create(@RequestParam MultipartFile image,
                       HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return imageService.create(image);
    }

    @GetMapping(value = "/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getById(@PathVariable UUID id,
                          HttpServletRequest request) {
        return imageService.getById(id);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity setRemoved(@PathVariable UUID id,
                                     HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        imageService.remove(id);
        return ResponseEntity.ok().build();
    }
}
