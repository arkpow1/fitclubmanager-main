package gymApp.gym.image.mapper;

import gymApp.gym.models.image.Image;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ImageMapper {

    void remove(@Param("id") UUID id);

    void create(Image image);

    Image getById(@Param("id") UUID id);
}
