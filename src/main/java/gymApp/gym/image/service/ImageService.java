package gymApp.gym.image.service;

import gymApp.gym.image.mapper.ImageMapper;
import gymApp.gym.models.image.Image;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.Instant;
import java.util.UUID;

@Service
public class ImageService {

    private final ImageMapper imageMapper;

    @Autowired
    public ImageService(ImageMapper imageMapper) {
        this.imageMapper = imageMapper;
    }

    public void remove(UUID id) {
        imageMapper.remove(id);
    }

    public UUID create(MultipartFile imageFile) {
        Image image = prepareImageToCreate(imageFile);
        UUID imageId = image.id;
        imageMapper.create(image);
        return imageId;
    }

    public byte[] getById(UUID id) {
        Image image =  imageMapper.getById(id);
        return image.getBytes();
    }

    private Image prepareImageToCreate(MultipartFile imageTemplate) {
        try {
            return Image.builder()
                    .id(UUID.randomUUID())
                    .bytes(imageTemplate.getBytes())
                    .creationDate(Instant.now())
                    .build();
        } catch (IOException e) {
            throw new IllegalStateException("Error while creating image");
        }
    }
}
