package gymApp.gym.address.mapper;

import gymApp.gym.models.address.Address;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface AddressMapper {

    List<Address> getAll();

    void setRemoved(@Param("id") Long id);

    void remove(@Param("id") Long id);

    void create(Address address);

    void update(Address address);

    Address getById(@Param("id") Long id);

    Address getForClientAddress(@Param("clientId") UUID clientId);

}
