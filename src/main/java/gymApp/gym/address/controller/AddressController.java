package gymApp.gym.address.controller;

import gymApp.gym.address.service.AddressService;
import gymApp.gym.models.address.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "address")
public class AddressController {

    private final AddressService addressService;

    @Autowired
    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @GetMapping
    public List<Address> getAll() {
        return addressService.getAll();
    }

    @DeleteMapping(value = "/setRemoved/{id}")
    public ResponseEntity setRemoved(@PathVariable Long id) {
        addressService.setRemoved(id);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/remove/{id}")
    public ResponseEntity remove(@PathVariable Long id) {
        addressService.remove(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping
    public Address create(@RequestBody Address address) {
        return addressService.create(address);
    }

    @PatchMapping
    public void update(@RequestBody Address address) {
        addressService.update(address);
    }

    @GetMapping(value = "/id/{id}")
    public Address getById(@PathVariable Long id) {
        return addressService.getById(id);
    }

    @GetMapping(value = "/cid/{clientId}")
    public Address getByClientId(@PathVariable UUID clientId) {
        return addressService.getForClientAddress(clientId);
    }
}
