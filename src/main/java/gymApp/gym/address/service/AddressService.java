package gymApp.gym.address.service;

import gymApp.gym.address.mapper.AddressMapper;
import gymApp.gym.models.address.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class AddressService {

    private final AddressMapper addressMapper;

    @Autowired
    public AddressService(AddressMapper addressMapper) {
        this.addressMapper = addressMapper;
    }

    public List<Address> getAll() {
        return addressMapper.getAll();
    }

    public void setRemoved(Long id) {
        addressMapper.setRemoved(id);
    }

    public void remove(Long id) {
        addressMapper.remove(id);
    }

    public Address create(Address address) {
        address.setRemoved(false);
        addressMapper.create(address);
        return address;
    }

    public void update(Address address) {
        addressMapper.update(address);
    }

    public Address getById(Long id) {
        return addressMapper.getById(id);
    }

    public Address getForClientAddress(UUID clientId) { // TODO nazwa
        return addressMapper.getForClientAddress(clientId);
    }
}
