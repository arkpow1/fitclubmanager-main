package gymApp.gym.trainer.service;

import gymApp.gym.models.trainer.Trainer;
import gymApp.gym.models.user.User;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.trainer.mapper.TrainerMapper;
import gymApp.gym.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Service
public class TrainerService {

    private final TrainerMapper trainerMapper;
    private final UserService userService;

    @Autowired
    public TrainerService(TrainerMapper trainerMapper, UserService userService) {
        this.trainerMapper = trainerMapper;
        this.userService = userService;
    }

    public List<Trainer> getAll() {
        return trainerMapper.getAll();
    }

    public void setRemoved(UUID id) {
        Trainer trainer = getById(id);
        trainerMapper.setRemoved(id);
        userService.setRemoved(trainer.getUserId());
    }

    public void deleteById(UUID id) {
        Trainer trainer = getById(id);
        trainerMapper.deleteById(id);
        userService.remove(trainer.getUserId());
    }

    @Transactional
    public Trainer create(UUID createdBy, Trainer trainerTemplate) {
        User user = userService.create(createdBy, User.builder().email(trainerTemplate.getEmail()).role(UserRole.TRAINER).build());
        trainerTemplate.setUserId(userService.getByEmail(user.getEmail()).getId());
        Trainer trainer = prepareTrainerToSave(trainerTemplate);
        validateTrainer(trainer);
        trainerMapper.create(trainer);
        return trainer;
    }

    private Trainer prepareTrainerToSave(Trainer trainerTemplate) {
        return Trainer
                .builder()
                .id(UUID.randomUUID())
                .email(trainerTemplate.getEmail())
                .name(trainerTemplate.getName())
                .surname(trainerTemplate.getSurname())
                .description(trainerTemplate.getDescription())
                .birthdate(trainerTemplate.getBirthdate())
                .pesel(null)
                .phoneNumber(trainerTemplate.getPhoneNumber())
                .notifications(true)
                .gender(trainerTemplate.getGender())
                .creationDate(Instant.now())
                .removed(false)
                .userId(trainerTemplate.getUserId())
                .build();
    }

    // TODO prepare to update
    @Transactional
    public Trainer update(Trainer trainer) {
        Trainer existingTrainer = getById(trainer.getId());
        validateTrainer(trainer);
        trainerMapper.update(trainer);
        userService.updateEmail(existingTrainer.getUserId(), trainer.getEmail());
        return getById(trainer.getId());
    }

    private void validateTrainer(Trainer trainer) {
        if (!trainer.getPhoneNumber().matches("[0-9]+"))
            throw new IllegalStateException("Wrong credentials");
    }

    public Trainer getById(UUID id) {
        return trainerMapper.getById(id);
    }

    public Trainer getByUserId(UUID userId) {
        return trainerMapper.getByUserId(userId);
    }

    public List<Trainer> getWithNotifications() {
        return trainerMapper.getWithNotifications();
    }

    public List<Trainer> getWithShowProfile() {
        return trainerMapper.getWithShowProfile();
    }
}
