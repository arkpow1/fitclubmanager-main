package gymApp.gym.trainer.mapper;


import gymApp.gym.models.trainer.Trainer;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TrainerMapper {

    List<Trainer> getAll();

    Trainer getById(@Param("id") UUID id);

    Trainer getByUserId(@Param("userId") UUID userId);

    List<Trainer> getWithNotifications();

    List<Trainer> getWithShowProfile();

    void setRemoved(@Param("id") UUID id);

    void deleteById(@Param("id") UUID id);

    void create(Trainer trainer);

    void update(Trainer trainer);

}
