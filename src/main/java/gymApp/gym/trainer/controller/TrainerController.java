package gymApp.gym.trainer.controller;


import gymApp.gym.models.trainer.Trainer;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.security.PermissionChecker;
import gymApp.gym.security.TokenService;
import gymApp.gym.trainer.service.TrainerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "trainers")
public class TrainerController {

    private final TrainerService trainerService;
    private final PermissionChecker permissionChecker;
    private final TokenService tokenService;

    @Autowired
    public TrainerController(TrainerService trainerService, PermissionChecker permissionChecker, TokenService tokenService) {
        this.trainerService = trainerService;
        this.permissionChecker = permissionChecker;
        this.tokenService = tokenService;
    }

    @GetMapping(value = "/all")
    public List<Trainer> getAll(HttpServletRequest request) {
        return trainerService.getAll();
    }

    @DeleteMapping(value = "/setRemoved/{id}")
    public ResponseEntity setRemoved(@PathVariable("id") UUID id,
                                     HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        trainerService.setRemoved(id);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/deleteById/{id}")
    public ResponseEntity deleteById(@PathVariable("id") UUID id,
                                     HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ADMIN_ONLY);
        trainerService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping
    public Trainer create(@RequestBody Trainer trainer,
                          HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ADMIN_ONLY);
        return trainerService.create(tokenService.getUserId(request), trainer);
    }

    @PatchMapping
    public Trainer update(@RequestBody Trainer trainer,
                          HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return trainerService.update(trainer);
    }

    @GetMapping(value = "/id/{id}")
    public Trainer getById(@PathVariable UUID id,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return trainerService.getById(id);
    }

    @GetMapping(value = "/self")
    public Trainer getSelf(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.TRAINER_ONLY);
        return trainerService.getByUserId(tokenService.getUserId(request));
    }

    @GetMapping(value = "/withNotifications")
    public List<Trainer> getWithNotifications(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return trainerService.getWithNotifications();
    }

    @GetMapping(value = "/withShowProfile")
    public List<Trainer> getWithShowProfile() {
        return trainerService.getWithShowProfile();
    }
}
