package gymApp.gym.productreturn.boundary;

import gymApp.gym.models.productreturn.ProductReturn;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.productreturn.service.ProductReturnService;
import gymApp.gym.security.PermissionChecker;
import gymApp.gym.security.TokenService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/productReturns")
public class ProductReturnController {

    private final ProductReturnService productReturnService;
    private final PermissionChecker permissionChecker;
    private final TokenService tokenService;

    public ProductReturnController(ProductReturnService productReturnService, PermissionChecker permissionChecker, TokenService tokenService) {
        this.productReturnService = productReturnService;
        this.permissionChecker = permissionChecker;
        this.tokenService = tokenService;
    }

    @GetMapping("/{id}")
    public ProductReturn getById(@PathVariable UUID id,
                                 HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return productReturnService.getById(id);
    }

    @GetMapping
    public List<ProductReturn> getAll(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return productReturnService.getAll();
    }

    @GetMapping("/product/{productId}")
    public List<ProductReturn> getAllByProductId(@PathVariable UUID productId, HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return productReturnService.getAllByProductId(productId);
    }

    @GetMapping("/productSale/{productSaleId}")
    public List<ProductReturn> getAllByProductSaleId(@PathVariable UUID productSaleId, HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return productReturnService.getAllByProductSaleId(productSaleId);
    }

    @PostMapping
    public ProductReturn create(@RequestBody ProductReturn productReturn,
                                HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return productReturnService.create(productReturn, tokenService.getUserId(request));
    }

}