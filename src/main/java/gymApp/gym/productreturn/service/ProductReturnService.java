package gymApp.gym.productreturn.service;

import gymApp.gym.models.productreturn.ProductReturn;
import gymApp.gym.productreturn.service.client.ProductReturnServiceClient;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ProductReturnService {

    private final ProductReturnServiceClient productReturnServiceClient;

    public ProductReturnService(ProductReturnServiceClient productReturnServiceClient) {
        this.productReturnServiceClient = productReturnServiceClient;
    }

    public ProductReturn getById(UUID id) {
        return productReturnServiceClient.getById(id);
    }

    public List<ProductReturn> getAll() {
        return productReturnServiceClient.getAll();
    }

    public List<ProductReturn> getAllByProductId(UUID productId) {
        return productReturnServiceClient.getAllByProductId(productId);
    }

    public List<ProductReturn> getAllByProductSaleId(UUID productSaleId) {
        return productReturnServiceClient.getAllByProductSaleId(productSaleId);
    }

    public ProductReturn create(ProductReturn productReturn, UUID userId) {
        productReturn.setUserId(userId);
        return productReturnServiceClient.create(productReturn);
    }

}