package gymApp.gym.productreturn.service.client;

import gymApp.gym.models.productreturn.ProductReturn;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
public class ProductReturnServiceClient {

    private final RestTemplate restTemplate;

    @Value("${product.return.service.url}")
    String url;

    public ProductReturnServiceClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ProductReturn getById(UUID id) {
        try {
            return restTemplate.getForEntity(prepareUrl(url + "/" + id.toString()), ProductReturn.class).getBody();
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    public List<ProductReturn> getAll() {
        try {
            return Arrays.asList(restTemplate.getForEntity(prepareUrl(url), ProductReturn[].class).getBody());
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    public List<ProductReturn> getAllByProductId(UUID productId) {
        try {
            return Arrays.asList(restTemplate.getForEntity(prepareUrl(url + "/product/" + productId.toString()), ProductReturn[].class).getBody());
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    public List<ProductReturn> getAllByProductSaleId(UUID productSaleId) {
        try {
            return Arrays.asList(restTemplate.getForEntity(prepareUrl(url + "/productSale/" + productSaleId.toString()), ProductReturn[].class).getBody());
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    public ProductReturn create(ProductReturn productReturn) {
        try {
            return restTemplate.postForEntity(prepareUrl(url), productReturn, ProductReturn.class).getBody();
        } catch (HttpClientErrorException ex) {
            throw new IllegalStateException("Client error during rest communication", ex);
        } catch (HttpServerErrorException ex) {
            throw new IllegalStateException("Server error during rest communication", ex);
        }
    }

    private URI prepareUrl(String urlAsString) {
        try {
            return new URI((urlAsString));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Error while preparing url");
    }

}
