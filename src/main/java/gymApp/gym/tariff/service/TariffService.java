package gymApp.gym.tariff.service;

import gymApp.gym.models.tariff.Tariff;
import gymApp.gym.tariff.mapper.TariffMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Service
public class TariffService {

    private final TariffMapper tariffMapper;

    @Autowired
    public TariffService(TariffMapper tariffMapper) {
        this.tariffMapper = tariffMapper;
    }

    public List<Tariff> getAll() {
        return tariffMapper.getAll();
    }

    public void setRemoved(UUID id) {
        tariffMapper.setRemoved(id);
    }

    public void deleteById(UUID id) {
        tariffMapper.deleteById(id);
    }

    public Tariff create(UUID userId, Tariff tariffTemplate) {
        Tariff tariff = prepareTariffToCreate(userId, tariffTemplate);
        tariffMapper.create(tariff);
        return tariff;
    }

    private Tariff prepareTariffToCreate(UUID userId, Tariff tariffTemplate) {
        return Tariff.builder()
                .id(UUID.randomUUID())
                .name(tariffTemplate.getName())
                .price(tariffTemplate.getPrice())
                .currency(tariffTemplate.getCurrency())
                .settlementType(tariffTemplate.getSettlementType())
                .createdBy(userId)
                .creationDate(Instant.now())
                .removed(false)
                .build();
    }

    public Tariff update(UUID userId, Tariff tariffTemplate) {
        Tariff existingTariff = tariffMapper.getById(tariffTemplate.getId());
        Tariff tariff = prepareTariffToUpdate(existingTariff, tariffTemplate);
        tariffMapper.update(tariff);
        return tariff;
    }

    private Tariff prepareTariffToUpdate(Tariff existingTariff, Tariff tariffTemplate) {
        return Tariff.builder()
                .id(existingTariff.getId())
                .name(tariffTemplate.getName())
                .price(tariffTemplate.getPrice())
                .currency(tariffTemplate.getCurrency())
                .settlementType(tariffTemplate.getSettlementType())
                .createdBy(existingTariff.getCreatedBy())
                .creationDate(existingTariff.getCreationDate())
                .removed(existingTariff.isRemoved())
                .build();
    }

    public Tariff getById(UUID id) {
        return tariffMapper.getById(id);
    }
}
