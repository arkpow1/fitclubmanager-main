package gymApp.gym.tariff.mapper;

import gymApp.gym.models.tariff.Tariff;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TariffMapper {

    List<Tariff> getAll();

    Tariff getById(@Param("id") UUID id);

    void setRemoved(@Param("id") UUID id);

    void deleteById(@Param("id") UUID id);

    void create(Tariff tarif);

    void update(Tariff tarif);
}
