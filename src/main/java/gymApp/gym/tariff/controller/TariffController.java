package gymApp.gym.tariff.controller;

import gymApp.gym.models.tariff.Tariff;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.security.PermissionChecker;
import gymApp.gym.security.TokenService;
import gymApp.gym.tariff.service.TariffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "tariff")
public class TariffController {

    private final TariffService tariffService;
    private final PermissionChecker permissionChecker;
    private final TokenService tokenService;

    @Autowired
    public TariffController(TariffService tariffService, PermissionChecker permissionChecker, TokenService tokenService) {
        this.tariffService = tariffService;
        this.permissionChecker = permissionChecker;
        this.tokenService = tokenService;
    }

    @GetMapping(value = "/all")
    public List<Tariff> getAll(HttpServletRequest request) {
        return tariffService.getAll();
    }

    @DeleteMapping(value = "/setRemoved/{id}")
    public void setRemoved(@PathVariable UUID id,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ADMIN_ONLY);
        tariffService.setRemoved(id);
    }

    @DeleteMapping(value = "/remove/{id}")
    public void deleteById(@PathVariable UUID id,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ADMIN_ONLY);
        tariffService.deleteById(id);
    }

    @PostMapping
    public Tariff create(@RequestBody Tariff tariff,
                         HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ADMIN_ONLY);
        return tariffService.create(tokenService.getUserId(request), tariff);
    }

    @PatchMapping
    public Tariff update(@RequestBody Tariff tariff,
                         HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ADMIN_ONLY);
        return tariffService.update(tokenService.getUserId(request), tariff);
    }

    @GetMapping(value = "/{id}")
    public Tariff getById(@PathVariable UUID id,
                          HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ADMIN_ONLY);
        return tariffService.getById(id);
    }
}
