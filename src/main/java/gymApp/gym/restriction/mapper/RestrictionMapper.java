package gymApp.gym.restriction.mapper;

import gymApp.gym.models.restriction.RestrictedAction;
import gymApp.gym.models.restriction.Restriction;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface RestrictionMapper {

    List<Restriction> getAll();

    Restriction getById(@Param("id") UUID id);

    List<Restriction> getAllByRestrictedActionAndUserId(@Param("restrictedAction") RestrictedAction restrictedAction, @Param("userId") UUID userId);

    List<Restriction> getAllByUserId(@Param("userId") UUID userId);

    void setRemoved(@Param("id") UUID id);

    void deleteById(@Param("id") UUID id);

    void create(Restriction restriction);
}
