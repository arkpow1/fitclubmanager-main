package gymApp.gym.restriction.exception;

import gymApp.gym.common.exception.ExceptionCode;
import gymApp.gym.common.exception.GymAppException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class ActionRestrictedException extends GymAppException {
    public ActionRestrictedException(String message) {
        super(message, ExceptionCode.ACTION_RESTRICTED);
    }
}
