package gymApp.gym.restriction.service;

import gymApp.gym.models.restriction.RestrictedAction;
import gymApp.gym.models.restriction.Restriction;
import gymApp.gym.restriction.exception.ActionRestrictedException;
import gymApp.gym.restriction.mapper.RestrictionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
public class RestrictionService {

    private final RestrictionMapper restrictionMapper;

    @Autowired
    public RestrictionService(RestrictionMapper restrictionMapper) {
        this.restrictionMapper = restrictionMapper;
    }

    public List<Restriction> getAll() {
        return restrictionMapper.getAll();
    }

    public Restriction getById(UUID id) {
        return restrictionMapper.getById(id);
    }

    public List<Restriction> getAllByRestrictedActionAndUserId(RestrictedAction restrictedAction, UUID userId) {
        return restrictionMapper.getAllByRestrictedActionAndUserId(restrictedAction, userId);
    }

    public boolean checkIfActionIsRestrictedOrThrow(RestrictedAction restrictedAction, UUID userId) {
        List<Restriction> restrictions = restrictionMapper.getAllByRestrictedActionAndUserId(restrictedAction, userId);
        restrictions.forEach(restriction -> {
            if (restriction.getRestrictedUntil() == null)
                throw new ActionRestrictedException("This action is restricted");
            if (LocalDate.now().isBefore(restriction.getRestrictedUntil()) || LocalDate.now().isEqual(restriction.getRestrictedUntil()))
                throw new ActionRestrictedException("This action is restricted");
        });
        return true;
    }

    public List<Restriction> getAllByUserId(UUID userId) {
        return restrictionMapper.getAllByUserId(userId);
    }

    public void setRemoved(UUID id) {
        restrictionMapper.setRemoved(id);
    }

    public void deleteById(UUID id) {
        restrictionMapper.deleteById(id);
    }

    public Restriction create(UUID createdBy, Restriction restrictionTemplate) {
        Restriction restriction = prepareRestrictionToCreate(createdBy, restrictionTemplate);
        restrictionMapper.create(restriction);
        return restriction;
    }

    private Restriction prepareRestrictionToCreate(UUID createdBy, Restriction restrictionTemplate) {
        return Restriction.builder()
                .id(UUID.randomUUID())
                .restrictedAction(restrictionTemplate.getRestrictedAction())
                .restrictedUntil(restrictionTemplate.getRestrictedUntil())
                .reason(restrictionTemplate.getReason())
                .createdBy(createdBy)
                .creationDate(Instant.now())
                .removed(false)
                .userId(restrictionTemplate.getUserId())
                .build();
    }
}
