package gymApp.gym.restriction.controller;

import gymApp.gym.models.restriction.Restriction;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.restriction.service.RestrictionService;
import gymApp.gym.security.PermissionChecker;
import gymApp.gym.security.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "restrictions")
public class RestrictionController {

    private final RestrictionService restrictionService;
    private final PermissionChecker permissionChecker;
    private final TokenService tokenService;

    @Autowired
    public RestrictionController(RestrictionService restrictionService, PermissionChecker permissionChecker, TokenService tokenService) {
        this.restrictionService = restrictionService;
        this.permissionChecker = permissionChecker;
        this.tokenService = tokenService;
    }

    @GetMapping(value = "/all")
    public List<Restriction> getAll(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return restrictionService.getAll();
    }

    @GetMapping(value = "/{id}")
    public Restriction getById(@PathVariable UUID id,
                               HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return restrictionService.getById(id);
    }

    @GetMapping(value = "/all/user/{userId}")
    public List<Restriction> getAllByUserId(@PathVariable UUID userId,
                                            HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return restrictionService.getAllByUserId(userId);
    }

    @GetMapping(value = "/all/self")
    public List<Restriction> getAllSelf(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.CLIENT_ONLY);
        return restrictionService.getAllByUserId(tokenService.getUserId(request));
    }

    @DeleteMapping(value = "/setRemoved/{id}")
    public void setRemoved(@PathVariable UUID id,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        restrictionService.setRemoved(id);
    }

    @DeleteMapping(value = "/remove/{id}")
    public void deleteById(@PathVariable UUID id,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        restrictionService.deleteById(id);
    }

    @PostMapping
    public Restriction create(@RequestBody Restriction restriction,
                              HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return restrictionService.create(tokenService.getUserId(request), restriction);
    }
}
