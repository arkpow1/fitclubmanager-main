package gymApp.gym.user.exception;

import gymApp.gym.common.exception.ExceptionCode;
import gymApp.gym.common.exception.GymAppException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class InvalidEmailException extends GymAppException {
    public InvalidEmailException(String message) {
        super(message, ExceptionCode.INVALID_EMAIL);
    }
}
