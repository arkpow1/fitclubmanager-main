package gymApp.gym.user.exception;

import gymApp.gym.common.exception.ExceptionCode;
import gymApp.gym.common.exception.GymAppException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class InvalidPasswordException extends GymAppException {
    public InvalidPasswordException(String message) {
        super(message, ExceptionCode.INVALID_PASSWORD);
    }
}
