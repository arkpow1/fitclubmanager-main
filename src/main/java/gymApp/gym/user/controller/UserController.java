package gymApp.gym.user.controller;

import gymApp.gym.models.user.User;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.security.PermissionChecker;
import gymApp.gym.security.TokenService;
import gymApp.gym.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "users")
public class UserController {

    private final UserService userService;
    private final PermissionChecker permissionChecker;
    private final TokenService tokenService;

    @Autowired
    public UserController(UserService userService, PermissionChecker permissionChecker, TokenService tokenService) {
        this.userService = userService;
        this.permissionChecker = permissionChecker;
        this.tokenService = tokenService;
    }

    @GetMapping
    public List<User> getAll(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return userService.getAll();
    }

    @DeleteMapping(value = "setRemoved")
    public void setRemovedSelf(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ALL);
        userService.setRemoved(tokenService.getUserId(request));
    }

    @DeleteMapping(value = "setRemoved/{id}")
    public void setRemoved(@PathVariable UUID id,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ALL);
        userService.setRemoved(id);
    }

    @DeleteMapping(value = "remove")
    public void removeSelf(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        userService.remove(tokenService.getUserId(request));
    }

    @DeleteMapping(value = "remove/{id}")
    public void remove(@PathVariable UUID id,
                       HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        userService.remove(id);
    }

    @PostMapping
    public User create(@RequestBody User user,
                       HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return userService.create(tokenService.getUserId(request), user);
    }

    @GetMapping(value = "{id}")
    public User getById(@PathVariable UUID id,
                        HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return userService.getById(id);
    }

    @GetMapping(value = "/self")
    public User getSelf(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.CLIENT_ONLY);
        return userService.getById(tokenService.getUserId(request));
    }

    @GetMapping(value = "/email")
    public User getByEmail(@RequestParam("email") String email,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ALL);
        return userService.getByEmail(email);
    }

    @GetMapping(value = "{role}")
    public List<User> getByRole(@PathVariable UserRole role,
                                HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ADMIN_ONLY);
        return userService.getByRole(role);
    }

    @PatchMapping(value = "/password")
    public void updateSelfPassword(@RequestParam("oldPassword") String oldPassword,
                                   @RequestParam("newPassword") String newPassword,
                                   HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ALL);
        userService.updateSelfPassword(tokenService.getUserId(request), oldPassword, newPassword);
    }

    @PatchMapping(value = "/password/{id}")
    public void updatePassword(@PathVariable("id") UUID id,
                               @RequestParam("newPassword") String newPassword,
                               HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        userService.updatePassword(id, newPassword);
    }

    //TODO remove
    @PatchMapping(value = "/email/")
    public void updateSelfEmail(@RequestParam("email") String email,
                                HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ADMIN_ONLY);
        userService.updateEmail(tokenService.getUserId(request), email);
    }
}
