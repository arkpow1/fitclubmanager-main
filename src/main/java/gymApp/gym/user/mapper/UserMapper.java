package gymApp.gym.user.mapper;

import gymApp.gym.models.user.User;
import gymApp.gym.models.user.UserRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserMapper {

    List<User> getAll();

    void setRemoved(@Param("id") UUID id);

    void remove(@Param("id") UUID id);

    void create(User user);

    void update(User user);

    User getById(@Param("id") UUID id);

    User getByEmail(@Param("email") String email);

    List<User> getByRole(@Param("role") UserRole role);

    void updatePassword(@Param("id") UUID id, @Param("password") String password);

    void updateEmail(@Param("id") UUID id, @Param("email") String email);
}
