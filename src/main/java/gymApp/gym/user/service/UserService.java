package gymApp.gym.user.service;

import gymApp.gym.common.exception.NoPermissionException;
import gymApp.gym.mail.service.EmailService;
import gymApp.gym.models.user.User;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.user.exception.InvalidPasswordException;
import gymApp.gym.user.generator.UserPasswordGenerator;
import gymApp.gym.user.mapper.UserMapper;
import gymApp.gym.user.validator.UserFieldsValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Service
public class UserService {

    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;
    private final UserFieldsValidator userFieldsValidator;
    private final UserPasswordGenerator userPasswordGenerator;
    private final EmailService emailService;
    @Value("${first.password.email.subject}")
    private String firstPasswordEmailSubject;
    @Value("${first.password.email.text}")
    private String firstPasswordEmailText;

    @Autowired
    public UserService(UserMapper userMapper, PasswordEncoder passwordEncoder, UserFieldsValidator userFieldsValidator, UserPasswordGenerator userPasswordGenerator, EmailService emailService) {
        this.userMapper = userMapper;
        this.passwordEncoder = passwordEncoder;
        this.userFieldsValidator = userFieldsValidator;
        this.userPasswordGenerator = userPasswordGenerator;
        this.emailService = emailService;
    }

    public List<User> getAll() {
        return userMapper.getAll();
    }

    public void setRemoved(UUID id) {
        userMapper.setRemoved(id);
    }

    public void remove(UUID id) {
        userMapper.remove(id);
    }

    @Transactional
    public User create(UUID createdBy, User userTemplate) {
        UserRole createdByRole = userMapper.getById(createdBy).getRole();
        if (checkUserPermissionToCreateNewUserWithGivenRole(createdByRole, userTemplate.getRole())) {
            String password = userPasswordGenerator.generateRandomPassword();
            User user = prepareUserToCreate(createdBy, password, userTemplate);
            userFieldsValidator.validateUser(user);
            userMapper.create(user);
            emailService.sendSimpleMessage(user.getEmail(), firstPasswordEmailSubject, String.format(firstPasswordEmailText, password));
            return user;
        } else {
            throw new NoPermissionException(String.format("User with role %s can't create an user with role %s", createdByRole, userTemplate.getRole()));
        }
    }

    private User prepareUserToCreate(UUID createdBy, String password, User userTemplate) {
        return User.builder()
                .id(UUID.randomUUID())
                .email(userTemplate.getEmail())
                .password(passwordEncoder.encode(password))
                .role(userTemplate.getRole())
                .createdBy(createdBy)
                .creationDate(Instant.now())
                .removed(false)
                .build();
    }

    private boolean checkUserPermissionToCreateNewUserWithGivenRole(UserRole createdByRole, UserRole creatingRole) {
        switch (createdByRole) {
            case ADMIN:
                return true;
            case RECEPTIONIST:
                return creatingRole == UserRole.CLIENT;
            case TRAINER:
                return creatingRole == UserRole.CLIENT;
            case CLIENT:
                return false;
        }
        return false;
    }

    public User update(UUID userId, User userTemplate) {
        userFieldsValidator.validateUser(userTemplate);
        User databaseUser = userMapper.getById(userId);
        User user = prepareUserToUpdate(userTemplate, databaseUser);
        userMapper.update(user);
        return user;
    }

    private User prepareUserToUpdate(User userTemplate, User databaseUser) {
        return User.builder()
                .id(databaseUser.getId())
                .email(userTemplate.getEmail())
                .password(passwordEncoder.encode(userTemplate.getPassword()))
                .role(databaseUser.getRole())
                .createdBy(databaseUser.getCreatedBy())
                .creationDate(databaseUser.getCreationDate())
                .removed(databaseUser.getRemoved())
                .build();
    }

    public User getById(UUID id) {
        return userMapper.getById(id);
    }

    public User getByEmail(String email) {
        return userMapper.getByEmail(email);
    }

    public List<User> getByRole(UserRole role) {
        return userMapper.getByRole(role);
    }

    public void updateSelfPassword(UUID id, String oldPassword, String newPassword) {
        userFieldsValidator.validatePassword(newPassword);
        User user = userMapper.getById(id);
        if (passwordEncoder.matches(oldPassword, user.getPassword())) {
            userMapper.updatePassword(id, passwordEncoder.encode(newPassword));
        } else {
            throw new InvalidPasswordException("Old password is invalid.");
        }
    }

    public void updatePassword(UUID id, String newPassword) {
        userFieldsValidator.validatePassword(newPassword);
        userMapper.updatePassword(id, passwordEncoder.encode(newPassword));
    }

    public void updateEmail(UUID id, String newEmail) {
        userFieldsValidator.validateEmail(newEmail);
        userMapper.updateEmail(id, newEmail);
    }
}
