package gymApp.gym.user.validator;

import gymApp.gym.models.user.User;
import gymApp.gym.user.exception.InvalidEmailException;
import gymApp.gym.user.exception.InvalidPasswordException;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Component;

@Component
public class UserFieldsValidator {

    private EmailValidator emailValidator = EmailValidator.getInstance();
    private String passwordRegex = ("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()-_=+<>,./?;:{}])(?=\\S+$).{8,}$");

    public void validateUser(User user) {
        validateEmail(user.getEmail());
        validatePassword(user.getPassword());
    }

    public void validateEmail(String email) {
        if (!emailValidator.isValid(email)) {
            throw new InvalidEmailException(String.format("Email %s is invalid.", email));
        }
    }

    public void validatePassword(String password) {
        if (!password.matches(passwordRegex)) {
            throw new InvalidPasswordException(String.format("Password %s is invalid.", password));
        }
    }
}
