package gymApp.gym.models.productsupply;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductSupply {

    private UUID id;

    private BigDecimal price;

    private BigDecimal totalPrice;

    private Integer amount;

    private Instant creationDate;

    private boolean removed;

    private UUID productId;

    private UUID userId;

    private String productName;
}