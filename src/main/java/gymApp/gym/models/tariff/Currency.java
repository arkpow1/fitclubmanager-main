package gymApp.gym.models.tariff;

public enum Currency {
    PLN, EUR, USD
}
