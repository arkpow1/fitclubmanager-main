package gymApp.gym.models.tariff;

import java.util.EnumSet;
import java.util.Set;

public enum SettlementType {
    MONTH, QUARTER, SIX_MONTH, YEAR,
    ONE_TRAINING, FOUR_TRAININGS, EIGHT_TRAININGS, TWELVE_TRAININGS;

    public static final Set<SettlementType> TIME_TYPE = EnumSet.of(MONTH, QUARTER, SIX_MONTH, YEAR);
    public static final Set<SettlementType> QUANTITY_TYPE = EnumSet.of(ONE_TRAINING, FOUR_TRAININGS, EIGHT_TRAININGS, TWELVE_TRAININGS);
}
