package gymApp.gym.models.tariff;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Tariff {

    private UUID id;

    private String name;

    private BigDecimal price;

    private Currency currency;

    private SettlementType settlementType;

    private UUID createdBy;

    private Instant creationDate;

    private boolean removed;
}
