package gymApp.gym.models.pass;

import gymApp.gym.models.client.Client;
import gymApp.gym.models.tariff.Currency;
import gymApp.gym.models.tariff.SettlementType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Pass {

    private UUID id;

    private String tariffName;

    private SettlementType tariffSettlementType;

    private BigDecimal totalPrice;

    private BigDecimal unitPrice;

    private Currency currency;

    private LocalDate activationDate;

    private LocalDate expirationDate;

    private Long daysToExpire;

    private Integer entriesLeft;

    private boolean active;

    private boolean paid;

    private UUID createdBy;

    private Instant creationDate;

    private boolean removed;

    private UUID clientId;

    private Client client;

    public Long getDaysToExpire() {
        if (SettlementType.TIME_TYPE.contains(getTariffSettlementType()) && timeEqualsNowOrBetweenActivationAndExpirationDate()) {
            return ChronoUnit.DAYS.between(LocalDate.now(), expirationDate);
        }
        return null;
    }

    public boolean getActive() {
        if (SettlementType.TIME_TYPE.contains(tariffSettlementType)) {
            return (timeEqualsNowOrBetweenActivationAndExpirationDate() && paid);
        }
        return ((entriesLeft > 0) && paid);
    }

    private Boolean timeEqualsNowOrBetweenActivationAndExpirationDate() {
        return ((LocalDate.now().isEqual(activationDate) || LocalDate.now().isEqual(expirationDate)) || (LocalDate.now().isAfter(activationDate) && LocalDate.now().isBefore(expirationDate)));
    }
}
