package gymApp.gym.models.groupTrainingOffer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GroupTrainingOffer {

    private UUID id;

    private String name;

    private String description;

    private Integer intensity;

    private UUID createdBy;

    private Instant creationDate;

    private boolean removed;

    private UUID imageId;

    public void setIntensity(int intensity) {
        if (intensity >= 0 && intensity <= 5) {
            this.intensity = intensity;
        } else {
            throw new IllegalStateException("Training intensity range is 0 to 5.");
        }
    }
}
