package gymApp.gym.models.groupTrainingMember;

import gymApp.gym.models.client.Client;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GroupTrainingMember {

    private UUID id;

    private Instant creationDate;

    private boolean removed;

    private UUID groupTrainingId;

    private UUID clientId;

    private Client client;
    //TODO double UQ groupTrainingId-clientId
}
