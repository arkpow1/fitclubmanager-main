package gymApp.gym.models.individualTrainingOffer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IndividualTrainingOffer {

    private UUID id;

    private String name;

    private String description;

    private String targetClientDescription;

    private UUID createdBy;

    private Instant creationDate;

    private boolean removed;

    private UUID imageId;
}
