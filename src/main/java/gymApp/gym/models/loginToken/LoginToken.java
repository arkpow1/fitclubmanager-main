package gymApp.gym.models.loginToken;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginToken {

    private UUID id;

    private String token;

    private String email;

    private Instant creationDate;

    private Boolean removed;
}
