package gymApp.gym.models.address;

import gymApp.gym.models.client.Client;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Address {

    private Long id;

    private String country;

    private String state;

    private String city;

    private String street;

    private Long houseNumber;

    private Long apartmentNumber;

    private String postOffice;

    private String postalCode;

    private Client client;

    private boolean removed;
}
