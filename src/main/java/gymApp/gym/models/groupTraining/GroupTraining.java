package gymApp.gym.models.groupTraining;

import gymApp.gym.models.trainer.Trainer;
import gymApp.gym.models.training.Training;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GroupTraining implements Training {

    private UUID id;

    private String name;

    private String description;

    private LocalDate date;

    private LocalTime startingTime;

    private Integer durationInMinutes;

    private Integer maxMembersAmount;

    private Integer spotsLeft;

    private Gender gender;

    private Integer intensity;

    private Boolean membership;

    private UUID createdBy;

    private Instant creationDate;

    private boolean removed;

    private UUID trainerId;

    private Trainer trainer;

    public void setIntensity(int intensity) {
        if (intensity >= 0 && intensity <= 5) {
            this.intensity = intensity;
        } else {
            throw new IllegalStateException("Training intensity range is 0 to 5.");
        }
    }
}
