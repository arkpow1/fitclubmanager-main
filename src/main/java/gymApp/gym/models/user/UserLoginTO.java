package gymApp.gym.models.user;

import lombok.Data;

@Data
public class UserLoginTO {

    private String email;

    private String password;

    private String token;

    private UserRole role;
}
