package gymApp.gym.models.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {

    private UUID id;

    private String email;

    private String password;

    private UserRole role;

    private UUID createdBy;

    private Instant creationDate;

    private Boolean removed;
}
