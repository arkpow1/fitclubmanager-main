package gymApp.gym.models.user;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserLoginTOHelper {

    private String email;

    private String password;

    private String token;

    private UserRole role;
}
