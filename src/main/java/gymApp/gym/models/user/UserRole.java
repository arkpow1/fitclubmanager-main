package gymApp.gym.models.user;

import java.util.EnumSet;
import java.util.Set;

public enum UserRole {

    ADMIN, CLIENT, RECEPTIONIST, TRAINER;

    public static final Set<UserRole> ALL = EnumSet.of(ADMIN, CLIENT, RECEPTIONIST, TRAINER);
    public static final Set<UserRole> WORKERS = EnumSet.of(ADMIN, RECEPTIONIST, TRAINER);
    public static final Set<UserRole> ADMIN_ONLY = EnumSet.of(ADMIN);
    public static final Set<UserRole> CLIENT_ONLY = EnumSet.of(CLIENT);
    public static final Set<UserRole> TRAINER_ONLY = EnumSet.of(TRAINER);
    public static final Set<UserRole> RECEPTIONIST_ONLY = EnumSet.of(RECEPTIONIST);
}
