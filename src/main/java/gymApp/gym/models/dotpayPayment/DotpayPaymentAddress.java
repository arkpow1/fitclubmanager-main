package gymApp.gym.models.dotpayPayment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DotpayPaymentAddress {

    private String street;

    private Integer buildingNumber;

    private String postcode;

    private String city;

    private String region;

    private String country;
}
