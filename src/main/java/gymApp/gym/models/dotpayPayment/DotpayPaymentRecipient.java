package gymApp.gym.models.dotpayPayment;

public class DotpayPaymentRecipient {

    private String account_number;

    private String company;

    private String first_name;

    private String last_name;

    private DotpayPaymentAddress address;
}
