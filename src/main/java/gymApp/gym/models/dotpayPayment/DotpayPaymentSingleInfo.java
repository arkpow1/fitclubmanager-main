package gymApp.gym.models.dotpayPayment;

import lombok.Data;

@Data
public class DotpayPaymentSingleInfo {

    private String href;

    private String number;

    private String creation_datetime;

    private String type;

    private String status;

    private String amount;

    private String currency;

    private String original_amount;

    private String original_curency;

    private String account_id;

    private String related_operation;

    private String description;

    private String control;

    private DotpayPaymentPayer payer;

    private String status_datetime;

}
