package gymApp.gym.models.dotpayPayment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DotpayPaymentLinkGeneration {

    private String amount;

    private String currency;

    private String description;

    private String control;

    private String language;

    private Integer ignoreLastPaymentChannel;

    private Integer redirectionType;

    private String url;

    private String urlc;

    private DotpayPaymentPayer payer;

}