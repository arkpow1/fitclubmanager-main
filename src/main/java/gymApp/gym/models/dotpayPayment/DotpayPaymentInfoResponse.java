package gymApp.gym.models.dotpayPayment;

import lombok.Data;

import java.util.List;

@Data
public class DotpayPaymentInfoResponse {

    private Integer count;

    private String next;

    private List<DotpayPaymentSingleInfo> results;

}
