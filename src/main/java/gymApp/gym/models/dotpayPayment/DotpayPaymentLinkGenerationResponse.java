package gymApp.gym.models.dotpayPayment;

import lombok.Data;

@Data
public class DotpayPaymentLinkGenerationResponse {

    private String href;

    private String payment_url;

    private String token;

    private String amount;

    private String currency;

    private String description;

    private String control;

    private String language;

    private String channel_id;

    private String ch_lock;

    private String onlinetransfer;

    private String redirection_type;

    private String buttontext;

    private String url;

    private String urlc;

    private String expiration_datetime;

    private String auto_reject_date;

    private DotpayPaymentPayer payer;

    private DotpayPaymentRecipient recipient;

    private String Customer; //TODO?
}
