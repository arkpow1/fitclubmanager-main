package gymApp.gym.models.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Event {

    private UUID id;

    private String title;

    private String description;

    private LocalDateTime date;

    private Integer likes;

    private UUID createdBy;

    private Instant creationDate;

    private boolean removed;

    private UUID imageId;
}
