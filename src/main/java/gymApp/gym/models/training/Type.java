package gymApp.gym.models.training;

public enum Type {
    INDIVIDUAL, GROUP
}
