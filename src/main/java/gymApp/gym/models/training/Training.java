package gymApp.gym.models.training;


import gymApp.gym.models.image.Image;
import gymApp.gym.models.trainer.Trainer;

import java.util.UUID;

public interface Training {

    void setId(UUID id);

    void setName(String name);

    void setDescription(String description);

    void setDurationInMinutes(Integer durationInMinutes);

    void setTrainerId(UUID trainerId);

    void setTrainer(Trainer Trainer);
}
