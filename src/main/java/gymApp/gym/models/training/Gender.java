package gymApp.gym.models.training;

public enum Gender {

    MAN, WOMAN, ALL
}
