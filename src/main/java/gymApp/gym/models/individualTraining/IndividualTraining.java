package gymApp.gym.models.individualTraining;

import gymApp.gym.models.client.Client;
import gymApp.gym.models.trainer.Trainer;
import gymApp.gym.models.training.Training;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IndividualTraining implements Training {

    private UUID id;

    private String name;

    private String description;

    private BigDecimal price;

    private LocalDate date;

    private LocalTime startingTime;

    private Integer durationInMinutes;

    private Integer intensity;

    private UUID createdBy;

    private Instant creationDate;

    private boolean removed;

    private UUID trainerId;

    private Trainer trainer;

    private UUID clientId;

    private Client client;

    public void setIntensity(int intensity) {
        if (intensity >= 0 && intensity <= 5) {
            this.intensity = intensity;
        } else {
            throw new IllegalStateException("Training intensity range is 0 to 5.");
        }
    }
}

