package gymApp.gym.models.trainer;

public enum Gender {

    MAN, WOMAN
}
