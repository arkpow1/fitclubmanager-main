package gymApp.gym.models.trainer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Trainer {

    private UUID id;

    private String email;

    private String name;

    private String surname;

    private String description;

    private String trainingTypes;

    private LocalDate birthdate;

    private String pesel;

    private String phoneNumber;

    private boolean notifications;

    private boolean showProfile;

    private Gender gender;

    private Instant creationDate;

    private boolean removed;

    private UUID userId;

    private UUID imageId;
}
