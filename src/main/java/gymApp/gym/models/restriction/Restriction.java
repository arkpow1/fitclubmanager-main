package gymApp.gym.models.restriction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Restriction {

    private UUID id;

    private RestrictedAction restrictedAction;

    private LocalDate restrictedUntil;

    private String reason;

    private UUID createdBy;

    private Instant creationDate;

    private boolean removed;

    private UUID userId;
}
