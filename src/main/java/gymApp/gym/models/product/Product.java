package gymApp.gym.models.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {

    private UUID id;

    private String name;

    private BigDecimal price;

    private Integer amount;

    private Instant creationDate;

    private boolean removed;

    private UUID userId;
}