package gymApp.gym.models.regulations;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Regulations {

    private UUID id;

    private RegulationsType type;

    private byte[] bytes;

    private Instant creationDate;

    private Boolean removed;
}
