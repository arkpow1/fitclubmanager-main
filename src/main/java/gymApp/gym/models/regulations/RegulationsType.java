package gymApp.gym.models.regulations;

public enum RegulationsType {
    REGIMEN, PRIVACY_POLICY
}
