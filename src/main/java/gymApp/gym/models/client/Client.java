package gymApp.gym.models.client;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Client {

    private UUID id;

    private String email;

    private String number;

    private String name;

    private String surname;

    private Instant birthdate;

    private String pesel;

    private String phoneNumber;

    private boolean notifications;

    private Gender gender;

    private Instant creationDate;

    private boolean removed;

    private RemoveReason removeReason;

    private UUID userId;
}
