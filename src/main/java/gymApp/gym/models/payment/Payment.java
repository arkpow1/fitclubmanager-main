package gymApp.gym.models.payment;

import gymApp.gym.models.pass.Pass;
import gymApp.gym.models.tariff.Currency;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Payment {

    private UUID id;

    private PaymentType type;

    private String url;

    private BigDecimal price;

    private Currency currency;

    private String description;

    private PaymentStatus status;

    private Instant creationDate;

    private boolean removed;

    private UUID passId;

    private UUID clientId;
}