package gymApp.gym.models.payment;

public enum PaymentStatus {
    NONE, COMPLETED, NOT_COMPLETED
}
