package gymApp.gym.common.exception;

import lombok.Getter;

@Getter
public abstract class GymAppException extends RuntimeException {
    private final ExceptionCode exceptionCode;

    public GymAppException(String message, ExceptionCode exceptionCode) {
        super(message);
        this.exceptionCode = exceptionCode;
    }
}
