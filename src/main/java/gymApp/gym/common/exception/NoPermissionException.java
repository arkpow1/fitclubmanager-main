package gymApp.gym.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
public class NoPermissionException extends GymAppException {
    public NoPermissionException(String message) {
        super(message, ExceptionCode.NO_PERMISSION);
    }
}
