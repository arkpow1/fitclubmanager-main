package gymApp.gym.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestTemplateConfig {

    @Bean
    public RestTemplate provideDefaultRestTemplate() {
        return new RestTemplate();
    }

}