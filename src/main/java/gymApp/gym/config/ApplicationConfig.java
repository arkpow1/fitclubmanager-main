package gymApp.gym.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;

@Data
@Configuration
@CrossOrigin
@PropertySource(value = "classpath:application.properties")
public class ApplicationConfig {

    @Value("${security.secret}")
    private String secret;

    @Value("${security.token.expiration.time}")
    private int tokenExpirationTime;

    @Value("${security.token.header}")
    private String tokenHeader;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
