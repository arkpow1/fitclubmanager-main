package gymApp.gym.groupTraining.service;

import gymApp.gym.client.service.ClientService;
import gymApp.gym.groupTraining.mapper.GroupTrainingMapper;
import gymApp.gym.groupTrainingMember.service.GroupTrainingMemberService;
import gymApp.gym.models.client.Client;
import gymApp.gym.models.groupTraining.GroupTraining;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class GroupTrainingService {

    private final GroupTrainingMapper groupTrainingMapper;
    private final GroupTrainingMemberService groupTrainingMemberService;
    private final ClientService clientService;

    @Autowired
    public GroupTrainingService(GroupTrainingMapper groupTrainingMapper, GroupTrainingMemberService groupTrainingMemberService, ClientService clientService) {
        this.groupTrainingMapper = groupTrainingMapper;
        this.groupTrainingMemberService = groupTrainingMemberService;
        this.clientService = clientService;
    }

    public List<GroupTraining> getAll() {
        List<GroupTraining> groupTrainings = groupTrainingMapper.getAll();
        return groupTrainings
                .stream()
                .map(this::setSpotsLeft)
                .collect(Collectors.toList());
    }

    public List<GroupTraining> getAllForClient(UUID userId) {
        Client client = clientService.getByUserId(userId);
        List<GroupTraining> groupTrainings = groupTrainingMapper.getAll();
        return groupTrainings
                .stream()
                .map(this::setSpotsLeft)
                .map(it -> setMembership(it, client.getId()))
                .collect(Collectors.toList());
    }

    public List<GroupTraining> getAllByDateBetweenTimeWithoutMembershipForClient(LocalDate date, LocalTime from, LocalTime to, UUID clientId) {
        List<GroupTraining> groupTrainings = groupTrainingMapper.getByDate(date);
        return groupTrainings
                .stream()
                .map(this::setSpotsLeft)
                .map(it -> setMembership(it, clientId))
                .filter(it -> !it.getMembership())
                .filter(it -> (it.getStartingTime().isAfter(from)) && (it.getStartingTime().isBefore(to)))
                .collect(Collectors.toList());
    }

    private GroupTraining setSpotsLeft(GroupTraining groupTraining) {
        groupTraining.setSpotsLeft(groupTraining.getMaxMembersAmount() - groupTrainingMemberService.countAllByGroupTrainingId(groupTraining.getId()));
        return groupTraining;
    }

    private GroupTraining setMembership(GroupTraining groupTraining, UUID clientId) {
        groupTraining.setMembership(groupTrainingMemberService.checkMembership(groupTraining.getId(), clientId));
        return groupTraining;
    }

    public void setRemoved(UUID id) {
        groupTrainingMapper.setRemoved(id);
    }

    public void deleteById(UUID id) {
        groupTrainingMapper.deleteById(id);
    }

    @Transactional
    public List<GroupTraining> create(UUID userId, GroupTraining groupTrainingTemplate, LocalDate cyclicTo, Integer daysRange) {
        ArrayList<GroupTraining> groupTrainingsCreated = new ArrayList<>();
        if (cyclicTo != null && daysRange != null) {
            List<LocalDate> dates = getDatesForCyclic(groupTrainingTemplate.getDate(), cyclicTo, daysRange);
            dates.forEach(date -> {
                        groupTrainingTemplate.setDate(date);
                        groupTrainingsCreated.addAll(create(userId, groupTrainingTemplate, null, null));
                    }
            );
            return groupTrainingsCreated;
        }
        GroupTraining groupTraining = prepareGroupTrainingToCreate(userId, groupTrainingTemplate);
        groupTrainingMapper.create(groupTraining);
        return Collections.singletonList(groupTraining);
    }

    public List<LocalDate> getDatesForCyclic(LocalDate startDate, LocalDate endDate, Integer daysRange) {
        ArrayList<LocalDate> dates = new ArrayList<>();
        while (startDate.isBefore(endDate) || startDate.isEqual(endDate)) {
            dates.add(startDate);
            startDate = startDate.plusDays(daysRange);
        }
        return dates;
    }

    private GroupTraining prepareGroupTrainingToCreate(UUID userId, GroupTraining groupTrainingTemplate) {
        return GroupTraining.builder()
                .id(UUID.randomUUID())
                .name(groupTrainingTemplate.getName())
                .description(groupTrainingTemplate.getDescription())
                .date(groupTrainingTemplate.getDate())
                .startingTime(groupTrainingTemplate.getStartingTime())
                .durationInMinutes(groupTrainingTemplate.getDurationInMinutes())
                .maxMembersAmount(groupTrainingTemplate.getMaxMembersAmount())
                .spotsLeft(groupTrainingTemplate.getMaxMembersAmount())
                .gender(groupTrainingTemplate.getGender())
                .intensity(groupTrainingTemplate.getIntensity())
                .createdBy(userId)
                .creationDate(Instant.now())
                .removed(false)
                .trainerId(groupTrainingTemplate.getTrainerId())
                .build();
    }

    // TODO bląd gdy update'ujemy ilosc osob na mniejsza niz jest aktualnie zapisanych
    public GroupTraining update(UUID userId, GroupTraining groupTrainingTemplate) {
        GroupTraining existingGroupTraining = groupTrainingMapper.getById(groupTrainingTemplate.getId());
        GroupTraining groupTraining = prepareGroupTrainingToUpdate(existingGroupTraining, groupTrainingTemplate);
        groupTrainingMapper.update(groupTraining);
        return groupTraining;
    }

    private GroupTraining prepareGroupTrainingToUpdate(GroupTraining existingGroupTraining, GroupTraining groupTrainingTemplate) {
        return GroupTraining.builder()
                .id(existingGroupTraining.getId())
                .name(groupTrainingTemplate.getName())
                .description(groupTrainingTemplate.getDescription())
                .date(groupTrainingTemplate.getDate())
                .startingTime(groupTrainingTemplate.getStartingTime())
                .durationInMinutes(groupTrainingTemplate.getDurationInMinutes())
                .maxMembersAmount(groupTrainingTemplate.getMaxMembersAmount())
                .gender(groupTrainingTemplate.getGender())
                .intensity(groupTrainingTemplate.getIntensity())
                .creationDate(existingGroupTraining.getCreationDate())
                .removed(existingGroupTraining.isRemoved())
                .trainerId(groupTrainingTemplate.getTrainerId())
                .build();
    }

    public GroupTraining getById(UUID id) {
        return groupTrainingMapper.getById(id);
    }
}
