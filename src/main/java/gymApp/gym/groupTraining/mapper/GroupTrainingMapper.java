package gymApp.gym.groupTraining.mapper;

import gymApp.gym.models.groupTraining.GroupTraining;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Repository
public interface GroupTrainingMapper {

    List<GroupTraining> getAll();

    List<GroupTraining> getByDate(@Param("date") LocalDate date);

    GroupTraining getById(@Param("id") UUID id);

    GroupTraining getByTrainerId(@Param("trainerId") UUID trainerId);

    void setRemoved(@Param("id") UUID id);

    void deleteById(@Param("id") UUID id);

    void create(GroupTraining groupTraining);

    void update(GroupTraining groupTraining);
}
