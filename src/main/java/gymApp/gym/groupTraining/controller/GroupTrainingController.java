package gymApp.gym.groupTraining.controller;


import gymApp.gym.groupTraining.service.GroupTrainingService;
import gymApp.gym.models.groupTraining.GroupTraining;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.security.PermissionChecker;
import gymApp.gym.security.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "training/groups")
public class GroupTrainingController {

    private final GroupTrainingService groupTrainingService;
    private final PermissionChecker permissionChecker;
    private final TokenService tokenService;

    @Autowired
    public GroupTrainingController(GroupTrainingService groupTrainingService, PermissionChecker permissionChecker, TokenService tokenService) {
        this.groupTrainingService = groupTrainingService;
        this.permissionChecker = permissionChecker;
        this.tokenService = tokenService;
    }

    @GetMapping(value = "/all")
    public List<GroupTraining> getAll(HttpServletRequest request) {
        return groupTrainingService.getAll();
    }

    @GetMapping(value = "/client")
    public List<GroupTraining> getAllForClient(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.CLIENT_ONLY);
        return groupTrainingService.getAllForClient(tokenService.getUserId(request));
    }

    @GetMapping(value = "/client/{clientId}")
    public List<GroupTraining> getAllByDateBetweenTimeWithoutMembershipForClient(
            @PathVariable UUID clientId,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.TIME) LocalTime from,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.TIME) LocalTime to,
            HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return groupTrainingService.getAllByDateBetweenTimeWithoutMembershipForClient(date, from, to, clientId);
    }

    @DeleteMapping(value = "/setRemoved/{id}")
    public void setRemoved(@PathVariable UUID id,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        groupTrainingService.setRemoved(id);
    }

    @DeleteMapping(value = "/remove/{id}")
    public void deleteById(@PathVariable UUID id,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        groupTrainingService.deleteById(id);
    }

    @PostMapping
    public List<GroupTraining> create(@RequestBody GroupTraining groupTraining,
                                      @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate cyclicTo,
                                      @RequestParam(required = false) Integer daysRange,
                                      HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return groupTrainingService.create(tokenService.getUserId(request), groupTraining, cyclicTo, daysRange);
    }

    @GetMapping("/days/count")
    public List<LocalDate> getDaysForCyclic(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
                                            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate cyclicTo,
                                            @RequestParam Integer daysRange,
                                            HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return groupTrainingService.getDatesForCyclic(date, cyclicTo, daysRange);
    }

    @PatchMapping
    public GroupTraining update(@RequestBody GroupTraining groupTraining,
                                HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return groupTrainingService.update(tokenService.getUserId(request), groupTraining);
    }

    @GetMapping(value = "/{id}")
    public GroupTraining getById(@PathVariable UUID id,
                                 HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ALL);
        return groupTrainingService.getById(id);
    }
}
