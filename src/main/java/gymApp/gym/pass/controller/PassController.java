package gymApp.gym.pass.controller;


import gymApp.gym.models.pass.Pass;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.pass.service.PassService;
import gymApp.gym.security.PermissionChecker;
import gymApp.gym.security.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "passes")
public class PassController {

    private final PassService passService;
    private final PermissionChecker permissionChecker;
    private final TokenService tokenService;

    @Autowired
    public PassController(PassService passService, PermissionChecker permissionChecker, TokenService tokenService) {
        this.passService = passService;
        this.permissionChecker = permissionChecker;
        this.tokenService = tokenService;
    }

    @GetMapping
    public List<Pass> getAll(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return passService.getAll();
    }

    @DeleteMapping(value = "/setRemoved/{id}")
    public void setRemoved(@PathVariable UUID id,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ALL);
        passService.setRemoved(id, tokenService.getUserId(request));
    }

    @DeleteMapping(value = "/remove/{id}")
    public void remove(@PathVariable UUID id,
                       HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        passService.remove(id);
    }

    @PostMapping
    public Pass create(@RequestParam UUID tariffId,
                       @RequestBody Pass pass,
                       HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return passService.create(tariffId, tokenService.getUserId(request), pass);
    }

    @PostMapping(value = "/self")
    public Pass createSelf(@RequestParam UUID tariffId,
                           @RequestBody Pass pass,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.CLIENT_ONLY);
        return passService.createSelf(tariffId, tokenService.getUserId(request), pass);
    }

    @PatchMapping(value = "/entriesLeft/{id}")
    public Integer decreaseEntriesLeft(@PathVariable UUID id,
                                       HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return passService.decreaseEntriesLeft(id);
    }

    @GetMapping(value = "/id/{id}")
    public Pass getById(@PathVariable UUID id,
                        HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ALL);
        return passService.getById(id);
    }

    @GetMapping(value = "/clientId/{clientId}")
    public List<Pass> getByClientId(@PathVariable UUID clientId,
                                    HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return passService.getByClientId(clientId);
    }

    @GetMapping(value = "/self")
    public List<Pass> getAllSelf(HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.CLIENT_ONLY);
        return passService.getAllSelf(tokenService.getUserId(request));
    }
}
