package gymApp.gym.pass.exception;

import gymApp.gym.common.exception.ExceptionCode;
import gymApp.gym.common.exception.GymAppException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class NoMoreEntriesLeftException extends GymAppException {
    public NoMoreEntriesLeftException(String message) {
        super(message, ExceptionCode.NO_MORE_ENTRIES_LEFT);
    }
}
