package gymApp.gym.pass.service;

import gymApp.gym.client.service.ClientService;
import gymApp.gym.models.pass.Pass;
import gymApp.gym.models.tariff.SettlementType;
import gymApp.gym.models.tariff.Tariff;
import gymApp.gym.pass.exception.NoMoreEntriesLeftException;
import gymApp.gym.pass.exception.PassNotRemovableException;
import gymApp.gym.pass.mapper.PassMapper;
import gymApp.gym.payment.payment.mapper.PaymentMapper;
import gymApp.gym.tariff.service.TariffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
public class PassService {
    private final PassMapper passMapper;
    private final TariffService tariffService;
    private final ClientService clientService;
    private final PaymentMapper paymentMapper;

    @Autowired
    public PassService(PassMapper passMapper, TariffService tariffService, ClientService clientService, PaymentMapper paymentMapper) {
        this.passMapper = passMapper;
        this.tariffService = tariffService;
        this.clientService = clientService;
        this.paymentMapper = paymentMapper;
    }

    public List<Pass> getAll() {
        return passMapper.getAll();
    }

    public void setRemoved(UUID id, UUID userId) {
        Pass pass = passMapper.getById(id);
        if ((pass.getActive()) ||
                (pass.isPaid()) ||
                (paymentMapper.getByPassId(id) != null) ||
                (!pass.getClientId().equals(clientService.getByUserId(userId).getId()))) {
            throw new PassNotRemovableException("Cannot remove pass");
        }
        passMapper.setRemoved(id);
    }

    public void remove(UUID id) {
        if (passMapper.getById(id).getActive()) {
            throw new PassNotRemovableException("Cannot remove active pass");
        }
        passMapper.remove(id);
    }

    public Pass create(UUID tariffId, UUID userId, Pass passTemplate) {
        Pass pass = createPassToSave(tariffId, userId, passTemplate);
        pass.setPaid(true);
        if (SettlementType.TIME_TYPE.contains(pass.getTariffSettlementType()))
            passMapper.createForTimeType(pass);
        if (SettlementType.QUANTITY_TYPE.contains(pass.getTariffSettlementType()))
            passMapper.createForQuantityType(pass);
        return pass;
    }

    public Pass createSelf(UUID tariffId, UUID userId, Pass passTemplate) {
        passTemplate.setClientId(clientService.getByUserId(userId).getId());
        Pass pass = createPassToSave(tariffId, userId, passTemplate);
        pass.setPaid(false);
        if (SettlementType.TIME_TYPE.contains(pass.getTariffSettlementType()))
            passMapper.createForTimeType(pass);
        if (SettlementType.QUANTITY_TYPE.contains(pass.getTariffSettlementType()))
            passMapper.createForQuantityType(pass);
        return pass;
    }

    private Pass createPassToSave(UUID tariffId, UUID createdBy, Pass passTemplate) {
        Tariff tariff = tariffService.getById(tariffId);
        return Pass.builder()
                .id(UUID.randomUUID())
                .tariffName(tariff.getName())
                .tariffSettlementType(tariff.getSettlementType())
                .totalPrice(tariff.getPrice())
                .unitPrice(generateUnitPrice(tariff.getPrice(), tariff.getSettlementType()))
                .currency(tariff.getCurrency())
                .activationDate(passTemplate.getActivationDate())
                .expirationDate(generateExpirationDate(passTemplate.getActivationDate(), tariff.getSettlementType()))
                .entriesLeft(generateEntriesLeft(tariff.getSettlementType()))
                .createdBy(createdBy)
                .creationDate(Instant.now())
                .removed(false)
                .clientId(passTemplate.getClientId())
                .build();
    }

    private LocalDate generateExpirationDate(LocalDate activationDate, SettlementType settlementType) {
        switch (settlementType) {
            case MONTH:
                return activationDate.plusMonths(1);
            case QUARTER:
                return activationDate.plusMonths(3);
            case SIX_MONTH:
                return activationDate.plusMonths(6);
            case YEAR:
                return activationDate.plusYears(1);
        }
        return null;
    }

    private Integer generateEntriesLeft(SettlementType settlementType) {
        switch (settlementType) {
            case ONE_TRAINING:
                return 1;
            case FOUR_TRAININGS:
                return 4;
            case EIGHT_TRAININGS:
                return 8;
            case TWELVE_TRAININGS:
                return 12;
        }
        return null;
    }

    private BigDecimal generateUnitPrice(BigDecimal totalPrice, SettlementType settlementType) {
        switch (settlementType) {
            case MONTH:
                return totalPrice;
            case QUARTER:
                return totalPrice.divide(BigDecimal.valueOf(3), 2, RoundingMode.DOWN);
            case SIX_MONTH:
                return totalPrice.divide(BigDecimal.valueOf(6), 2, RoundingMode.DOWN);
            case YEAR:
                return totalPrice.divide(BigDecimal.valueOf(12), 2, RoundingMode.DOWN);
            case ONE_TRAINING:
                return totalPrice;
            case FOUR_TRAININGS:
                return totalPrice.divide(BigDecimal.valueOf(4), 2, RoundingMode.DOWN);
            case EIGHT_TRAININGS:
                return totalPrice.divide(BigDecimal.valueOf(8), 2, RoundingMode.DOWN);
            case TWELVE_TRAININGS:
                return totalPrice.divide(BigDecimal.valueOf(12), 2, RoundingMode.DOWN);
        }
        throw new IllegalStateException("Illegal settlement type");
    }

    public Integer decreaseEntriesLeft(UUID passId) {
        Pass pass = passMapper.getById(passId);
        if (pass.getEntriesLeft() != null && pass.getEntriesLeft() > 0) {
            pass.setEntriesLeft(pass.getEntriesLeft() - 1);
            passMapper.updateEntriesLeft(pass);
            return pass.getEntriesLeft();
        }
        throw new NoMoreEntriesLeftException("There are no more entries left");
    }

    public Pass updatePaid(UUID id, boolean paid) {
        Pass pass = getById(id);
        pass.setPaid(paid);
        passMapper.updatePaid(pass);
        return pass;
    }

    public Pass getById(UUID id) {
        return passMapper.getById(id);
    }

    public List<Pass> getByClientId(UUID clientId) {
        return passMapper.getByClientId(clientId);
    }

    public List<Pass> getAllSelf(UUID userId) {
        return passMapper.getByClientId(clientService.getByUserId(userId).getId());
    }
}
