package gymApp.gym.pass.mapper;

import gymApp.gym.models.pass.Pass;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface PassMapper {

    List<Pass> getAll();

    void remove(@Param("id") UUID id);

    void setRemoved(@Param("id") UUID id);

    void createForTimeType(Pass pass);

    void createForQuantityType(Pass pass);

    void updateEntriesLeft(Pass pass);

    void updatePaid(Pass pass);

    Pass getById(@Param("id") UUID id);

    List<Pass> getByClientId(@Param("clientId") UUID clientId);
}
