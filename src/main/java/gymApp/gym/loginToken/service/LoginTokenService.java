package gymApp.gym.loginToken.service;

import gymApp.gym.loginToken.mapper.LoginTokenMapper;
import gymApp.gym.mail.service.EmailService;
import gymApp.gym.models.loginToken.LoginToken;
import gymApp.gym.user.generator.UserPasswordGenerator;
import gymApp.gym.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.UUID;

@Service
public class LoginTokenService {

    private final LoginTokenMapper loginTokenMapper;
    private final UserService userService;
    private final EmailService emailService;
    private final UserPasswordGenerator userPasswordGenerator;
    private final PasswordEncoder passwordEncoder;
    @Value("${login.token.email.subject}")
    private String loginTokenEmailSubject;
    @Value("${login.token.email.text}")
    private String loginTokenEmailText;

    @Autowired
    public LoginTokenService(LoginTokenMapper loginTokenMapper, UserService userService, EmailService emailService, UserPasswordGenerator userPasswordGenerator, PasswordEncoder passwordEncoder) {
        this.loginTokenMapper = loginTokenMapper;
        this.userService = userService;
        this.emailService = emailService;
        this.userPasswordGenerator = userPasswordGenerator;
        this.passwordEncoder = passwordEncoder;
    }

    public void createAndSendToEmail(String email) {
        if (userService.getByEmail(email) == null) {
            return;
        }
        if (getByEmail(email) != null) {
            removeByEmail(email);
        }
        String token = userPasswordGenerator.generateRandomPassword();
        LoginToken loginToken = prepareLoginTokenToSave(email, token);
        loginTokenMapper.create(loginToken);
        emailService.sendSimpleMessage(email, loginTokenEmailSubject, String.format(loginTokenEmailText, token));
    }

    private LoginToken prepareLoginTokenToSave(String email, String token) {
        return LoginToken.builder()
                .id(UUID.randomUUID())
                .token(passwordEncoder.encode(token))
                .email(email)
                .creationDate(Instant.now())
                .removed(false)
                .build();
    }

    public LoginToken getByEmail(String email) {
        return loginTokenMapper.getByEmail(email);
    }

    public boolean validateToken(String email, String token) {
        LoginToken loginToken = getByEmail(email);
        if (passwordEncoder.matches(token, loginToken.getToken())) {
            removeByEmail(email);
            return true;
        }
        return false;
    }

    private void removeByEmail(String email) {
        loginTokenMapper.removeByEmail(email);
    }
}