package gymApp.gym.loginToken.mapper;

import gymApp.gym.models.loginToken.LoginToken;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginTokenMapper {

    void removeByEmail(@Param("email") String email);

    void create(LoginToken loginToken);

    LoginToken getByEmail(@Param("email") String email);
}
