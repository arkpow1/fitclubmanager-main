package gymApp.gym.loginToken.controller;

import gymApp.gym.loginToken.service.LoginTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "login/tokens")
public class LoginTokenController {

    private final LoginTokenService loginTokenService;

    @Autowired
    public LoginTokenController(LoginTokenService loginTokenService) {
        this.loginTokenService = loginTokenService;
    }

    @PostMapping
    public ResponseEntity createAndSentToEmail(@RequestParam("email") String email) {
        loginTokenService.createAndSendToEmail(email);
        return ResponseEntity.ok().build();
    }
}
