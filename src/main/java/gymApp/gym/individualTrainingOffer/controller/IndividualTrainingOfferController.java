package gymApp.gym.individualTrainingOffer.controller;

import gymApp.gym.individualTrainingOffer.service.IndividualTrainingOfferService;
import gymApp.gym.models.individualTrainingOffer.IndividualTrainingOffer;
import gymApp.gym.models.user.UserRole;
import gymApp.gym.security.PermissionChecker;
import gymApp.gym.security.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "training/individual/offer")
public class IndividualTrainingOfferController {

    private final IndividualTrainingOfferService individualTrainingOfferService;
    private final PermissionChecker permissionChecker;
    private final TokenService tokenService;

    @Autowired
    public IndividualTrainingOfferController(IndividualTrainingOfferService individualTrainingOfferService, PermissionChecker permissionChecker, TokenService tokenService) {
        this.individualTrainingOfferService = individualTrainingOfferService;
        this.permissionChecker = permissionChecker;
        this.tokenService = tokenService;
    }

    @GetMapping(value = "/all")
    public List<IndividualTrainingOffer> getAll(HttpServletRequest request) {
        return individualTrainingOfferService.getAll();
    }

    @DeleteMapping(value = "/setRemoved/{id}")
    public void setRemoved(@PathVariable UUID id,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        individualTrainingOfferService.setRemoved(id);
    }

    @DeleteMapping(value = "/remove/{id}")
    public void deleteById(@PathVariable UUID id,
                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        individualTrainingOfferService.deleteById(id);
    }

    @PostMapping
    public IndividualTrainingOffer create(@RequestBody IndividualTrainingOffer individualTrainingOffer,
                                          HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return individualTrainingOfferService.create(tokenService.getUserId(request), individualTrainingOffer);
    }

    @PatchMapping
    public IndividualTrainingOffer update(@RequestBody IndividualTrainingOffer individualTrainingOffer,
                                          HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.WORKERS);
        return individualTrainingOfferService.update(tokenService.getUserId(request), individualTrainingOffer);
    }

    @GetMapping(value = "/{id}")
    public IndividualTrainingOffer getById(@PathVariable UUID id,
                                           HttpServletRequest request) {
        permissionChecker.checkPermission(request, UserRole.ALL);
        return individualTrainingOfferService.getById(id);
    }
}
