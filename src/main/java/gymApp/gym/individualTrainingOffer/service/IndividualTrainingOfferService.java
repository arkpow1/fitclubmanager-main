package gymApp.gym.individualTrainingOffer.service;

import gymApp.gym.individualTrainingOffer.mapper.IndividualTrainingOfferMapper;
import gymApp.gym.models.individualTrainingOffer.IndividualTrainingOffer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Service
public class IndividualTrainingOfferService {

    private final IndividualTrainingOfferMapper individualTrainingOfferMapper;

    @Autowired
    public IndividualTrainingOfferService(IndividualTrainingOfferMapper individualTrainingOfferMapper) {
        this.individualTrainingOfferMapper = individualTrainingOfferMapper;
    }

    public List<IndividualTrainingOffer> getAll() {
        return individualTrainingOfferMapper.getAll();
    }

    public void setRemoved(UUID id) {
        individualTrainingOfferMapper.setRemoved(id);
    }

    public void deleteById(UUID id) {
        individualTrainingOfferMapper.deleteById(id);
    }

    public IndividualTrainingOffer create(UUID userId, IndividualTrainingOffer individualTrainingOfferTemplate) {
        IndividualTrainingOffer individualTrainingOffer = prepareIndividualTrainingOfferToCreate(userId, individualTrainingOfferTemplate);
        individualTrainingOfferMapper.create(individualTrainingOffer);
        return individualTrainingOffer;
    }

    private IndividualTrainingOffer prepareIndividualTrainingOfferToCreate(UUID userId, IndividualTrainingOffer individualTrainingOfferTemplate) {
        return IndividualTrainingOffer.builder()
                .id(UUID.randomUUID())
                .name(individualTrainingOfferTemplate.getName())
                .description(individualTrainingOfferTemplate.getDescription())
                .targetClientDescription(individualTrainingOfferTemplate.getTargetClientDescription())
                .createdBy(userId)
                .creationDate(Instant.now())
                .removed(false)
                .imageId(individualTrainingOfferTemplate.getImageId())
                .build();
    }

    public IndividualTrainingOffer update(UUID userId, IndividualTrainingOffer individualTrainingOfferTemplate) {
        IndividualTrainingOffer existingIndividualTrainingOffer = individualTrainingOfferMapper.getById(individualTrainingOfferTemplate.getId());
        IndividualTrainingOffer individualTrainingOffer = prepareIndividualTrainingOfferToUpdate(existingIndividualTrainingOffer, individualTrainingOfferTemplate);
        individualTrainingOfferMapper.update(individualTrainingOffer);
        return individualTrainingOffer;
    }

    private IndividualTrainingOffer prepareIndividualTrainingOfferToUpdate(IndividualTrainingOffer existingIndividualTrainingOffer, IndividualTrainingOffer individualTrainingOfferTemplate) {
        return IndividualTrainingOffer.builder()
                .id(existingIndividualTrainingOffer.getId())
                .name(individualTrainingOfferTemplate.getName())
                .description(individualTrainingOfferTemplate.getDescription())
                .targetClientDescription(individualTrainingOfferTemplate.getTargetClientDescription())
                .createdBy(existingIndividualTrainingOffer.getCreatedBy())
                .creationDate(existingIndividualTrainingOffer.getCreationDate())
                .removed(existingIndividualTrainingOffer.isRemoved())
                .imageId(individualTrainingOfferTemplate.getImageId())
                .build();
    }

    public IndividualTrainingOffer getById(UUID id) {
        return individualTrainingOfferMapper.getById(id);
    }
}
