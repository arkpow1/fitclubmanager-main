package gymApp.gym.individualTrainingOffer.mapper;

import gymApp.gym.models.individualTrainingOffer.IndividualTrainingOffer;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface IndividualTrainingOfferMapper {

    List<IndividualTrainingOffer> getAll();

    IndividualTrainingOffer getById(@Param("id") UUID id);

    void setRemoved(@Param("id") UUID id);

    void deleteById(@Param("id") UUID id);

    void create(IndividualTrainingOffer individualTrainingOffer);

    void update(IndividualTrainingOffer individualTrainingOffer);
}
