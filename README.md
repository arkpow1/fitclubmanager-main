# FitClubManager - Main

# General informations
FitClubManager is web application designated for sport facilities like e.g. a gym, fit club, etc.

Its purpose is to help owners with management of such objects and human resources.

Contact us for informations about additional projects for extra functionalities.

Project was created as engineering project by students of Adam Mickiewicz University in Poznań, Poland.

This repository contains source code of backend server written in Java8.

# Properties
Explanation about some of properties that needs to be replaced in project to successfully run application:

# Database - docker
There is docker-compose file included in repository that creates postgres database, change properties:

POSTGRES_PASSWORD:

POSTGRES_USER:

POSTGRES_DB:

Than execute with command "docker-compose up" at computer with docker and docker-compose installed.

# Database - application.properties
Replace these values in application.properties file to match those from docker-compose file:

spring.datasource.url=

spring.datasource.username=

spring.datasource.password=

spring.datasource.driver-class-name=org.postgresql.Driver

spring.datasource.name=

# Secret key
Secret key used for authentication:

security.secret=

# Email
Email account informations (application sends emails to users):

spring.mail.host=

spring.mail.port=

spring.mail.username=

spring.mail.password=

# Admin user
System requires 1 default user, you must modify its data in file db-1.0.0-userTable.xml located in resources.

Needed replacements:

XXX with email address

YYY with password encoded with Spring Password Encoder

# Tests
In order to run all tests successfully also replace:

admin.email=<same email address as in db-1.0.0-userTable.xml file>

admin.password=<unencoded password from db-1.0.0-userTable.xml file>

# Generating jar
To generate jar file you should run command "mvn clean install" with Maven installed on your computer.

# Frontend
Source code of frontend web application can be found here:

    https://bitbucket.org/GoldmannAgnieszka/fitclubmanager-frontend/src/master/

# License
Copyright [2020] [Arkadiusz Adam Powęska]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

# Contact emails
arkpow1@st.amu.edu.pl

agngol3@st.amu.edu.pl

krystianlipiec@gmail.com